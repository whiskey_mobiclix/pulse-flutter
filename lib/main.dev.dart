import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nu/dependency-injection.dart';
import 'package:nu/routes.dart';
import 'package:nu/src/configs/enviroment.config.dart';
import 'package:nu/src/configs/theme.config.dart';
import 'package:nu/src/mockup/token.mockup.dart';
import 'package:nu/src/providers/providers.dart';
import 'package:nu/src/services/auth.service.dart';
import 'package:provider/provider.dart';

void main() async {
  SystemChrome.setSystemUIOverlayStyle(AppTheme.status.style);
  WidgetsFlutterBinding.ensureInitialized();

  // Configs
  configureInjection();
  getIt.get<AuthService>().login(accessToken: token);
  getIt<Enviroment>().setMode("dev");

  runApp(App());
}

class App extends StatelessWidget {
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: providers,
      child: MaterialApp(
        title: 'Nu core project',
        theme: ThemeData(
          fontFamily: AppTheme.font.name,
        ),
        routes: routes,

      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:nu/src/screens/event/idol-event-instruction.screen.dart';
import 'package:nu/src/screens/event/idol-event.screen.dart';
import 'package:nu/src/screens/home/home.screen.dart';
import 'package:nu/src/screens/leaderboard/leaderboard.screen.dart';
import 'package:nu/src/screens/level/level.screen.dart';
import 'package:nu/src/screens/me/me.screen.dart';
import 'package:nu/src/screens/test.dart';

final Map<String, Widget Function(BuildContext)> routes = {
  "/": (_) => Home(),
  "/profile": (_) => MeScreen(),
  "/level": (_) => LevelScreen(),
  "/event": (_) => IdolEventScreen(),
  "/event-introduction": (_) => IdolEventInstruction(),
  "/leader-board": (_) => LeaderboardScreen(),
  "/test": (_) => TestScreen(),
};

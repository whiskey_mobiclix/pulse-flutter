import 'dart:convert';

import 'package:nu/dependency-injection.dart';
import 'package:nu/src/configs/enviroment.config.dart';
import 'package:nu/src/models/gift.model.dart';
import 'package:nu/src/utils/map.dart';
import 'package:nu/src/utils/request.dart';

class GiftService {
  static Future<List<Gift>> list() async {
    final response = await Request.GET(
        "${getIt<Enviroment>().get("api-url")}/v1/engagement/gift?type_of_gift=gift_item&product=pulse");

    if (response.statusCode != 200) {
      throw "Error when get list gifts";
    } else {
      final res = json.decode(response.body);

      if (res["data"] is List) {
        List<Gift> gifts = [];

        try {
          res["data"].forEach((o) {
            if (o["name"].toLowerCase() != "hot") {
              o["gifts"].forEach((item) {
                final tmp = CustomMap(item);
                gifts.add(new Gift(
                  id: tmp.get("id"),
                  thumbnail: tmp.get("extension.thumb_url"),
                  name: tmp.get("name"),
                ));
              });
            }
          });
        } catch (e) {
          return [];
        }

        return gifts;
      }
    }

    return [];
  }
}

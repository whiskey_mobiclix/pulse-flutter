import 'dart:convert';

import 'package:nu/dependency-injection.dart';
import 'package:nu/src/configs/enviroment.config.dart';
import 'package:nu/src/models/stat.model.dart';
import 'package:nu/src/utils/constants.dart';
import 'package:nu/src/utils/map.dart';
import 'package:nu/src/utils/request.dart';

class UserStatsService {
  static Future<List<Stat>> list(
      {int id, List<String> fields, int gender}) async {
    final url =
        "${getIt<Enviroment>().get("api-url")}/v1/counting-all-time?user_id=$id&fields[]=${fields.join("&fields[]=")}";
    final response = await Request.GET(url);
    final res = json.decode(response.body);

    if (response.statusCode != 200) {
      throw "Error when get user stats: id = $id";
    } else if (res["data"] != null) {
      final data = new CustomMap(res["data"]);

      List<Stat> stats = [
        new Stat(
          label: {
            "i18n": "profile.stat.matched",
          },
          value: (data.get("people_matched_count") ?? 0).toDouble(),
        )
      ];

      if (gender != Constants.gender.female) {
        stats.add(new Stat(
          show: gender != Constants.gender.female,
          label: {
            "i18n": "profile.stat.gifts",
          },
          value: (data.get("gift_sent_count") ?? 0).toDouble(),
        ));

        stats.add(new Stat(
          show: gender != Constants.gender.female,
          label: {
            "i18n": "profile.stat.friends",
          },
          value: (data.get("friend_count") ?? 0).toDouble(),
        ));
      } else {
        stats.add(new Stat(
          show: gender == Constants.gender.female,
          label: {
            "i18n": "profile.stat.gifts-received",
          },
          value: (data.get("gift_received_count") ?? 0).toDouble(),
        ));
      }

      return stats;
    }

    return <Stat>[];
  }

  static Future<List<dynamic>> giftsSent({int userId}) async {
    final url =
        "${getIt<Enviroment>().get("api-url")}/v1/counting-all-time?user_id=$userId&fields[]=list_gift_sent_count";
    final response = await Request.GET(url);
    final res = json.decode(response.body);

    if (response.statusCode != 200) {
      throw "Error when get gifts stats: userId = $userId";
    } else if (res["data"] != null) {
      return res["data"]["list_gift_sent_count"];
    }

    return [];
  }
}

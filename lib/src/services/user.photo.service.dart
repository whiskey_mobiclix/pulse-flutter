import 'dart:convert';

import 'package:nu/dependency-injection.dart';
import 'package:nu/src/configs/enviroment.config.dart';
import 'package:nu/src/utils/map.dart';
import 'package:nu/src/utils/request.dart';

class UserPhotosService {
  static Future<List<String>> list({int userId}) async {
    final response = await Request.GET(
        "${getIt<Enviroment>().get("api-url")}/v1/user/photo?user_id=$userId");
    final res = json.decode(response.body);

    if (response.statusCode != 200) {
      throw "Failed when get my photos";
    } else {
      if (res["data"] is List) {
        List<String> photos = [];

        res["data"].forEach((o) {
          photos.add(new CustomMap(o).get("document.url"));
        });

        return photos;
      }

      return <String>[];
    }
  }
}

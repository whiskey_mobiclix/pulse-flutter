import 'dart:convert';

import 'package:nu/dependency-injection.dart';
import 'package:nu/src/configs/enviroment.config.dart';
import 'package:nu/src/models/event.model.dart';
import 'package:nu/src/models/quest.event.model.dart';
import 'package:nu/src/utils/map.dart';
import 'package:nu/src/utils/request.dart';

class EventService {
  static Future<Event> details() async {
    print("Loading event details...");

    final response = await Request.GET(
        "${getIt<Enviroment>().get("api-url")}/v1/engagement/events?product=pulse");

    if (response.statusCode != 200) {
      throw "Error when get event info";
    } else {
      final res = json.decode(response.body);

      if (res["data"] is List && res["data"].length > 0) {
        final event = CustomMap(res["data"][0]);
        return new Event(
          name: event.get("name"),
          start: event.get("start_time"),
          end: event.get("end_time"),
          opened: event.get("status") == "open",
        );
      }
    }

    return new Event();
  }

  static Future<List<Quest>> list() async {
    print("Loading event...");

    final response = await Request.GET(
        "${getIt<Enviroment>().get("api-url")}/v1/engagement/user/event?event_id=1");

    if (response.statusCode != 200) {
      throw "Error when get event data";
    } else {
      final res = json.decode(response.body);

      if (res["data"] is List) {
        final rounds = res["data"];

        List<Quest> items = [];
        rounds.forEach((o) {
          final tmp = CustomMap(o["round"] ?? {});
          final tmp2 = CustomMap(o["process"] ?? {});

          items.add(new Quest(
            name: tmp.get("name"),
            goal: tmp.get("goal.kpi") ?? 0,
            earned: tmp2.get("got") ?? 0,
            score: tmp2.get("score") ?? 0,
            giftId: int.parse(tmp.get("goal.ref_id")[0] ?? "-99"),
            bonus: tmp.get("reward.gift_amount"),
            gems: tmp.get("reward.gems"),
          ));
        });

        return items;
      }
    }

    return [];
  }
}

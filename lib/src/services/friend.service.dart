import 'dart:convert';

import 'package:nu/dependency-injection.dart';
import 'package:nu/src/configs/enviroment.config.dart';
import 'package:nu/src/utils/constants.dart';
import 'package:nu/src/utils/request.dart';

class FriendService {
  static Future<Map<String, List<int>>> getRelationship({int userId}) async {
    final response = await Request.GET(
        "${getIt<Enviroment>().get("api-url")}/v1/user/relationship?fields[]=friend_normal&fields[]=friend_favourite&fields[]=requesting&fields[]=requested&fields[]=blocking&fields[]=blocked&user_id=$userId");

    if (response.statusCode != 200) {
      throw "Error when get relationship";
    } else {
      final res = json.decode(response.body);
      if (res["data"] is Map) {
        Map<String, List<int>> results = {};
        final keys = res["data"].keys;
        keys.forEach((o) {
          results[o] = List<int>.from(res["data"][o]);
        });

        return results;
      }
    }

    return {};
  }

  static Future<bool> create({int receiverId}) async {
    final response = await Request.POST(
      "${getIt<Enviroment>().get("api-url")}/pulse/v1/user/friend-request",
      {
        "receiver_id": receiverId,
      },
    );

    print("Receiver ID $receiverId");
    print("Add friend response ${response.body}");

    if (response.statusCode != 200) {
      print("Sent friend request failed");
      return false;
    }

    final res = json.decode(response.body);

    if (res["status"] == Constants.response.success) return true;

    return false;
  }

  static Future<bool> accept({int userId}) async {
    final response = await Request.PUT(
        "${getIt<Enviroment>().get("api-url")}/pulse/v1/user/friend-request", {
      "user_id": userId,
      "status": Constants.friendRequestStatus.confirm,
    });

    print("Receiver ID $userId");
    print("Accept friend response ${response.body}");

    if (response.statusCode != 200) {
      print("Accept friend failed");
      return false;
    }

    final res = json.decode(response.body);

    if (res["status"] == Constants.response.success) return true;

    return false;
  }
}

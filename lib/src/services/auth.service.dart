class AuthService {
  String accessToken = "";
  bool isAuthenticated = false;

  void login({
    String accessToken
  }){
    this.accessToken = accessToken ?? "";
    this.isAuthenticated = true;
  }
}
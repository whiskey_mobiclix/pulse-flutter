import 'dart:io';

import 'package:device_info/device_info.dart';

class DeviceService {
  Future<String> get id async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();

    // iOS
    if (Platform.isIOS) {
      final info = await deviceInfo.iosInfo;
      return info.identifierForVendor;
    }

    // Android
    final info = await deviceInfo.androidInfo;
    return info.androidId;
  }
}
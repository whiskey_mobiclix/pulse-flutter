import 'dart:convert';

import 'package:nu/dependency-injection.dart';
import 'package:nu/src/configs/enviroment.config.dart';
import 'package:nu/src/utils/map.dart';
import 'package:nu/src/utils/request.dart';

class UserInterestService {
  static Future<List<String>> list({int userId}) async {
    final response = await Request.GET(
        "${getIt<Enviroment>().get("api-url")}/v1/pulse/user/interest?user_id=$userId");
    final res = json.decode(response.body);

    if (response.statusCode != 200) {
      throw "Failed when get user interest: id = $userId";
    } else {
      if (res["data"] is List) {
        List<String> hobbies = [];

        res["data"].forEach((o) {
          hobbies.add(new CustomMap(o).get("interest.name"));
        });

        return hobbies;
      }

      return <String>[];
    }
  }
}

import 'dart:convert';

import 'package:nu/dependency-injection.dart';
import 'package:nu/src/configs/enviroment.config.dart';
import 'package:nu/src/models/property.model.dart';
import 'package:nu/src/utils/map.dart';
import 'package:nu/src/utils/request.dart';

class PropertyService {
  static Future<List<Property>> list() async {
    final response = await Request.GET(
        "${getIt<Enviroment>().get("api-url")}/v1/engagement/gift?product=pulse&type_of_gift=entrance_effect");

    final res = json.decode(response.body);

    if (response.statusCode != 200) {
      throw "Error when get property list";
    } else if (res["data"] != null && res["data"] is List) {
      final gifts = res["data"][0]["gifts"];

      if (gifts is List) {
        List<Property> props = [];

        gifts.forEach((e) {
          final item = CustomMap(e);

          props.add(new Property({
            "name": item.get("name"),
            "thumbnail": item.get("extension.thumb_url"),
          }));
        });

        return props;
      }
    }

    return [];
  }
}

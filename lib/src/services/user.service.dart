import 'dart:convert';
import 'package:nu/dependency-injection.dart';
import 'package:nu/src/configs/enviroment.config.dart';
import 'package:nu/src/models/avatar.model.dart';
import 'package:nu/src/utils/request.dart';
import 'package:nu/src/utils/map.dart';

//Models
import 'package:nu/src/models/user.model.dart';
import 'package:nu/src/models/level.model.dart';

class UserService {
  static Future<User> info({dynamic id}) async {
    String url = "${getIt<Enviroment>().get("api-url")}/pulse/v1/user/$id";
    final response = await Request.GET(url);
    final res = json.decode(response.body);

    if (response.statusCode != 200 || res["data"] == null) {
      throw "Failed when get user info: id = $id";
    } else {
      final data = new CustomMap(res["data"]);

      final user = new User({
        "id": data.get("user_id"),
        "uuid": data.get("user_name"),
        "isIdol": data.get("extension.is_idol"),
        "avatar": new Avatar(
          url: data.get("avatar.document.url"),
          frame: data.get("extension.frame_image"),
        ),
        "coins": data.get("extension.coin"),
        "level": new Level({
          "value": data.get("extension.level"),
          "score": data.get("extension.score"),
          "nextLevelScore": data.get("extension.xp_next_level"),
          "percentage": data.get("extension.progress_to_level_up"),
        }),
        "platinum": data.get("extension.is_premium"),
        "name": "${data.get("first_name")} ${data.get("last_name")}",
        "age": new DateTime.now().year -
            new DateTime.fromMillisecondsSinceEpoch(
                    (data.get("birthdate") ?? 0) * 1000)
                .year,
        "country": data.get("country_code"),
        "gender": data.get("gender"),
        "extra": {
          "friend-status": data.get("friend_status"),
        }
      });

      return user;
    }
  }
}

import 'dart:convert';

import 'package:nu/dependency-injection.dart';
import 'package:nu/src/configs/enviroment.config.dart';
import 'package:nu/src/models/avatar.model.dart';
import 'package:nu/src/models/leaderboard.item.model.dart';
import 'package:nu/src/models/user.model.dart';
import 'package:nu/src/modules/datetime.module.dart';
import 'package:nu/src/utils/map.dart';
import 'package:nu/src/utils/request.dart';

class LeaderBoardGlobalService {
  static Future<List<LeaderboardItem>> list({
    String type,
    int from,
    int to,
  }) async {
    final _to = to ?? DateTimeModule.getCachedTimestampUTC();
    final leaderBoardListResponse = await Request.GET(
        "${getIt<Enviroment>().get("api-url")}/v1/pulse/leader-board?type=$type&from=$from&to=$_to&pag.page=1&pag.size=50");

    if (leaderBoardListResponse.statusCode != 200)
      throw "Error when get leader board";
    else {
      final res = json.decode(leaderBoardListResponse.body);

      if (res["data"] is List) {
        List<LeaderboardItem> items = [];
        res["data"].forEach((o) {
          final tmp = CustomMap(o);
          items.add(
            new LeaderboardItem(
              user: new User({
                "id": tmp.get("user_id"),
                "name": tmp.get("full_name"),
                "gender": tmp.get("gender"),
                "avatar": new Avatar(
                  url: tmp.get("avatar_url"),
                ),
              }),
              point: tmp.get("point"),
              type: type,
              rank: tmp.get("rank"),
            ),
          );
        });

        return items;
      }
    }
    return [];
  }

  static Future<LeaderboardItem> getMyRank({
    String type,
    int from = 1,
  }) async {
    print("Loading my global rank...");

    final response = await Request.GET(
        "${getIt<Enviroment>().get("api-url")}/v1/pulse/user/current-rank?&type=$type&from=$from&to=${DateTimeModule.getCachedTimestampUTC()}");

    if (response.statusCode != 200) {
      throw "Error when get my rank";
    } else {
      final res = json.decode(response.body);

      if (res["data"] is Map) {
        return new LeaderboardItem(
          user: new User({}),
          type: type,
          point: res["data"]["point"],
          rank: res["data"]["rank"],
        );
      }
    }

    return new LeaderboardItem();
  }
}

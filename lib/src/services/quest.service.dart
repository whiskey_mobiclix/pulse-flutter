import 'dart:math' as math;

import 'package:nu/src/mockup/quest.mockup.dart';
import 'package:nu/src/models/quest.daily.model.dart';
import 'package:nu/src/utils/map.dart';

class QuestService {
  static Future<List<DailyQuest>> daily() async {
    List<DailyQuest> quests = [];

    MockupDailyQuest.forEach((o) {
      final qst = CustomMap(o);
      quests.add(new DailyQuest(
        name: qst.get("name") ?? "",
        goal: math.max(qst.get("goal"), 1),
        earned: qst.get("earned") ?? 0,
        bonusGems: qst.get("gemsBonus") ?? 0,
        icon: qst.get("icon") ?? "",
      ));
    });

    return quests;
  }
}

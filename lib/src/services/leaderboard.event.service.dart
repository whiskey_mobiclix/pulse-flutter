import 'dart:convert';

import 'package:nu/dependency-injection.dart';
import 'package:nu/src/configs/enviroment.config.dart';
import 'package:nu/src/models/avatar.model.dart';
import 'package:nu/src/models/leaderboard.item.model.dart';
import 'package:nu/src/models/level.model.dart';
import 'package:nu/src/models/user.model.dart';
import 'package:nu/src/utils/map.dart';
import 'package:nu/src/utils/request.dart';

class LeaderboardService {
  static Future<List<LeaderboardItem>> list() async {
    final response = await Request.GET(
        "${getIt<Enviroment>().get("api-url")}/v1/pulse/leader-board?event_id=1&type=event&pag.page=1&pag.size=50");

    if (response.statusCode != 200) {
      throw "Error when get leader board";
    } else {
      final res = json.decode(response.body);
      if (res["data"] is List) {
        List<LeaderboardItem> items = [];
        res["data"].forEach((o) {
          final tmp = CustomMap(o);
          items.add(new LeaderboardItem(
            user: new User({
              "id": tmp.get("user_id"),
              "name": tmp.get("full_name"),
              "avatar": new Avatar(
                url: tmp.get("avatar_url"),
              ),
              "level": new Level({
                "value": tmp.get("level"),
              }),
            }),
            point: tmp.get("point"),
            rank: tmp.get("rank"),
          ));
        });

        return items;
      }
    }

    return [];
  }

  static Future<LeaderboardItem> getMyRank() async {
    final response = await Request.GET(
        "${getIt<Enviroment>().get("api-url")}/v1/pulse/user/current-rank?event_id=1&type=event&to=${(DateTime.now().millisecondsSinceEpoch / 1000).round()}");

    if (response.statusCode != 200) {
      throw "Error when get my rank";
    } else {
      final res = json.decode(response.body);

      if (res["data"] is Map) {
        return new LeaderboardItem(
          user: new User({}),
          point: res["data"]["point"],
          rank: res["data"]["rank"],
        );
      }
    }

    return new LeaderboardItem();
  }
}

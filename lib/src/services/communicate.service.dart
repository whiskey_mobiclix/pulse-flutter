import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nu/dependency-injection.dart';
import 'package:nu/routes.dart';
import 'package:nu/src/providers/general.provider.dart';
import 'package:nu/src/screens/user/user.router.dart';
import 'package:nu/src/services/auth.service.dart';
import 'package:rxdart/rxdart.dart';

class CommunicateService {
  MethodChannel _channel;
  BuildContext context;
  ReplaySubject<void> _refreshProfile = ReplaySubject<void>();

  Stream<void> get refreshProfile => _refreshProfile.stream;

  ReplaySubject<void> _clearProfile = ReplaySubject<void>();

  Stream<void> get clearProfile => _clearProfile.stream;

  ReplaySubject<void> _onProfileUpdated = ReplaySubject<void>();

  Stream<void> get onProfileUpdated => _onProfileUpdated.stream;

  ReplaySubject<void> _refreshRelationShip = ReplaySubject<void>();

  Stream<void> get refreshRelationShip => _refreshRelationShip.stream;

  MethodChannel get channel => _channel;

  CommunicateService() {
    _channel = MethodChannel("pulse", JSONMethodCodec());
    _channel.setMethodCallHandler((call) async {
      switch (call.method) {
        case "setToken":
          final token = call.arguments["token"];
          final lang = call.arguments["language"];
          getIt.get<AuthService>().login(accessToken: token);
          getIt<GeneralProvider>().changeLanguage(lang ?? "en");
          break;

        case "refreshProfile":
          _refreshProfile.add(null);
          break;

        case "clearProfile":
          _clearProfile.add(null);
          break;

        case "refreshRelationship":
          _refreshRelationShip.add(null);
          break;

        case "profileUpdated":
          _onProfileUpdated.add(null);
          break;

        case "replaceRoute":
          final route = call.arguments["route"];

          if (route == "/user") {
            final userId = call.arguments["args"]["user_id"];
            Navigator.pushReplacement(
              context,
              PageRouteBuilder(
                pageBuilder: (_, __, ___) => UserRouter(),
                transitionDuration: Duration(seconds: 0),
              ),
            );
          } else {
            Navigator.pushReplacement(
              context,
              PageRouteBuilder(
                pageBuilder: (_, __, ___) => routes[route](context),
                transitionDuration: Duration(seconds: 0),
              ),
            );
          }

          break;
      }
      return null;
    });
  }

  navigate({String route, dynamic args}) {
    _channel.invokeMethod("navigate", {"route": route, "args": args});
  }

  request({String requestName, dynamic args}) {
    _channel.invokeMethod(requestName, {"args": args});
  }

  navigateBack() {
    SystemNavigator.pop(animated: true);
  }
}

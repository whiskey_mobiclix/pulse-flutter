import 'package:nu/dependency-injection.dart';
import 'package:nu/src/providers/general.provider.dart';
import 'package:nu/src/providers/gift.provider.dart';
import 'package:nu/src/providers/leaderboard.provider.dart';
import 'package:nu/src/providers/property.provider.dart';
import 'package:nu/src/providers/event.provider.dart';
import 'package:nu/src/providers/quest.provider.dart';
import 'package:nu/src/providers/test.provider.dart';
import 'package:provider/provider.dart';

import 'me.provider.dart';

final providers = [
  ChangeNotifierProvider<GeneralProvider>(
      create: (_) => getIt<GeneralProvider>()),
  ChangeNotifierProvider<MeProvider>(create: (_) => getIt<MeProvider>()),
  ChangeNotifierProvider<PropertyProvider>(
      create: (_) => getIt<PropertyProvider>()),
  ChangeNotifierProvider<EventProvider>(create: (_) => getIt<EventProvider>()),
  ChangeNotifierProvider<GiftProvider>(create: (_) => getIt<GiftProvider>()),
  ChangeNotifierProvider<LeaderboardProvider>(
      create: (_) => getIt<LeaderboardProvider>()),
  ChangeNotifierProvider<QuestProvider>(create: (_) => getIt<QuestProvider>()),
  ChangeNotifierProvider<TestProvider>(create: (_) => getIt<TestProvider>()),
];

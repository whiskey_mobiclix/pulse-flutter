import 'package:flutter/material.dart';
import 'package:nu/src/models/gift.model.dart';
import 'package:nu/src/services/gift.service.dart';

class GiftProvider extends ChangeNotifier {
  ValueNotifier<List<Gift>> list = ValueNotifier([]);
  ValueNotifier<bool> loading = ValueNotifier(true);

  void fetch() async {
    try {
      this.loading.value = true;
      this.list.value = await GiftService.list();
      this.loading.value = false;
    } catch (e) {
      this.loading.value = false;
      print(e);
    }
  }
}

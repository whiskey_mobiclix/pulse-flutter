import 'package:flutter/material.dart';

class TestProvider extends ChangeNotifier {
  String message = "Hello";
  int count = 0;

  void changeMessage(String mess) {
    this.message = mess;
    notifyListeners();
  }

  void addCount(){
    this.count = 1;
    notifyListeners();
  }
}

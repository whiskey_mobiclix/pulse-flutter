import 'package:flutter/material.dart';
import 'package:nu/src/models/property.model.dart';
import 'package:nu/src/services/property.service.dart';

class PropertyProvider extends ChangeNotifier {
  ValueNotifier<List<Property>> list = ValueNotifier([]);

  void fetch() async {
    try {
      this.list.value = await PropertyService.list();
    } catch (e) {
      print(e);
    }
  }
}

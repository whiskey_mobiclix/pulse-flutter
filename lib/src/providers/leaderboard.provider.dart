import 'package:flutter/material.dart';
import 'package:nu/dependency-injection.dart';
import 'package:nu/src/models/leaderboard.item.model.dart';
import 'package:nu/src/modules/datetime.module.dart';
import 'package:nu/src/providers/me.provider.dart';
import 'package:nu/src/services/leaderboard.event.service.dart';
import 'package:nu/src/services/leaderboard.global.service.dart';
import 'package:nu/src/utils/constants.dart';

class LeaderboardProvider extends ChangeNotifier {
  ValueNotifier<List<LeaderboardItem>> list = ValueNotifier([]);
  ValueNotifier<LeaderboardItem> myRank = ValueNotifier(new LeaderboardItem());
  ValueNotifier<bool> loading = ValueNotifier(true);

  ValueNotifier<bool> loadingGlobal = ValueNotifier(true);
  ValueNotifier<LeaderboardItem> myGlobalRank =
      ValueNotifier(new LeaderboardItem());
  ValueNotifier<bool> myGlobalRankLoading = ValueNotifier(true);
  ValueNotifier<List<LeaderboardItem>> leaderboardGlobalList =
      ValueNotifier([]);
  ValueNotifier<String> currentActiveTab =
      ValueNotifier(Constants.leaderboardType.coin);
  ValueNotifier<String> previousActiveTab =
      ValueNotifier(Constants.leaderboardType.coin);
  ValueNotifier<String> leaderboardGlobalTimeFilter = ValueNotifier("all");

  void fetch() async {
    try {
      print("Loading leaderboard...");
      this.loading.value = true;
      this.list.value = await LeaderboardService.list();
      this.myRank.value = await LeaderboardService.getMyRank();
      this.loading.value = false;
    } catch (e) {
      this.loading.value = false;
      print(e);
    }
  }

  void getLeaderboardGlobalList({String type}) async {
    try {
      print("Loading leaderboard global...");
      this.loadingGlobal.value = true;
      this.leaderboardGlobalList.value = [];
      this.myGlobalRankLoading.value = true;
      this.myGlobalRank.value = new LeaderboardItem();
      this.leaderboardGlobalList.value = await LeaderBoardGlobalService.list(
        type: type,
        from: this.leaderboardGlobalTimeFilter.value == "all"
            ? 1
            : (DateTimeModule.getMondayUTC().millisecondsSinceEpoch / 1000)
                .round(),
      );
      final filteredByMyId =  this.leaderboardGlobalList.value.where((o) => o.user.id == getIt<MeProvider>().user.value.id);
      if(filteredByMyId.isNotEmpty){
        this.myGlobalRank.value = filteredByMyId.first;
      }
      this.myGlobalRankLoading.value = false;

      this.loadingGlobal.value = false;
    } catch (e) {
      this.loadingGlobal.value = false;
      print(e);
    }
  }

  void getMyGlobalRank({String type}) async {
    try {
      print("Loading my rank...");
      this.myGlobalRankLoading.value = true;
      this.myGlobalRank.value = await LeaderBoardGlobalService.getMyRank(
        type: type,
        from: this.leaderboardGlobalTimeFilter.value == "all"
            ? 1
            : (DateTimeModule.getMondayUTC().millisecondsSinceEpoch / 1000)
                .round(),
      );
      this.myGlobalRankLoading.value = false;
    } catch (e) {
      print(e);
    }
  }

  void onActiveTabChange(String newTab) async {
    if (currentActiveTab.value == newTab) return;
    previousActiveTab.value = currentActiveTab.value;
    currentActiveTab.value = newTab;

    getLeaderboardGlobalList(type: newTab);
  }

  void setLeaderBoardGlobalTimeFrom(String value) {
    this.leaderboardGlobalTimeFilter.value = value;
    this.getLeaderboardGlobalList(type: this.currentActiveTab.value);
  }
}

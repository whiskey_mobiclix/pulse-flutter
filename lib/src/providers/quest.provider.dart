import 'package:flutter/material.dart';
import 'package:nu/src/models/quest.daily.model.dart';
import 'package:nu/src/services/quest.service.dart';

class QuestProvider extends ChangeNotifier {
  ValueNotifier<List<DailyQuest>> dailyQuests = ValueNotifier([]);

  void getDailyQuests() async {
    this.dailyQuests.value = await QuestService.daily();
  }
}

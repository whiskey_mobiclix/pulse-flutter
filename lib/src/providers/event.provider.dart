import 'package:flutter/material.dart';
import 'package:nu/src/models/event.model.dart';
import 'package:nu/src/models/quest.event.model.dart';
import 'package:nu/src/services/event.service.dart';

class EventProvider extends ChangeNotifier {
  ValueNotifier<List<Quest>> list = ValueNotifier(<Quest>[]);
  ValueNotifier<Event> details = ValueNotifier(new Event());

  void getDetails() async {
    try {
      this.details.value = await EventService.details();
    } catch (e) {
      print(e);
    }
  }

  void fetch() async {
    try {
      this.list.value = await EventService.list();
    } catch (e) {
      print(e);
    }
  }
}

import 'package:flutter/material.dart';
import 'package:nu/src/configs/general.config.dart';

class GeneralProvider extends ChangeNotifier {
  ValueNotifier<String> language = ValueNotifier(AppSettings.defaultlang);

  void changeLanguage(String lang) {
    this.language.value = lang;
  }
}

import 'package:flutter/material.dart';
import 'package:nu/dependency-injection.dart';
import 'package:nu/src/models/message.model.dart';
import 'package:nu/src/models/user.model.dart';
import 'package:nu/src/services/communicate.service.dart';
import 'package:nu/src/services/friend.service.dart';
import 'package:nu/src/services/user.interest.service.dart';
import 'package:nu/src/services/user.photo.service.dart';
import 'package:nu/src/services/user.service.dart';
import 'package:nu/src/services/user.stats.service.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MeProvider extends ChangeNotifier {
  ValueNotifier<Map<String, dynamic>> introduction = ValueNotifier({
    "checking": true,
    "users": <String>[],
  });

  bool loading = true;
  String error = "";
  User data = new User({});
  ValueNotifier updated = ValueNotifier(new Message(
    i18n: "profile.message.updated",
  ));

  ValueNotifier<User> user = ValueNotifier(new User({}));
  ValueNotifier<Map<String, List<int>>> relationship = ValueNotifier({});

  MeProvider() {
    checkIntroductionStatus();

    getIt<CommunicateService>().refreshProfile.listen((event) {
      fetch();
    });

    getIt<CommunicateService>().clearProfile.listen((event) {
      reset();
    });

    getIt<CommunicateService>().refreshRelationShip.listen((event) {
      getRelationShip();
    });

    getIt<CommunicateService>().onProfileUpdated.listen((event) {
      setUpdatedMessage(status: true);
      new Future.delayed(Duration(milliseconds: 3000), () {
        setUpdatedMessage(status: false);
      });
    });
  }

  void fetch() async {
//    final SharedPreferences prefs = await SharedPreferences.getInstance();
//    prefs.remove("profile-introduction-users");

    User usr = new User({});

    this.user.value = usr;

    try {
      usr = await UserService.info(id: "me");
    } catch (e) {
      usr = new User({"id": -99});
      print("Username: ${usr.name}");
      print(e);
    }

    try {
      final hobbies = await UserInterestService.list(userId: usr.id);
      usr.setHobbies(hobbies);
    } catch (e) {
      print(e);
    }

    try {
      List<String> statsFields = [];
      if (usr.isFemale()) {
        statsFields = ["gift_received_count", "people_matched_count"];
      } else {
        statsFields = [
          "gift_sent_count",
          "people_matched_count",
          "friend_count"
        ];
      }
      final stats = await UserStatsService.list(
          id: usr.id, fields: statsFields, gender: usr.gender);
      usr.setStats(stats);
    } catch (e) {
      print(e);
    }

    try {
      final photos = await UserPhotosService.list(userId: usr.id);
      usr.photos.setData(photos);
    } catch (e) {
      print(e);
    }

    this.user.value = usr;
  }

  void addUserIdAsSeenProfileIntro(int userId) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    this.introduction.value["users"].add(userId.toString());

    List<String> prefsValue = this.introduction.value["users"].cast<String>();
    prefs.setStringList("profile-introduction-users", prefsValue);

    this.introduction.value = {
      "checking": false,
      "users": prefsValue,
    };
  }

  void checkIntroductionStatus() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> status = prefs.getStringList("profile-introduction-users");

    this.introduction.value = {
      "checking": false,
      "users": status == null ? [] : status,
    };
  }

  void setUpdatedMessage({bool status}) {
    this.updated.value = new Message(
      i18n: this.updated.value.i18n,
      show: status,
    );
  }

  void reset() {
    this.user.value = new User({});
    this.updated.value = new Message(i18n: "profile.message.updated");
    this.relationship.value = {};
  }

  void getRelationShip({VoidCallback callback}) async {
    try {
      print("Loading relationship...");
      this.relationship.value = await FriendService.getRelationship(
          userId: getIt<MeProvider>().user.value.id);
      if (callback != null) callback();
    } catch (e) {
      print(e);
    }
  }
}

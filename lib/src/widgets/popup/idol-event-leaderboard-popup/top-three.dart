import 'package:flutter/material.dart';
import 'package:nu/dependency-injection.dart';
import 'package:nu/src/models/leaderboard.item.model.dart';
import 'package:nu/src/models/user.model.dart';
import 'package:nu/src/modules/number.module.dart';
import 'package:nu/src/providers/leaderboard.provider.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/utils/utils.dart';
import 'package:nu/src/widgets/core/image.widget.dart';

import 'top-three-item.dart';

class TopThree extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _widthScreen = Grid.columns(context, 24);
    final leaderboardPvdr = getIt<LeaderboardProvider>();

    List<LeaderboardItem> items =
        leaderboardPvdr.list.value.where((e) => e.rank < 4).toList();
    if (items.length == 0) {
      items.add(new LeaderboardItem());
      items.add(new LeaderboardItem());
      items.add(new LeaderboardItem());
    } else if (items.length < 2) {
      items.add(new LeaderboardItem(rank: 2));
      items.add(new LeaderboardItem(rank: 3));
    } else if (items.length < 3) {
      items.add(new LeaderboardItem(rank: 3));
    }

    items.sort((a, b) => a.rank.compareTo(b.rank));

    return Container(
      child: Stack(
        children: [
          Container(
            height: _widthScreen / 1.7142857142857144,
            alignment: Alignment.bottomCenter,
            child: NuImage(
              path:
                  'assets/images/common/idol-event-leaderboard-stand-reward.png',
              width: double.infinity,
              fit: BoxFit.fitWidth,
            ),
          ),
          _UserRank1st(context, items[0]),
          _UserRank2nd(context, items[1]),
          _UserRank3rd(context, items[2]),
        ],
      ),
    );
  }

  Widget _UserRank1st(BuildContext context, LeaderboardItem item) {
    return Positioned(
      width: Grid.columns(context, 20),
      top: 0,
      child: TopThreeItem(
        user: item.user,
        width: Grid.columns(context, 5),
        name: item.user.name,
        point: item.point,
        type: 'idolStar',
        top: 1,
      ),
    );
  }

  Widget _UserRank2nd(BuildContext context, LeaderboardItem item) {
    final _widthScreen = Grid.columns(context, 24);

    return Positioned(
      top: _widthScreen / 9,
      left: _widthScreen / 41.142857142857146,
      child: TopThreeItem(
        user: item.user,
        width: Grid.columns(context, 5),
        name: item.user.name,
        point: item.point,
        type: 'idolStar',
        top: 2,
      ),
    );
  }

  Widget _UserRank3rd(BuildContext context, LeaderboardItem item) {
    final _widthScreen = Grid.columns(context, 24);

    return Positioned(
      top: _widthScreen / 6.5,
      right: _widthScreen / 100,
      child: TopThreeItem(
        user: item.user,
        width: Grid.columns(context, 5),
        name: item.user.name,
        point: item.point,
        type: 'idolStar',
        top: 3,
      ),
    );
  }
}

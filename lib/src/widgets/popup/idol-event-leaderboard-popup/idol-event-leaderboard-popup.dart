import 'package:flutter/material.dart';
import 'package:nu/dependency-injection.dart';
import 'package:nu/src/providers/general.provider.dart';
import 'package:nu/src/providers/leaderboard.provider.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/widgets/core/image.widget.dart';
import 'package:nu/src/widgets/popup/idol-event-leaderboard-popup/idol-event-leaderboard-list/leaderboard-list.dart';

import 'top-three.dart';

class IdolEventLeaderboardPopup extends StatefulWidget {
  @override
  _IdolEventLeaderboardPopupState createState() =>
      _IdolEventLeaderboardPopupState();
}

class _IdolEventLeaderboardPopupState extends State<IdolEventLeaderboardPopup> {
  void initState() {
    super.initState();
    getIt<LeaderboardProvider>().fetch();
  }

  @override
  Widget build(BuildContext context) {
    final _widthScreen = Grid.columns(context, 24);

    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Stack(
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            margin: EdgeInsets.only(
              left: Grid.columns(context, 1),
              right: Grid.columns(context, 1),
              top: _widthScreen / 8.22857142857143,
              bottom: _widthScreen / 13.714285714285715,
            ),
            padding: EdgeInsets.symmetric(
              horizontal: Grid.columns(context, 1),
              vertical: Grid.columns(context, 1),
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(14.0),
              image: DecorationImage(
                alignment: Alignment.topCenter,
                image: AssetImage(
                  'assets/images/backgrounds/idol-event-leaderboard-popup-bg.png',
                ),
                fit: BoxFit.cover,
              ),
            ),
            child: Column(
              children: [
                Container(
                    child: ValueListenableBuilder(
                  valueListenable: getIt<GeneralProvider>().language,
                  builder: (context, lang, _) {
                    return NuImage(
                      path: 'assets/images/common/idol-event-title.$lang.png',
                      height: _widthScreen / 3.428571428571429,
                      fit: BoxFit.fitHeight,
                    );
                  },
                )),
                _LeaderBoardMainContent(),
              ],
            ),
          ),
          _LeaderboardPopupCloseButton(),
        ],
      ),
    );
  }

  Widget _LeaderboardListener({Widget child}) {
    final leaderboardPvdr = getIt<LeaderboardProvider>();

    return ValueListenableBuilder(
      valueListenable: leaderboardPvdr.loading,
      builder: (context, loading, _) {
        if (loading)
          return Expanded(
            child: Center(
              child: CircularProgressIndicator(
                valueColor:
                    new AlwaysStoppedAnimation<Color>(Color(0xffffffff)),
              ),
            ),
          );

        return child;
      },
    );
  }

  Widget _LeaderBoardMainContent() {
    return _LeaderboardListener(
      child: Expanded(
        child: Column(
          children: [
            TopThree(),
            LeaderboardList(),
          ],
        ),
      ),
    );
  }

  Widget _LeaderboardPopupCloseButton() {
    final _widthScreen = Grid.columns(context, 24);

    return Positioned(
      right: Grid.columns(context, 1),
      child: GestureDetector(
        onTap: () => Navigator.of(context).pop(),
        child: Container(
          width: _widthScreen / 10.285714285714286,
          height: _widthScreen / 10.285714285714286,
          decoration: new BoxDecoration(
            color: Colors.white.withOpacity(0.15),
            shape: BoxShape.circle,
          ),
          child: Center(
            child: Icon(
              Icons.clear,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }
}

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:nu/src/modules/number.module.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/utils/utils.dart';
import 'package:nu/src/widgets/core/text.widget.dart';

class ListItem extends StatelessWidget {
  final int number;
  final String name;
  final String avatarUrl;
  final int point;
  final String type;
  final bool noLine;

  ListItem({
    this.number,
    this.name,
    this.avatarUrl,
    this.point = 0,
    this.type,
    this.noLine = false,
  }) : assert(type != null);

  @override
  Widget build(BuildContext context) {
    final _width = Grid.columns(context, 20);

    return Container(
      padding: EdgeInsets.symmetric(
        vertical: _width / 48.97959183673469,
      ),
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(
            color: noLine == false
                ? Color(0xffdedede).withOpacity(0.2)
                : Colors.transparent,
            width: noLine == false ? 1 : 0,
          ),
        ),
      ),
      child: Row(
        children: [
          SizedBox(
            width: Grid.columns(context, 2) - 10,
            child: NuText(
              text: number.toString(),
              style: TextStyle(
                color: Color(0xff999999),
                fontSize: 10,
              ),
            ),
          ),
          Container(
            width: _width / 8.571428571428571,
            height: _width / 8.571428571428571,
            child: CircleAvatar(
              backgroundImage: !Utils.isEmpty(avatarUrl)
                  ? NetworkImage(avatarUrl)
                  : AssetImage("assets/images/avatar.png"),
              backgroundColor: Color(0xffeeeeee),
              radius: Grid.columns(context, 1.65),
            ),
          ),
          SizedBox(width: _width / 22.857142857142854),
          Expanded(
            child: NuText(
              text: Utils.clearZeroWidthSpace(name),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                color: Colors.white,
                fontSize: 12,
              ),
            ),
          ),
          SizedBox(width: 15),
          _getPointNumber(_width),
        ],
      ),
    );
  }

  Widget _getPointNumber(double _containerWidth) {
    const textColor = {
      'coin': Color(0xffffee00),
      'gem': Colors.white,
      'gift': Colors.white,
      'idolStar': Colors.white,
    };

    final icons = {
      'coin': 'assets/images/coin.png',
      'gem': 'assets/images/gem.png',
      'gift': 'assets/images/gift.png',
      'idolStar': 'assets/images/star.png',
    };

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        NuText(
          text: NumberModule.format(point),
          style: TextStyle(
              color: textColor['$type'],
              fontSize: 12,
              fontWeight: FontWeight.bold),
        ),
        SizedBox(width: 10),
        Image.asset(
          icons['$type'],
          height: _containerWidth / 22.857142857142854,
        ),
      ],
    );
  }
}

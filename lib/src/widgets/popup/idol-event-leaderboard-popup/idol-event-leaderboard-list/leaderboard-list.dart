import 'package:flutter/material.dart';
import 'package:nu/dependency-injection.dart';
import 'package:nu/src/models/leaderboard.item.model.dart';
import 'package:nu/src/providers/leaderboard.provider.dart';
import 'package:nu/src/providers/me.provider.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/widgets/core/text.widget.dart';
import 'package:provider/provider.dart';
import 'list-item/list-item.dart';

class LeaderboardList extends StatelessWidget {
  final String type;

  LeaderboardList({
    this.type = 'idolStar',
  });

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Stack(
        children: [
          _LeaderBoardListWrapper(context),
          _LeaderboardMyRank(),
        ],
      ),
    );
  }

  Widget _LeaderBoardListWrapper(BuildContext context) {
    final leaderboardPvdr = getIt<LeaderboardProvider>();
    final data = leaderboardPvdr.list.value;
    final _width = Grid.columns(context, 20);

    return Container(
      height: double.infinity,
      width: double.infinity,
      decoration: BoxDecoration(
        color: Color(0xff06001c).withOpacity(0.6),
        borderRadius: BorderRadius.circular(12),
      ),
      padding: EdgeInsets.only(
        left: _width / 34.285714285714285,
        right: _width / 34.285714285714285,
        bottom: _width / 6.233766233766233,
      ),
      child: data.isNotEmpty
          ? _LeaderboardListItems(data)
          : Padding(
              padding: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
              child: NuText(
                i18n: "event.leaderboard.empty",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 12,
                ),
              ),
            ),
    );
  }

  Widget _LeaderboardMyRank() {
    final leaderboardPvdr = getIt<LeaderboardProvider>();
    final mePvdr = getIt<MeProvider>();

    return Positioned(
      left: 0,
      right: 0,
      bottom: 0,
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 10.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          gradient: LinearGradient(
            colors: [
              Color(0xff4b915d8).withOpacity(0.8),
              Color(0xff26078f).withOpacity(0.8),
            ],
          ),
        ),
        child: leaderboardPvdr.myRank.value.rank > 0 &&
                leaderboardPvdr.myRank.value.rank < 50
            ? ListItem(
                avatarUrl: mePvdr.user.value.avatar.url,
                type: type,
                noLine: true,
                name: mePvdr.user.value.name,
                number: leaderboardPvdr.myRank.value.rank,
                point: leaderboardPvdr.myRank.value.point,
              )
            : Padding(
              padding: EdgeInsets.symmetric(horizontal: 5, vertical: 15),
              child: NuText(
                  i18n: "event.leaderboard.out-of-leadeboard",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 12,
                    fontWeight: FontWeight.bold,
                  ),
                ),
            ),
      ),
    );
  }

  Widget _LeaderboardListItems(List<LeaderboardItem> data) {
    return Container(
      width: double.infinity,
      child: SingleChildScrollView(
        child: Column(
          children: data.map((item) {
            if (item.rank < 4)
              return SizedBox();
            else
              return ListItem(
                avatarUrl: item.user.avatar.url,
                type: type,
                noLine: item.rank == 4,
                name: item.user.name,
                number: item.rank,
                point: item.point,
              );
          }).toList(),
        ),
      ),
    );
  }
}

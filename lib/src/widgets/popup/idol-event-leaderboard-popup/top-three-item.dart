import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:nu/src/configs/theme.config.dart';
import 'package:nu/src/models/avatar.model.dart';
import 'package:nu/src/models/level.model.dart';
import 'package:nu/src/models/user.model.dart';
import 'package:nu/src/modules/number.module.dart';
import 'package:nu/src/utils/utils.dart';
import 'package:nu/src/widgets/core/image.widget.dart';
import 'package:nu/src/widgets/core/text.widget.dart';
import 'package:nu/src/widgets/profile/avatar.widget.dart';

class TopThreeItem extends StatelessWidget {
  final double width;
  final double height;
  final String name;
  final int point;
  final int top;
  final String type;
  final User user;

  TopThreeItem({
    this.width,
    this.height,
    @required this.name,
    this.point = 0,
    this.top = 3,
    this.type = 'coin',
    this.user,
  }) : assert((width != null ?? height != null));

  @override
  Widget build(BuildContext context) {
    final rankIcons = {
      '1': 'assets/images/common/number-one.png',
      '2': 'assets/images/common/number-two.png',
      '3': 'assets/images/common/number-three.png',
    };

    return Column(
      children: [
        ProfileAvatarWidget(
          avatar: user.avatar,
          level: user.level,
        ),
        _LeaderBoardUserName(),
        if (!Utils.isEmpty(this.name)) _getPointNumber(),
        if (!Utils.isEmpty(this.name))
          SizedBox(
              height: top == 3
                  ? width / 8.571428571428571
                  : width / 4.285714285714286),
        if (!Utils.isEmpty(this.name))
          Image.asset(
            rankIcons["$top"],
            height: width / 2.142857142857143,
          ),
      ],
    );
  }

  Widget _LeaderBoardUserName() {
    if (Utils.isEmpty(this.name))
      return SizedBox(
        width: width,
        child: NuText(
          text: '???',
          style: TextStyle(
            color: Colors.white,
            fontSize: 10,
          ),
          textAlign: TextAlign.center,
        ),
      );
    return SizedBox(
      width: width,
      child: AutoSizeText(
        Utils.clearZeroWidthSpace(name),
        maxLines: 1,
        maxFontSize: (width / 7.142857142857142).floor().toDouble(),
        minFontSize: (width / 10.142857142857142).floor().toDouble(),
        overflow: TextOverflow.ellipsis,
        style: TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.w300,
        ),
        textAlign: TextAlign.center,
      ),
    );
  }

  Widget _getPointNumber() {
    const textColor = {
      'coin': Colors.white,
      'gem': Colors.white,
      'gift': Colors.white,
      'idolStar': Colors.white,
    };

    final icons = {
      'coin': "assets/images/common/coin.png",
      'gem': "assets/images/common/gem.png",
      'gift': "assets/images/common/common/gift.png",
      'idolStar': "assets/images/common/white-star.png",
    };

    return SizedBox(
      width: width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Flexible(
            child: AutoSizeText(
              NumberModule.format(point),
              minFontSize: 10,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                color: textColor['$type'],
                fontWeight: FontWeight.w700,
                fontSize: this.width / 5.7142857142857135,
                fontFamily: AppTheme.font.name,
                shadows: <Shadow>[
                  Shadow(
                    offset: Offset(0.0, 2.0),
                    blurRadius: 5.0,
                    color: Colors.black.withOpacity(0.78),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            width: this.width / 28.57142857142857,
          ),
          NuImage(
            path: icons['$type'],
            height: this.width / 6.593406593406593,
          ),
        ],
      ),
    );
  }
}

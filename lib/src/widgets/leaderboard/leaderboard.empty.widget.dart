import 'package:flutter/material.dart';
import 'package:nu/dependency-injection.dart';
import 'package:nu/src/modules/number.module.dart';
import 'package:nu/src/providers/me.provider.dart';
import 'package:nu/src/services/communicate.service.dart';
import 'package:nu/src/utils/constants.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/widgets/core/image.widget.dart';
import 'package:nu/src/widgets/core/text.widget.dart';

class LeaderBoardEmptyWidget extends StatelessWidget {
  final String type;

  LeaderBoardEmptyWidget({this.type});

  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        child: Column(
          children: [
            _leaderBoardEmptyIcon(context),
            _leaderBoardEmptyFooter(context),
          ],
        ),
      ),
    );
  }

  Widget _leaderBoardEmptyIcon(BuildContext context) {
    return Expanded(
      child: Transform.translate(
        offset: Offset(0, -Grid.columns(context, 2)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            NuImage(
              path: "assets/images/leaderboard-empty.gif",
              width: Grid.columns(context, 12),
            ),
            NuText(
              i18n: "leaderboard.empty",
              style: TextStyle(
                fontSize: 12,
                color: Color(0xffb5b5b5),
                fontWeight: FontWeight.bold,
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _leaderBoardEmptyFooter(BuildContext context) {
    return Stack(
      overflow: Overflow.visible,
      alignment: Alignment.center,
      children: [
        Container(
          width: Grid.columns(context, 24),
          padding: EdgeInsets.only(
            left: Grid.columns(context, .5),
            right: Grid.columns(context, .5),
            bottom: 20,
            top: Grid.columns(context, 2),
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(5),
              topRight: Radius.circular(5),
            ),
            boxShadow: [
              BoxShadow(
                color: Color.fromRGBO(0, 0, 0, .07),
                blurRadius: 3,
              )
            ],
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [Color(0xffF4FEFF), Color(0xffE9EFFF)],
            ),
          ),
          child: Column(
            children: [
              NuText(
                i18n: "leaderboard.empty.footer",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 11.5,
                ),
              ),
              SizedBox(height: 15),
              _leaderBoardEmptyButton(),
            ],
          ),
        ),
        _leaderBoardEmptyFooterMe(context),
      ],
    );
  }

  Widget _leaderBoardEmptyFooterCoins(BuildContext context) {
    return GestureDetector(
      onTap: () {
        getIt<CommunicateService>()
            .navigate(route: "/store", args: {"selected": "coins"});
      },
      child: Stack(
        overflow: Overflow.visible,
        children: [
          Container(
            padding: EdgeInsets.symmetric(
              horizontal: 15,
              vertical: 10,
            ),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(25),
            ),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                NuText(
                  text: NumberModule.format(1000),
                  uppercase: true,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 12,
                  ),
                ),
                SizedBox(width: 8),
                NuImage(
                  path: "assets/images/coins.png",
                  width: Grid.columns(context, 1.1),
                ),
              ],
            ),
          ),
          Positioned(
            top: -1.0 * Grid.columns(context, .2),
            right: -1.0 * Grid.columns(context, .2),
            child: Container(
              padding: EdgeInsets.only(
                top: Grid.columns(context, .06),
                left: Grid.columns(context, .06),
                right: Grid.columns(context, .12),
                bottom: Grid.columns(context, .12),
              ),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Color(0xff56BA00),
              ),
              child: NuImage(
                path: "assets/images/plus.png",
                width: Grid.columns(context, .7),
                fit: BoxFit.cover,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _leaderBoardEmptyFooterMe(BuildContext context) {
    return Positioned(
      top: -1.0 * Grid.columns(context, 1.8),
      child: CircleAvatar(
        radius: Grid.columns(context, 1.8),
        backgroundImage:
            NetworkImage(getIt<MeProvider>().user.value.avatar.url ?? ""),
        backgroundColor: Color(0xffd5d5d5),
      ),
    );
  }

  Widget _leaderBoardEmptyButton() {
    return GestureDetector(
      onTap: () {
        getIt<CommunicateService>().navigate(route: "/discover", args: {});
      },
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: 30,
          vertical: 10,
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(25),
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [Color(0xff4DFF8B), Color(0xff16D087)],
          ),
        ),
        child: NuText(
          i18n: "button.start-match",
          uppercase: true,
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontSize: 12,
          ),
        ),
      ),
    );
  }
}

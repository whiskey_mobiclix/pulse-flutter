import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:nu/dependency-injection.dart';
import 'package:nu/src/providers/me.provider.dart';
import 'package:nu/src/services/communicate.service.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/utils/utils.dart';
import 'package:nu/src/widgets/core/image.widget.dart';
import 'package:nu/src/widgets/core/text.widget.dart';

class LeaderboardOutOfCoinPopup extends StatelessWidget {
  final Function onCancel;
  final Function onOk;

  const LeaderboardOutOfCoinPopup({
    Key key,
    this.onCancel,
    this.onOk,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          margin: EdgeInsets.fromLTRB(
            Grid.columns(context, 1),
            0,
            Grid.columns(context, 1),
            Grid.columns(context, 2),
          ),
          padding: EdgeInsets.all(Grid.columns(context, 1.5)),
          width: double.infinity,
          height: 270 + Grid.columns(context, 2),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
          ),
          child: _mainContent(context),
        ),
        Positioned(
          top: Grid.columns(context, .6),
          right: Grid.columns(context, 1.6),
          child: _closeButton(context),
        )
      ],
    );
  }

  Widget _closeButton(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pop(context);
        if (onCancel != null) onCancel();
      },
      child: Container(
        padding: EdgeInsets.all(Grid.columns(context, 2.5) / 3),
        decoration: BoxDecoration(
          color: Color(0xfff4f4f4).withOpacity(.5),
          shape: BoxShape.circle,
        ),
        width: Grid.columns(context, 2.5),
        height: Grid.columns(context, 2.5),
        child: NuImage(
          path: "assets/images/common/close_ic.png",
        ),
      ),
    );
  }

  Widget _mainContent(BuildContext context) {
    final _contentStyle = TextStyle(
      fontSize: 13,
    );

    welcome({String name}) => Row(
//          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            NuText(
              i18n: "leaederboard.popup.outOfCoin.welcome",
              style: _contentStyle,
            ),
            ConstrainedBox(
              constraints: BoxConstraints(maxWidth: Grid.columns(context, 10)),
              child: NuText(
                text: Utils.clearZeroWidthSpace(" $name"),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: _contentStyle.merge(TextStyle(
                  fontWeight: FontWeight.bold,
                )),
              ),
            )
          ],
        );

    return ValueListenableBuilder(
        valueListenable: getIt<MeProvider>().user,
        builder: (context, user, _) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              CircleAvatar(
                radius: Grid.columns(context, 1.8),
                backgroundColor: Color(0xffd5d5d5),
                backgroundImage: NetworkImage(
                    getIt<MeProvider>().user.value.avatar.url ?? ""),
              ),
              SizedBox(height: 30),
              welcome(name: user.name),
              SizedBox(height: 5),
              NuText(
                i18n: "leaederboard.popup.outOfCoin.content",
                style: _contentStyle,
              ),
              SizedBox(height: 30),
              _buttonBuilder(context),
            ],
          );
        });
  }

  Widget _buttonBuilder(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pop(context);
        getIt<CommunicateService>()
            .navigate(route: "/store", args: {"selected": "coins"});
        if (onOk != null) onOk();
      },
      child: Container(
        height: Grid.columns(context, 2.7),
        width: Grid.columns(context, 14),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [Color(0xff4DFF8B), Color(0xff16D087)],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
          borderRadius: BorderRadius.circular(100),
        ),
        child: NuText(
          i18n: "leaederboard.popup.outOfCoin.button.label",
          uppercase: true,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.white,
            fontSize: 12.5,
          ),
        ),
      ),
    );
  }
}

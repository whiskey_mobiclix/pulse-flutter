import 'dart:async';

import 'package:flutter/material.dart';
import 'package:nu/dependency-injection.dart';
import 'package:nu/src/models/user.model.dart';
import 'package:nu/src/providers/me.provider.dart';
import 'package:nu/src/services/communicate.service.dart';
import 'package:nu/src/services/friend.service.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/widgets/core/center-popup.widget.dart';
import 'package:nu/src/widgets/core/image.widget.dart';
import 'package:nu/src/widgets/core/text.widget.dart';

import 'leaderboard.error.dart';

class LeaderboardConfirmPopup extends StatefulWidget {
  final Function onCancel;
  final Function onOk;
  final User user;
  final bool accept;
  final BuildContext parentContext;

  const LeaderboardConfirmPopup(
    this.parentContext, {
    Key key,
    this.onCancel,
    this.onOk,
    this.user,
    this.accept,
  }) : super(key: key);

  @override
  _LeaderboardConfirmPopupState createState() =>
      _LeaderboardConfirmPopupState();
}

class _LeaderboardConfirmPopupState extends State<LeaderboardConfirmPopup> {
  bool loading = false;

  @override
  Widget build(BuildContext context) {
    bool freeToMakeFriend = false;
    if (getIt<MeProvider>().user.value.isFemale()) {
      freeToMakeFriend = true;
    } else {
      if (widget.accept && !widget.user.isFemale()) {
        freeToMakeFriend = true;
      }
    }

    return Stack(
      children: [
        Container(
          margin: EdgeInsets.fromLTRB(
            Grid.columns(context, 1),
            0,
            Grid.columns(context, 1),
            Grid.columns(context, 2),
          ),
          padding: EdgeInsets.all(Grid.columns(context, 1.5)),
          width: double.infinity,
          height: freeToMakeFriend ? 230 : 260 + Grid.columns(context, 2),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
          ),
          child: _mainContent(context, freeToMakeFriend: freeToMakeFriend),
        ),
        Positioned(
          top: Grid.columns(context, .6),
          right: Grid.columns(context, 1.6),
          child: _closeButton(context),
        )
      ],
    );
  }

  Widget _closeButton(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.pop(context);
        if (widget.onCancel != null) widget.onCancel();
      },
      child: Container(
        padding: EdgeInsets.all(Grid.columns(context, 2.5) / 3),
        decoration: BoxDecoration(
          color: Color(0xfff4f4f4).withOpacity(.5),
          shape: BoxShape.circle,
        ),
        width: Grid.columns(context, 2.5),
        height: Grid.columns(context, 2.5),
        child: NuImage(
          path: "assets/images/common/close_ic.png",
        ),
      ),
    );
  }

  Widget _mainContent(BuildContext context, {bool freeToMakeFriend = false}) {
    final _contentStyle = TextStyle(
      fontSize: 12,
    );
    final me = getIt<MeProvider>().user.value;

    content() => freeToMakeFriend
        ? NuText(
            i18n: "friend.description",
            style: _contentStyle,
            textAlign: TextAlign.center,
          )
        : Row(
            crossAxisAlignment: CrossAxisAlignment.baseline,
            textBaseline: TextBaseline.ideographic,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              NuText(
                text: "80 ",
                style: _contentStyle,
              ),
              NuText(
                i18n: "leaederboard.popup.confirm.content",
                style: _contentStyle,
              ),
            ],
          );

    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        if (!freeToMakeFriend)
          NuImage(
            path: "assets/images/coins.png",
            height: Grid.columns(context, 3),
          ),
        if (!freeToMakeFriend) SizedBox(height: 10),
        NuText(
          i18n: "button.friend.add",
          capitalize: true,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 16,
          ),
        ),
        SizedBox(height: 20),
        content(),
        SizedBox(height: 30),
        _buttonBuilder(context, freeToMakeFriend: freeToMakeFriend),
      ],
    );
  }

  Widget _buttonBuilder(BuildContext context, {bool freeToMakeFriend = false}) {
    return GestureDetector(
      onTap: () async {
        if (loading) return;

        setState(() {
          loading = true;
        });

        final mePvdr = getIt<MeProvider>();

        try {
          final res = widget.accept
              ? await FriendService.accept(userId: this.widget.user.id)
              : await FriendService.create(receiverId: this.widget.user.id);

          if (res) {
            if (!freeToMakeFriend) mePvdr.fetch();
            mePvdr.getRelationShip(callback: () {
              setState(() {
                loading = false;
              });
              if (widget.onOk != null) widget.onOk();
              Navigator.pop(context);
            });
            getIt<CommunicateService>()
                .request(requestName: "reload-friends-list", args: {});
          } else {
            setState(() {
              loading = false;
            });
            _showErrorPopup();
          }
        } on TimeoutException catch (_) {
          setState(() {
              loading = false;
            });
            _showErrorPopup();
        } catch (e) {
          print(e);
        }
      },
      child: Container(
        height: Grid.columns(context, 2.7),
        width: Grid.columns(context, 10),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [Color(0xff4DFF8B), Color(0xff16D087)],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
          ),
          borderRadius: BorderRadius.circular(100),
        ),
        child: NuText(
          i18n: loading
              ? "button.loading"
              : freeToMakeFriend
                  ? "friend.confirm"
                  : "leaederboard.popup.confirm.button.label",
          uppercase: true,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 13,
            color: Colors.white,
          ),
        ),
      ),
    );
  }

  void _showErrorPopup() {
    showCenterPopup(
      context: widget.parentContext,
      width: Grid.columns(context, 22),
      height: 160,
      isDismissible: true,
      content: LeaderboardErrorPopup(),
      actions: [
        {
          "title": "OK",
        },
      ],
    );
  }
}

import 'package:flutter/material.dart';
import 'package:nu/src/widgets/core/text.widget.dart';

class LeaderboardErrorPopup extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(20.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          NuText(
            i18n: "response.error.common",
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:nu/dependency-injection.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/widgets/core/image.widget.dart';
import 'package:nu/src/widgets/core/text.widget.dart';

class LeaderBoardRankWidget extends StatelessWidget {
  final int rank;
  final bool loading;

  LeaderBoardRankWidget({this.rank, this.loading = false});

  Widget build(BuildContext context) {
    final _textStyle = TextStyle(
      color: Colors.white,
      fontWeight: FontWeight.bold,
      fontSize: 12,
      shadows: <Shadow>[
        Shadow(
          offset: Offset(0, 1),
          blurRadius: 1,
          color: Color.fromRGBO(0, 0, 0, .25),
        ),
      ],
    );

    if (this.loading)
      return Container(
        width: Grid.columns(context, 1.5),
        alignment: Alignment.center,
        child: NuText(
          text: "--",
          style: _textStyle.merge(TextStyle(
            color: Color(0xff8918E2),
            shadows: [],
          )),
        ),
      );

    switch (rank) {
      case 1:
        return Stack(
          alignment: Alignment.center,
          children: [
            NuImage(
              width: Grid.columns(context, 1.5),
              path: "assets/images/badges/first_rank_badge.png",
              fit: BoxFit.fitWidth,
            ),
            Positioned(
              top: Grid.columns(context, .12),
              child: NuText(
                text: "$rank",
                style: _textStyle,
              ),
            ),
          ],
        );
      case 2:
        return Stack(
          alignment: Alignment.center,
          children: [
            NuImage(
              width: Grid.columns(context, 1.5),
              path: "assets/images/badges/second_rank_badge.png",
              fit: BoxFit.fitWidth,
            ),
            Positioned(
              top: Grid.columns(context, .12),
              child: NuText(
                text: "$rank",
                style: _textStyle,
              ),
            ),
          ],
        );
      case 3:
        return Stack(
          alignment: Alignment.center,
          children: [
            NuImage(
              width: Grid.columns(context, 1.5),
              path: "assets/images/badges/third_rank_badge.png",
              fit: BoxFit.fitWidth,
            ),
            Positioned(
              top: Grid.columns(context, .12),
              child: NuText(
                text: "$rank",
                style: _textStyle,
              ),
            ),
          ],
        );

      default:
        return Container(
          width: Grid.columns(context, 1.5),
          alignment: Alignment.center,
          child: NuText(
            text: "${rank > 0 && rank <= 50 ? rank : "--"}",
            style: _textStyle.merge(TextStyle(
              color: Color(0xff8918E2),
              shadows: [],
            )),
          ),
        );
    }
  }
}

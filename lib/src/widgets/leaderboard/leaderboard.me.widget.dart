import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:nu/dependency-injection.dart';
import 'package:nu/src/modules/number.module.dart';
import 'package:nu/src/providers/leaderboard.provider.dart';
import 'package:nu/src/providers/me.provider.dart';
import 'package:nu/src/utils/constants.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/utils/utils.dart';
import 'package:nu/src/widgets/core/image.widget.dart';
import 'package:nu/src/widgets/core/text.widget.dart';
import 'package:nu/src/widgets/leaderboard/leaderboard.rank.widget.dart';

class LeaderBoardMeWidget extends StatefulWidget {
  final String type;
  final double width;
  final bool fixed;

  LeaderBoardMeWidget({this.type, this.width, this.fixed = true});

  @override
  _LeaderBoardMeWidgetState createState() => _LeaderBoardMeWidgetState();
}

class _LeaderBoardMeWidgetState extends State<LeaderBoardMeWidget> {
  void initState() {
    super.initState();
  }

  Widget build(BuildContext context) {
    //Male user in tab 'Hot Idol'
    if (widget.type == Constants.leaderboardType.receiveGem &&
        !getIt<MeProvider>().user.value.isFemale()) return SizedBox();

    //Female user in tab 'Top Buyer'
    if (widget.type == Constants.leaderboardType.coin &&
        getIt<MeProvider>().user.value.isFemale()) return SizedBox();

    //Female user in tab 'Top Gifts Sender'
    if (widget.type == Constants.leaderboardType.sendGift &&
        getIt<MeProvider>().user.value.isFemale()) return SizedBox();

    return AnimatedBuilder(
      animation: Listenable.merge([
        getIt<LeaderboardProvider>().myGlobalRank,
        getIt<LeaderboardProvider>().myGlobalRankLoading,
      ]),
      builder: (ctx, _) {
        return Container(
          width: widget.width ?? Grid.columns(context, 24),
          margin: EdgeInsets.symmetric(vertical: 5),
          padding: EdgeInsets.symmetric(
            horizontal: !widget.fixed ? 10 : Grid.columns(context, 1) + 10,
            vertical: 14,
          ),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [Color(0xffF4FEFF), Color(0xffE9EFFF)],
            ),
            borderRadius: BorderRadius.circular(6),
            boxShadow: !widget.fixed
                ? []
                : [
                    BoxShadow(
                      color: Color.fromRGBO(0, 0, 0, .07),
                      blurRadius: 3,
                    )
                  ],
          ),
          child: Row(
            children: [
              LeaderBoardRankWidget(
                rank: getIt<LeaderboardProvider>().myGlobalRank.value.rank,
                loading: getIt<LeaderboardProvider>().myGlobalRankLoading.value,
              ),
              SizedBox(width: 20),
              _leaderboardItemInfo(context),
              Spacer(),
              _leaderboardItemPoint(context),
            ],
          ),
        );
      },
    );
  }

  Widget _leaderboardItemInfo(BuildContext context) {
    final mePvdr = getIt<MeProvider>();

    return Row(
      children: [
        CircleAvatar(
          radius: Grid.columns(context, 1.4),
          backgroundColor: Color(0xffd5d5d5),
          backgroundImage:
              CachedNetworkImageProvider(mePvdr.user.value.avatar.url ?? ""),
        ),
        SizedBox(width: 10),
        SizedBox(
          width: Grid.columns(context, widget.fixed ? 3.5 : 7),
          child: NuText(
            capitalize: true,
            i18n: widget.fixed ? "leaderboard.me" : null,
            text: Utils.clearZeroWidthSpace(mePvdr.user.value.name),
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 11.5,
            ),
          ),
        ),
      ],
    );
  }

  Widget _leaderboardItemPoint(BuildContext context) {
    if(widget.type == Constants.leaderboardType.coin) return SizedBox();

    final leaderBoardPvdr = getIt<LeaderboardProvider>();
    final loading = leaderBoardPvdr.myGlobalRankLoading.value;
    final rank = leaderBoardPvdr.myGlobalRank.value.rank;

    if (loading) {
      return Container(
        width: Grid.columns(context, 6.5) + 10,
        alignment: Alignment.center,
        child: NuText(
          textAlign: TextAlign.center,
          text: "--",
          style: TextStyle(
            fontSize: 10.5,
            fontWeight: FontWeight.bold,
          ),
        ),
      );
    }

    if (rank > 50 || rank == 0) {
      return Container(
        width: Grid.columns(context, 10),
        child: NuText(
          textAlign: TextAlign.center,
          i18n: "leaderboard.rankout",
          style: TextStyle(
            fontSize: 10.5,
            fontStyle: FontStyle.italic,
            fontWeight: FontWeight.bold,
          ),
        ),
      );
    }

    return Container(
      width: Grid.columns(context, 6.5) + 10,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          _leaderBoardItemIcon(context),
          SizedBox(width: 7),
          NuText(
            text: NumberModule.format(leaderBoardPvdr.myGlobalRank.value.point),
            style: TextStyle(
              color: Color(0xff424242),
              fontSize: 12,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }

  Widget _leaderBoardItemIcon(BuildContext context) {
    String path = "";

    if (widget.type == Constants.leaderboardType.coin)
      path = "assets/images/coins.png";
    if (widget.type == Constants.leaderboardType.receiveGem) {
      if (getIt<MeProvider>().user.value.isFemale())
        path = "assets/images/gem.png";
      else
        path = "assets/images/idol-star.png";
    }
    if (widget.type == Constants.leaderboardType.receiveGift)
      path = "assets/images/gift-received.png";
    if (widget.type == Constants.leaderboardType.sendGift)
      path = "assets/images/gift-sent.png";

    if (Utils.isEmpty(path)) return SizedBox();

    return NuImage(
      path: path,
      width: Grid.columns(context, 1.2),
    );
  }
}

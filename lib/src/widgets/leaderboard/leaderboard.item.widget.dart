import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:nu/dependency-injection.dart';
import 'package:nu/src/models/leaderboard.item.model.dart';
import 'package:nu/src/modules/number.module.dart';
import 'package:nu/src/providers/me.provider.dart';
import 'package:nu/src/services/communicate.service.dart';
import 'package:nu/src/utils/constants.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/utils/utils.dart';
import 'package:nu/src/widgets/core/center-popup.widget.dart';
import 'package:nu/src/widgets/core/image.widget.dart';
import 'package:nu/src/widgets/core/text.widget.dart';
import 'package:nu/src/widgets/leaderboard/leaderboard.me.widget.dart';
import 'package:nu/src/widgets/leaderboard/leaderboard.rank.widget.dart';
import 'package:nu/src/widgets/leaderboard/popup/leaderboard.confirm.dart';
import 'package:nu/src/widgets/leaderboard/popup/leaderboard.outOfCoin.dart';

class LeaderboardItemWidget extends StatefulWidget {
  final LeaderboardItem data;
  final BuildContext parentContext;

  const LeaderboardItemWidget(
    this.parentContext, {
    Key key,
    this.data,
  }) : super(key: key);

  @override
  _LeaderboardItemWidgetState createState() => _LeaderboardItemWidgetState();
}

class _LeaderboardItemWidgetState extends State<LeaderboardItemWidget> {
  @override
  Widget build(BuildContext context) {
    if (widget.data.user.id == getIt<MeProvider>().user.value.id) {
      return LeaderBoardMeWidget(
        type: widget.data.type,
        width: Grid.columns(context, 22),
        fixed: false,
      );
    }

    return Container(
      margin: EdgeInsets.symmetric(vertical: 5),
      padding: EdgeInsets.symmetric(
        horizontal: 10,
        vertical: 14,
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(6),
      ),
      child: Row(
        children: [
          LeaderBoardRankWidget(rank: widget.data.rank),
          SizedBox(width: 20),
          _leaderboardItemInfo(context),
          Spacer(),
          _leaderboardItemAction(context),
        ],
      ),
    );
  }

  Widget _leaderboardItemInfo(BuildContext context) {
    final friendStatus = Utils.getRelationship(widget.data.user.id);
    final isBlockedOrBlocking =
        friendStatus == Constants.friendStatus.blocking ||
            friendStatus == Constants.friendStatus.blocked;
    final isMe = widget.data.user.id == getIt<MeProvider>().user.value.id;

    return Row(
      children: [
        GestureDetector(
          onTap: () {
            if (friendStatus == Constants.friendStatus.blocked ||
                friendStatus == Constants.friendStatus.blocking) {
              showCenterPopup(
                context: widget.parentContext,
                content: Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.symmetric(
                    horizontal: Grid.columns(context, 1),
                  ),
                  child: NuText(
                    textAlign: TextAlign.center,
                    i18n: friendStatus == Constants.friendStatus.blocking
                        ? "leaderboard.profile.blocked"
                        : "leaderboard.profile.blocking",
                    style: TextStyle(
                      fontSize: 14,
                    ),
                  ),
                ),
                width: Grid.columns(context, 20),
                isDismissible: true,
                height: 160,
                actions: [
                  {
                    "title": "OK",
                  }
                ],
              );
            } else {
              getIt<CommunicateService>()
                  .navigate(route: "/user-profile", args: {
                "user-id": widget.data.user.id,
                "is-friend": friendStatus == Constants.friendStatus.friend
              });
            }
          },
          child: CircleAvatar(
            radius: Grid.columns(context, 1.4),
            backgroundColor: Color(0xffd5d5d5),
            backgroundImage:
                CachedNetworkImageProvider(widget.data.user.avatar.url ?? ""),
          ),
        ),
        SizedBox(width: 10),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              width:
                  Grid.columns(context, isBlockedOrBlocking || isMe ? 10 : 7),
              child: NuText(
                capitalize: true,
                i18n: isBlockedOrBlocking ? "friend.blocked" : null,
                text: Utils.clearZeroWidthSpace(widget.data.user.name),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 11.5,
                ),
              ),
            ),
            SizedBox(height: 5),
            _leadrderboardItemPoint(context),
          ],
        ),
      ],
    );
  }

  Widget _leadrderboardItemPoint(BuildContext context) {
    if (widget.data.type == Constants.leaderboardType.coin) return SizedBox();

    return Row(
      children: [
        _leaderBoardItemIcon(context),
        SizedBox(width: 7),
        NuText(
          text: NumberModule.format(widget.data.point),
          style: TextStyle(color: Color(0xff424242), fontSize: 12),
        ),
      ],
    );
  }

  Widget _leaderBoardItemIcon(BuildContext context) {
    String path = "";

    if (this.widget.data.type == Constants.leaderboardType.coin)
      path = "assets/images/coins.png";
    if (this.widget.data.type == Constants.leaderboardType.receiveGem) {
      if (getIt<MeProvider>().user.value.isFemale())
        path = "assets/images/gem.png";
      else
        path = "assets/images/idol-star.png";
    }
    if (this.widget.data.type == Constants.leaderboardType.receiveGift)
      path = "assets/images/gift-received.png";
    if (this.widget.data.type == Constants.leaderboardType.sendGift)
      path = "assets/images/gift-sent.png";

    if (Utils.isEmpty(path)) return SizedBox();

    return NuImage(
      path: path,
      width: Grid.columns(context, 1.2),
    );
  }

  Widget _leaderboardItemAction(BuildContext context) {
    final friendStatus = Utils.getRelationship(widget.data.user.id);
    if (widget.data.user.id == getIt<MeProvider>().user.value.id)
      return SizedBox();

    if (friendStatus == Constants.friendStatus.stranger) {
      return GestureDetector(
        onTap: () {
          final mePvdr = getIt<MeProvider>();

          if (mePvdr.user.value.isFemale() || mePvdr.user.value.coins >= 80) {
            _showConfirmPopUp(context);
          } else {
            _showOutOfCoinPopUp(context);
          }
        },
        child: Container(
          width: Grid.columns(context, 7),
          alignment: Alignment.center,
          padding: EdgeInsets.symmetric(
            vertical: 10,
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(60),
            border: Border.all(
              width: 1,
              color: Color(0xffd5d5d5),
            ),
          ),
          child: NuText(
            i18n: "button.friend.add",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: Color(0xff424242),
              fontSize: 10.5,
            ),
          ),
        ),
      );
    }

    if (friendStatus == Constants.friendStatus.friend) {
      return Container(
        width: Grid.columns(context, 7),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            NuImage(
              path: "assets/images/tick-green.png",
              width: Grid.columns(context, 1),
            ),
            SizedBox(width: 8),
            NuText(
              i18n: "friend.friend",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 11,
              ),
            ),
          ],
        ),
      );
    }

    if (friendStatus == Constants.friendStatus.sent) {
      return Container(
        width: Grid.columns(context, 7),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            NuText(
              i18n: "friend.pending",
              style: TextStyle(
                fontSize: 10,
                fontStyle: FontStyle.italic,
                color: Color(0xff7f7f7f),
              ),
            ),
          ],
        ),
      );
    }

    if (friendStatus == Constants.friendStatus.received) {
      return GestureDetector(
        onTap: () {
          final mePvdr = getIt<MeProvider>();
          bool freeToAcceptFriend = false;
          if (mePvdr.user.value.isFemale()) freeToAcceptFriend = true;
          if (!mePvdr.user.value.isFemale() && !widget.data.user.isFemale())
            freeToAcceptFriend = true;

          if (!freeToAcceptFriend && mePvdr.user.value.coins < 80) {
            _showOutOfCoinPopUp(context);
          } else {
            _showConfirmPopUp(context, accept: true);
          }
        },
        child: Container(
          width: Grid.columns(context, 7),
          alignment: Alignment.center,
          padding: EdgeInsets.symmetric(
            vertical: 10,
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(60),
            border: Border.all(
              width: 1,
              color: Color(0xff16D087).withOpacity(.8),
            ),
          ),
          child: NuText(
            i18n: "friend.received",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: Color(0xff16D087),
              fontSize: 10.5,
            ),
          ),
        ),
      );
    }

    return SizedBox();
  }

  void _showOutOfCoinPopUp(BuildContext context) {
    showModalBottomSheet(
      isScrollControlled: true,
      barrierColor: Colors.black.withOpacity(.3),
      context: context,
      backgroundColor: Colors.transparent,
      builder: (_) => LeaderboardOutOfCoinPopup(),
    );
  }

  void _showConfirmPopUp(BuildContext context, {bool accept = false}) {
    showModalBottomSheet(
      isScrollControlled: true,
      barrierColor: Colors.black.withOpacity(.3),
      context: context,
      backgroundColor: Colors.transparent,
      builder: (_) => LeaderboardConfirmPopup(
        widget.parentContext,
        user: widget.data.user,
        accept: accept,
      ),
    );
  }
}

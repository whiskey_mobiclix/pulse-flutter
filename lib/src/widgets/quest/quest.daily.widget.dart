import 'package:flutter/material.dart';
import 'package:nu/src/models/quest.daily.model.dart';
import 'package:nu/src/screens/event/Idol-event-screen-body/progress-bar.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/widgets/core/image.widget.dart';
import 'package:nu/src/widgets/core/text.widget.dart';

class DailyQuestWidget extends StatelessWidget {
  final DailyQuest data;

  DailyQuestWidget({this.data});

  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(5),
      padding: EdgeInsets.symmetric(
        horizontal: Grid.columns(context, 1),
        vertical: 10,
      ),
      decoration: BoxDecoration(
        color: Color(0xfff7f7f7),
        borderRadius: BorderRadius.all(Radius.circular(10)),
        boxShadow: [
          BoxShadow(
            color: Color.fromRGBO(0, 0, 0, .16),
            spreadRadius: 0,
            blurRadius: 4,
          )
        ],
      ),
      child: Row(
        children: [
          _dailyQuestIcon(context),
          SizedBox(width: 10),
          _dailyQuestProgress(context),
          Spacer(),
          _dailyQuestBonus(context),
        ],
      ),
    );
  }

  Widget _dailyQuestIcon(BuildContext context) {
    return Container(
      width: Grid.columns(context, 3),
      height: Grid.columns(context, 3),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        boxShadow: [
          BoxShadow(
            color: Color.fromRGBO(0, 0, 0, .08),
          ),
          BoxShadow(
            color: Colors.white,
            spreadRadius: -2,
            blurRadius: 4,
          ),
        ],
      ),
      child: NuImage(
        path: this.data.icon,
      ),
    );
  }

  Widget _dailyQuestProgress(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        NuText(
          text: this.data.name,
          style: TextStyle(
            fontSize: 11,
            color: Colors.black,
          ),
        ),
        SizedBox(height: 6),
        ProgressBar(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [Color(0xff4DFF8B), Color(0xff16D087)],
          ),
          threshold: this.data.goal.toDouble(),
          value: this.data.earned.toDouble(),
          width: Grid.columns(context, 10),
          ratio: 10,
          border: Border.all(
            color: Color(0xffececec),
            width: 2,
          ),
          textStyle: TextStyle(
            color: Colors.black,
          ),
        ),
      ],
    );
  }

  Widget _dailyQuestBonus(BuildContext context) {
    return Column(
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            NuImage(
              path: "assets/images/gem.png",
              width: Grid.columns(context, 1.4),
            ),
            NuText(
              text: " x${this.data.bonusGems}",
              style: TextStyle(
                color: Colors.black,
                fontSize: 10,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
        SizedBox(height: 5),
        GestureDetector(
          child: Container(
            padding: EdgeInsets.symmetric(
              vertical: 3,
              horizontal: 12,
            ),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(25)),
                border: Border.all(
                  color: Color(0xff26D57D),
                  width: 1,
                )),
            child: NuText(
              i18n: "button.do-it",
              style: TextStyle(
                fontSize: 11,
                fontWeight: FontWeight.bold,
                color: Color(0xff26D57D),
              ),
            ),
          ),
        )
      ],
    );
  }
}

import 'package:flutter/material.dart';
import 'package:nu/src/models/quest.reward.model.dart';
import 'package:nu/src/screens/event/Idol-event-screen-body/progress-bar.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/widgets/core/image.widget.dart';
import 'package:nu/src/widgets/core/text.widget.dart';
import 'package:nu/src/widgets/core/timer.widget.dart';

class DailyQuestRewardWidget extends StatelessWidget {
  final QuestReward data;

  DailyQuestRewardWidget({this.data});

  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: 10,
        horizontal: Grid.columns(context, 1.5),
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        color: Color(0xfff7f7f7),
        boxShadow: [
          BoxShadow(
            color: Color.fromRGBO(0, 0, 0, .16),
            spreadRadius: 0,
            blurRadius: 4,
          )
        ],
      ),
      child: Column(
        children: [
          _questRewardTitle(),
          SizedBox(height: 15),
          _questRewardBonus(context),
          SizedBox(height: 10),
          _questRewardProgress(context),
          SizedBox(height: 20),
          _questRewardButton(),
          SizedBox(height: 10),
          _questRewardTimer(),
        ],
      ),
    );
  }

  Widget _questRewardTitle() {
    return NuText(
      i18n: "quest.reward.title",
      textAlign: TextAlign.center,
      style: TextStyle(
        fontSize: 12,
        fontWeight: FontWeight.bold,
      ),
    );
  }

  Widget _questRewardBonus(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          NuImage(
            path: "assets/images/gem.png",
            width: Grid.columns(context, 1.6),
          ),
          NuText(
            text: " x${this.data.bonusGems}",
            style: TextStyle(
              fontSize: 11,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }

  Widget _questRewardProgress(BuildContext context) {
    return ProgressBar(
      gradient: LinearGradient(
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
        colors: [Color(0xff4DFF8B), Color(0xff16D087)],
      ),
      threshold: this.data.goal.toDouble(),
      value: this.data.completed.toDouble(),
      width: Grid.columns(context, 20),
      ratio: 18,
      border: Border.all(
        color: Color(0xffececec),
        width: 2,
      ),
      i18n: "quest.normal",
      textStyle: TextStyle(
        color: Colors.black,
      ),
    );
  }

  Widget _questRewardButton() {
    return GestureDetector(
      child: Container(
        width: double.infinity,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: Colors.black,
          borderRadius: BorderRadius.circular(25),
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [Color(0xff4DFF8B), Color(0xff16D087)],
          ),
        ),
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 12),
        child: NuText(
          i18n: "button.do-quest",
          uppercase: true,
          style: TextStyle(
            fontSize: 12,
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),
        ),
      ),
    );
  }

  Widget _questRewardTimer() {
    final _style = TextStyle(
      fontSize: 10,
      color: Colors.black.withOpacity(.8),
    );
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        NuText(
          i18n: "quest.reset",
          style: _style,
        ),
        SizedBox(width: 3),
        NuTimer(
          milliseconds: 1000 * 60,
          style: _style,
        ),
        SizedBox(width: 3),
        NuText(
          i18n: "quest.left",
          style: _style,
        ),
      ],
    );
  }
}

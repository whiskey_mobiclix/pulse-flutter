import 'package:flutter/material.dart';
import 'package:nu/dependency-injection.dart';
import 'package:nu/src/layouts/layout.dart';
import 'package:nu/src/providers/me.provider.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/utils/utils.dart';
import 'package:nu/src/widgets/core/image.widget.dart';
import 'package:nu/src/widgets/core/text.widget.dart';

class ProfileIntroductionWidget extends StatelessWidget {
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        NuLayout(
          padding: EdgeInsets.symmetric(
              horizontal: Grid.columns(context, 1), vertical: 30),
          child: Container(
            height: MediaQuery.of(context).size.height,
            child: Column(
              children: [
                NuImage(
                  path: "assets/images/logo.png",
                  width: Grid.columns(context, 3.5),
                ),
                SizedBox(height: 20),
                _IntroductionTitle(context),
                SizedBox(height: 30),
                _IntroductionItem(
                  context,
                  icon: "assets/images/level.png",
                  titleI18nKey: "profile.level",
                  descriptionI18nKey: "profile.level.description",
                ),
                _IntroductionItemDivider(),
                _IntroductionItem(
                  context,
                  icon: "assets/images/wallet.png",
                  titleI18nKey: "profile.coins",
                  descriptionI18nKey: "profile.coins.description",
                ),
                _IntroductionItemDivider(),
                _IntroductionItem(
                  context,
                  icon: "assets/images/store.png",
                  titleI18nKey: "profile.store",
                  descriptionI18nKey: "profile.store.description",
                ),
                _IntroductionItemDivider(),
                _IntroductionItem(
                  context,
                  icon: "assets/images/leaderboard.png",
                  titleI18nKey: "profile.leaderboard",
                  descriptionI18nKey: "profile.leaderboard.description",
                ),
                SizedBox(height: 20),
              ],
            ),
          ),
        ),
        Positioned(
          bottom: 20,
          child: _IntroductionStartButton(context),
        )
      ],
    );
  }

  Widget _IntroductionTitle(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: Grid.columns(context, 2)),
      child: NuText(
        i18n: "profile.introduction.title",
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: 16,
        ),
      ),
    );
  }

  Widget _IntroductionItem(BuildContext context,
      {String icon, String titleI18nKey, String descriptionI18nKey}) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 15),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          NuImage(
            path: icon,
            width: Grid.columns(context, 2),
          ),
          SizedBox(width: 15),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: Grid.columns(context, .2)),
              NuText(
                i18n: titleI18nKey,
                style: TextStyle(
                  fontSize: 13,
                  fontWeight: FontWeight.bold,
                ),
              ),
              if (!Utils.isEmpty(descriptionI18nKey))
                Container(
                  margin: EdgeInsets.only(top: 8),
                  child: NuText(
                    i18n: descriptionI18nKey,
                    style: TextStyle(
                      fontSize: 11,
                    ),
                  ),
                ),
            ],
          )
        ],
      ),
    );
  }

  Widget _IntroductionItemDivider() {
    return Container(
      color: Color(0xff707070).withOpacity(.2),
      height: 1,
    );
  }

  Widget _IntroductionStartButton(BuildContext context) {
    return GestureDetector(
      onTap: () {
        getIt<MeProvider>()
            .addUserIdAsSeenProfileIntro(getIt<MeProvider>().user.value.id);
      },
      child: SafeArea(
        child: Container(
            alignment: Alignment.center,
            padding: EdgeInsets.symmetric(vertical: 15),
            width: Grid.columns(context, 20),
            decoration: BoxDecoration(
              color: Color(0xff26d57d),
              borderRadius: BorderRadius.all(Radius.circular(25)),
            ),
            child: NuText(
              i18n: "button.let-start",
              uppercase: true,
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 13,
              ),
            )),
      ),
    );
  }
}

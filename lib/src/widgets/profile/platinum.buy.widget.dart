import 'package:flutter/material.dart';
import 'package:nu/dependency-injection.dart';
import 'package:nu/src/services/communicate.service.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/widgets/core/image.widget.dart';
import 'package:nu/src/widgets/core/text.widget.dart';

class BuyPlatinumButtonWidget extends StatelessWidget {
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        getIt<CommunicateService>().navigate(
            route: "/store", args: {"selected": "", "type": "platinum"});
      },
      child: Row(
        children: [
          NuImage(
            path: "assets/images/diamond-icon.png",
            width: Grid.columns(context, 1.3),
          ),
          SizedBox(width: 3),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 12, vertical: 4),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(25)),
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [
                  Color(0xfff8cb18),
                  Color(0xffd6ab08),
                ],
              ),
            ),
            child: NuText(
              i18n: "profile.platinum.buy",
              style: TextStyle(
                fontSize: 10,
                fontWeight: FontWeight.bold,
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

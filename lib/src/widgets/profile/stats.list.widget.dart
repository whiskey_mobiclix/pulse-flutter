import 'package:flutter/material.dart';
import 'package:nu/src/models/stat.model.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/widgets/profile/stat.widget.dart';

class StatsListWidget extends StatelessWidget {
  List<Stat> stats;

  StatsListWidget({List<Stat> stats}) {
    this.stats = stats ?? [];
  }

  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: Grid.columns(context, 1)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: this.stats.map((e) => StatWidget(data: e)).toList(),
      ),
    );
  }
}

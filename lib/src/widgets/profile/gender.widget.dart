import 'package:flutter/material.dart';
import 'package:nu/src/utils/constants.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/widgets/core/image.widget.dart';

class ProfileGenderWidget extends StatelessWidget {
  int gender;

  ProfileGenderWidget({int gender}) {
    this.gender = gender ?? Constants.gender.male;
  }

  Widget build(BuildContext context) {
    return NuImage(
      path:
          "assets/images/${this.gender == Constants.gender.male ? "male" : "female"}.png",
      width:
          Grid.columns(context, this.gender == Constants.gender.male ? .8 : .55),
    );
  }
}

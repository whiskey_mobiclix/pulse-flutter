import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:nu/src/models/avatar.model.dart';
import 'package:nu/src/models/level.model.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/utils/utils.dart';
import 'package:nu/src/widgets/core/image.widget.dart';
import 'package:nu/src/widgets/core/text.widget.dart';

class ProfileAvatarWidget extends StatelessWidget {
  Avatar avatar;
  Level level;

  ProfileAvatarWidget({
    Avatar avatar,
    Level level,
  }) {
    this.avatar = avatar ?? new Avatar();
    this.level = level ?? new Level({});
  }

  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      overflow: Overflow.visible,
      children: [
        Positioned(
          top: 1.0 * Grid.columns(context, 1.2),
          child: CircleAvatar(
            backgroundImage: !Utils.isEmpty(this.avatar.url)
                ? NetworkImage(this.avatar.url)
                : AssetImage("assets/images/avatar.png"),
            backgroundColor: Color(0xffeeeeee),
            radius: Grid.columns(context, 1.65),
          ),
        ),
        NuImage(
          path: !Utils.isEmpty(this.avatar.frame)
              ? this.avatar.frame
              : "assets/images/frames/gold.png",
          width: Grid.columns(context, 5.5),
        ),
        if (this.level.value > 0)
          Positioned(
            bottom: 0,
            child: Container(
              width: Grid.columns(context, 3.5),
              alignment: Alignment.center,
              child: NuText(
                text: "Lv. ${this.level.value}",
                style: TextStyle(
                  fontSize: 7.5,
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
      ],
    );
  }
}

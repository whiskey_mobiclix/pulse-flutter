import 'package:flutter/material.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/widgets/core/text.widget.dart';

class FrameItemWidget extends StatelessWidget {
  String thumbnail;
  String nameI18nKey;
  String description;
  double width;

  FrameItemWidget({
    this.thumbnail,
    this.nameI18nKey,
    this.description,
    this.width = 80,
  });

  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.symmetric(vertical: 10),
        child: Column(
          children: [
            Image.asset(
              this.thumbnail,
              width: this.width,
              fit: BoxFit.cover,
            ),
            SizedBox(height: 10),
            NuText(
                i18n: this.nameI18nKey,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 11,
                )),
            Container(
              margin: EdgeInsets.only(top: 2),
              child: NuText(
                  text: this.description != null ? this.description : " ",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 10,
                  )),
            )
          ],
        ));
  }
}

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:nu/src/models/property.model.dart';
import 'package:nu/src/widgets/core/text.widget.dart';

class PropertyItemWidget extends StatelessWidget {
  TextStyle style;
  double width;
  VoidCallback onTap;
  Property data;

  PropertyItemWidget({
    this.data,
    this.width,
    this.onTap,
    this.style,
  });

  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          if (this.onTap != null) {
            this.onTap();
          }
        },
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 10),
          child: Column(children: [
            Container(
              width: this.width ?? 100,
              height: (this.width ?? 100) / 1.62,
              child: Image.network(
                this.data.thumbnail,
                width: this.width ?? 100,
              ),
            ),
            NuText(
              text: this.data.name,
              textAlign: TextAlign.center,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 11,
              ).merge(this.style),
            ),
          ]),
        ));
  }
}

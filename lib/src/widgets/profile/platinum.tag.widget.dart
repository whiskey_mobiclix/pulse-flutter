import 'package:flutter/material.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/widgets/core/image.widget.dart';
import 'package:nu/src/widgets/core/text.widget.dart';

class PlatinumTagWidget extends StatelessWidget {
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        NuImage(
          path: "assets/images/diamond-icon.png",
          width: Grid.columns(context, 1.1),
        ),
        SizedBox(width: 5),
        NuText(
          i18n: "tag.platinum",
          style: TextStyle(
            fontSize: 10,
            fontWeight: FontWeight.bold,
            color: Color(0xffd6ab08),
          ),
        ),
      ],
    );
  }
}

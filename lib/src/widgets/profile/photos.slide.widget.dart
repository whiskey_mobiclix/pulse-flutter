import 'package:flutter/material.dart';
import 'package:nu/src/utils/grid.dart';

class PhotosSlideWidget extends StatelessWidget {
  List<String> photos;
  Function(String) onSelect;

  PhotosSlideWidget({
    List<String> photos,
    Function(String) onSelect,
  }) {
    this.photos = photos ?? [];
    this.onSelect = onSelect;
  }

  Widget build(BuildContext context) {
    if (this.photos.isEmpty) return Container();

    List<Widget> children = [SizedBox(width: Grid.columns(context, 1))];
    children.addAll(
        this.photos.map((e) => _PhotoSlideItem(context, path: e)).toList());

    return Container(
      width: double.infinity,
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Padding(
          padding: EdgeInsets.only(right: Grid.columns(context, 1) - 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: children,
          ),
        ),
      ),
    );
  }

  Widget _PhotoSlideItem(BuildContext context, {String path}) {
    return GestureDetector(
      onTap: () {
        if (this.onSelect != null) this.onSelect(path);
      },
      child: Container(
        margin: EdgeInsets.only(
          right: 10,
        ),
        width: Grid.columns(context, 7.5),
        height: Grid.columns(context, 7.5),
        decoration: new BoxDecoration(
            color: Color(0xffeeeeee),
            borderRadius: new BorderRadius.circular(10),
            image:
            DecorationImage(image: NetworkImage(path), fit: BoxFit.cover)),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class PhotoPreview extends StatefulWidget {
  String url;
  VoidCallback onCancel;

  PhotoPreview({
    String url,
    VoidCallback onCancel,
  }) {
    this.url = url;
    this.onCancel = onCancel;
  }

  _PhotoPreview createState() => _PhotoPreview();
}

class _PhotoPreview extends State<PhotoPreview> with TickerProviderStateMixin {
  double scale = 0.0;
  AnimationController scaleController;

  void initState() {
    scaleController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 250),
      value: 1.0,
    )..addListener(() {
        setState(() {
          scale = scaleController.value;
        });
      });

    scaleController.forward(from: 0.0);

    super.initState();
  }

  void dispose() {
    scaleController.dispose();
    super.dispose();
  }

  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);

    void cancel() {
      if (widget.onCancel != null) {
        scaleController.reverse();

        new Future.delayed(const Duration(milliseconds: 400), () {
          widget.onCancel();
          SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
        });
      }
    }

    return Stack(alignment: Alignment.center, children: [
      GestureDetector(
          onTap: () {
            cancel();
          },
          child: Container(
            width: double.infinity,
            height: double.infinity,
            color: Color.fromRGBO(0, 0, 0, .95),
          )),
      Transform.scale(
          scale: scale,
          child: Image.network(
            widget.url,
          )),
      Positioned(
        right: 20,
        top: 20,
        child: GestureDetector(
          onTap: () {
            cancel();
          },
          child: Container(
            padding: EdgeInsets.all(8),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(50)),
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.2),
                  spreadRadius: 5,
                  blurRadius: 7,
                  offset: Offset(0, 3), // changes position of shadow
                ),
              ],
            ),
            child:
                Image.asset("assets/images/close.png", width: 15, height: 15),
          ),
        ),
      )
    ]);
  }
}

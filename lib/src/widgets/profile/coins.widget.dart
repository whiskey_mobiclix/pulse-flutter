import 'package:flutter/material.dart';
import 'package:nu/dependency-injection.dart';
import 'package:nu/src/modules/number.module.dart';
import 'package:nu/src/services/communicate.service.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/widgets/core/image.widget.dart';
import 'package:nu/src/widgets/core/text.widget.dart';

class ProfileCoinsWidget extends StatelessWidget {
  double coins;

  ProfileCoinsWidget({this.coins = 0.0});

  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        getIt<CommunicateService>()
            .navigate(route: "/store", args: {"selected": "coins"});
      },
      child: Row(
        children: [
          NuImage(
            path: "assets/images/coins.png",
            width: Grid.columns(context, 1),
          ),
          SizedBox(width: 5),
          Container(
            padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(12)),
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Color(0xffffffff),
                    Color(0xffb5b5b5),
                  ]),
            ),
            child: NuText(
              text: NumberModule.format(this.coins),
              style: TextStyle(
                fontSize: 10,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:nu/dependency-injection.dart';
import 'package:nu/src/models/user.model.dart';
import 'package:nu/src/providers/me.provider.dart';
import 'package:nu/src/utils/constants.dart';
import 'package:nu/src/widgets/core/progress.widget.dart';
import 'package:nu/src/widgets/core/text.widget.dart';

class ProfileLevelWidget extends StatelessWidget {
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: getIt<MeProvider>().user,
      builder: (context, user, _) {
        return Column(
          children: [
            _ProfileLevelHead(user),
            SizedBox(height: 3),
            NuProgress(
              percentage: user.level.percentage.toDouble(),
            ),
          ],
        );
      },
    );
  }

  Widget _ProfileLevelHead(User user) {
    final levelStyle = TextStyle(
      fontSize: 11,
      fontWeight: FontWeight.bold,
    );

    final sessionsStyle = TextStyle(
      fontSize: 8,
      fontStyle: FontStyle.italic,
      color: Color(0xff333333),
    );

    int remainingSessions() {
      int p = user.gender == Constants.gender.female ? 50 : 5;

      return ((user.level.nextLevelScore - user.level.score) / p).round();
    }

    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        NuText(
          i18n: "profile.level",
          style: levelStyle,
        ),
        SizedBox(width: 5),
        NuText(
          text: "${user.level.value}",
          style: levelStyle,
        ),
        Expanded(child: Container()),
        NuText(
          text: "${remainingSessions()}",
          style: sessionsStyle,
        ),
        SizedBox(width: 3),
        NuText(
          i18n: "level.sessions-to-level-up",
          style: sessionsStyle,
        ),
      ],
    );
  }
}

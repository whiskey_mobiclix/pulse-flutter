import 'package:flutter/material.dart';
import 'package:nu/src/models/stat.model.dart';
import 'package:nu/src/modules/number.module.dart';
import 'package:nu/src/widgets/core/text.widget.dart';

class StatWidget extends StatelessWidget {
  Stat data;

  StatWidget({Stat data}) {
    this.data = data ?? new Stat();
  }

  Widget build(BuildContext context) {
    return Column(
      children: [
        NuText(
          text: NumberModule.format(data.value),
          style: TextStyle(
            fontSize: 15,
            fontWeight: FontWeight.bold,
          ),
        ),
        SizedBox(height: 3),
        NuText(
          i18n: data.label["i18n"],
          text: data.label["text"],
          style: TextStyle(
            fontSize: 9,
            color: Color(0xff707070),
          ),
        )
      ],
    );
  }
}

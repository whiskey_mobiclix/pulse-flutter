import 'package:flutter/material.dart';
import 'package:nu/src/models/user.model.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/widgets/core/country.widget.dart';
import 'package:nu/src/widgets/core/image.widget.dart';
import 'package:nu/src/widgets/core/text.widget.dart';

class ProfileDetailsWidget extends StatelessWidget {
  User user;

  ProfileDetailsWidget({
    User user,
  }) {
    this.user = user ?? new User({});
  }

  Widget build(BuildContext context) {
    return Column(
      children: [
        _ProfileDetailsLocation(context),
        SizedBox(height: 8),
        _ProfileDetalsHobbies(context),
      ],
    );
  }

  Widget _ProfileDetailsLocation(BuildContext context) {
    return Row(
      children: [
        NuImage(
          path: "assets/images/location.png",
          width: Grid.columns(context, 1.3),
        ),
        SizedBox(width: 10),
        NuCountry(code: this.user.country),
      ],
    );
  }

  Widget _ProfileDetalsHobbies(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        NuImage(
          path: "assets/images/heart.png",
          width: Grid.columns(context, 1.3),
        ),
        SizedBox(width: 10),
        Expanded(
          child: NuText(
              text: this.user.hobbies.isEmpty
                  ? "-"
                  : this.user.hobbies.join(", "),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                fontSize: 11,
              )),
        ),
      ],
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:nu/src/modules/country.module.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/widgets/core/text.widget.dart';

class NuCountry extends StatelessWidget {
  String code;
  TextStyle style;

  NuCountry({this.code, this.style});

  Widget build(BuildContext context) {
    return Row(
      children: [
        SvgPicture.asset(
          CountryModule.getFlagSvgPath(code: this.code),
          width: Grid.columns(context, 1.2),
        ),
        SizedBox(width: 5),
        NuText(
          text: CountryModule.getName(code: this.code),
          style: TextStyle(
            fontSize: 10,
          ).merge(this.style),
        ),
      ],
    );
  }
}

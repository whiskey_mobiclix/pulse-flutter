import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:nu/src/utils/constants.dart';
import 'package:nu/src/widgets/core/text.widget.dart';

class NuImage extends StatelessWidget {
  String path;
  double width;
  double height;
  BoxFit fit;
  String onLoadErrorImagePath;

  NuImage({
    this.path = "",
    this.width,
    this.height,
    this.fit,
    this.onLoadErrorImagePath,
  });

  Widget build(BuildContext context) {
    if (Constants.regex.url.hasMatch(this.path)) {
      return CachedNetworkImage(
          imageUrl: this.path,
          width: this.width,
          height: this.height,
          fit: this.fit ?? BoxFit.cover);
    }

    return Image.asset(this.path,
        width: this.width, height: this.height, fit: this.fit ?? BoxFit.cover);
  }
}

import 'package:flutter/material.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/widgets/core/text.widget.dart';

class NuProgress extends StatelessWidget {
  double percentage;
  List<Color> colors;
  double height;

  NuProgress({this.percentage = 0.0, this.colors, this.height});

  double _height(BuildContext context) {
    if (this.height != null) return this.height;
    return Grid.columns(context, 1);
  }

  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
//        print(constraints.maxWidth);

        return Stack(
          children: [
            _NuProgressBackground(context),
            _NuProgressBar(context, maxWidth: constraints.maxWidth),
            Container(
              alignment: Alignment.center,
              child: NuText(
                text: "${this.percentage.toInt()}%",
                style: TextStyle(
                  fontSize: 9,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  Widget _NuProgressBar(BuildContext context, {double maxWidth = 0}) {
    return Container(
      width: maxWidth * this.percentage / 100,
      height: _height(context),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(25)),
          gradient: LinearGradient(
            colors: this.colors != null && this.colors.isNotEmpty
                ? this.colors
                : [
                    Color(0xffff5700),
                    Color(0xffffbb47),
                  ],
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
          )),
    );
  }

  Widget _NuProgressBackground(BuildContext context) {
    return Container(
        height: _height(context),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(25)),
            color: Color(0xff333333)));
  }
}

import 'package:flutter/material.dart';

class NuList extends StatelessWidget {
  final List<Widget> children;
  final int columns;

  NuList({
    this.children,
    this.columns,
  });

  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      List<Widget> rows = [];
      List<Widget> items = [];

      for (var i = 0; i < this.children.length; i++) {
        items.add(new Container(
          child: this.children[i],
          width: (constraints.maxWidth / this.columns),
        ));
        if (i != 0 && (i + 1) % this.columns == 0) {
          rows.add(new Row(children: items));
          items = [];
        }
      }

      rows.add(new Row(children: items));

      return Column(
        children: rows,
      );
    });
  }
}

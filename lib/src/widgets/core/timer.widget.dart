import 'dart:async';

import 'package:flutter/material.dart';
import 'package:nu/src/modules/number.module.dart';
import 'package:nu/src/widgets/core/text.widget.dart';

class NuTimer extends StatefulWidget {
  final int milliseconds;
  final TextStyle style;

  NuTimer({this.milliseconds, this.style});

  @override
  _NuTimerState createState() => _NuTimerState();
}

class _NuTimerState extends State<NuTimer> {
  Timer _timer;

  Duration _timeUntilDue = Duration(milliseconds: 0);

  @override
  void initState() {
    new Future.delayed(Duration(milliseconds: 0), () {
      setState(() {
        _timeUntilDue = Duration(milliseconds: widget.milliseconds);
      });

      _timer = Timer.periodic(Duration(seconds: 1), (timer) {
        if (_timeUntilDue.inMilliseconds == 0)
          _timer.cancel();
        else
          setState(() {
            _timeUntilDue =
                Duration(milliseconds: _timeUntilDue.inMilliseconds - 1000);
          });
      });
    });

    super.initState();
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  String _getTimeLeft() {
    int hours = _timeUntilDue.inHours;
    int minutes = (_timeUntilDue.inMinutes - _timeUntilDue.inHours * 60);
    int seconds = (_timeUntilDue.inSeconds - _timeUntilDue.inMinutes * 60);

    return "${NumberModule.display(hours)}:${NumberModule.display(minutes)}:${NumberModule.display(seconds)}s";
  }

  Widget build(BuildContext context) {
    if (this.widget.milliseconds == null) return SizedBox();

    return NuText(
      text: _getTimeLeft(),
      style: widget.style,
    );
  }
}

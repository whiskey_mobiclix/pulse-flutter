import 'package:flutter/material.dart';
import 'package:nu/dependency-injection.dart';
import 'package:nu/src/modules/string.module.dart';
import 'package:nu/src/providers/general.provider.dart';
import 'package:nu/src/configs/general.config.dart';
import 'package:nu/src/configs/theme.config.dart';
import 'package:nu/src/i18n/i18n.dart';

class NuText extends StatelessWidget {
  String i18n;
  String text;
  Key key;
  TextStyle style;
  StrutStyle strutStyle;
  TextAlign textAlign;
  TextDirection textDirection;
  Locale locale;
  bool softWrap;
  TextOverflow overflow;
  double textScaleFactor;
  int maxLines;
  String semanticsLabel;
  TextWidthBasis textWidthBasis;
  TextHeightBehavior textHeightBehavior;
  bool capitalize;
  bool uppercase;

  NuText({
    String i18n,
    String text,
    Key key,
    TextStyle style,
    StrutStyle strutStyle,
    TextAlign textAlign,
    TextDirection textDirection,
    Locale locale,
    bool softWrap,
    TextOverflow overflow,
    double textScaleFactor,
    int maxLines,
    String semanticsLabel,
    TextWidthBasis textWidthBasis,
    TextHeightBehavior textHeightBehavior,
    bool capitalize,
    bool uppercase,
  }) {
    this.i18n = i18n;
    this.text = text;
    this.key = key;
    this.style = style;
    this.strutStyle = strutStyle;
    this.textAlign = textAlign;
    this.textDirection = textDirection;
    this.locale = locale;
    this.softWrap = softWrap;
    this.overflow = overflow;
    this.textScaleFactor = textScaleFactor;
    this.maxLines = maxLines;
    this.semanticsLabel = semanticsLabel;
    this.textWidthBasis = textWidthBasis;
    this.textHeightBehavior = textHeightBehavior;
    this.capitalize = capitalize ?? false;
    this.uppercase = uppercase ?? false;
  }

  Widget build(BuildContext context) {
    return ValueListenableBuilder(
        valueListenable: getIt<GeneralProvider>().language,
        builder: (_, lang, child) {
          final translated =
              I18n.languages[lang] ?? I18n.languages[AppSettings.defaultlang];
          String value = translated[this.i18n] ??
              this.text ??
              '[NuText] \'${i18n}\' key is not defined in i18n';
          if (this.capitalize) value = StringModule.capitalize(value);
          if (this.uppercase) value = value.toUpperCase();

          TextStyle textStyle = TextStyle(
            decoration: TextDecoration.none,
            color: AppTheme.font.color,
            fontSize: AppTheme.font.size,
            fontFamily: AppTheme.font.name,
            fontWeight: FontWeight.w400,
          ).merge(this.style);

          return Text(
            value,
            key: this.key,
            style: textStyle.merge(new TextStyle(
              fontSize: textStyle.fontSize *
                  (MediaQuery.of(context).size.width / 320),
            )),
            strutStyle: this.strutStyle,
            textAlign: this.textAlign,
            textDirection: this.textDirection,
            locale: this.locale,
            softWrap: this.softWrap,
            overflow: this.overflow,
            textScaleFactor: this.textScaleFactor,
            maxLines: this.maxLines,
            semanticsLabel: this.semanticsLabel,
            textWidthBasis: this.textWidthBasis,
            textHeightBehavior: this.textHeightBehavior,
          );
        });
  }
}

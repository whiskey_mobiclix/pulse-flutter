final Map<String, dynamic> LeaderBoardMockupResponse = {
  "status": 1,
  "data": [
    {
      "user_id": 1143111,
      "full_name": "Pulse User Test 1",
      "rank": 1,
      "point": 1550,
      "level": 1
    },
    {
      "user_id": 11753550,
      "full_name": "Hieu  Mai",
      "rank": 2,
      "point": 1240,
      "avatar_url": "tentendomain/335.jpg",
      "level": 5
    },
    {
      "user_id": 901,
      "full_name": "Phan Sinh Thưởng",
      "rank": 3,
      "point": 350,
      "avatar_url": "tentendomain/7.png",
      "level": 1
    },
    {
      "user_id": 1531903,
      "full_name": "Net Net",
      "rank": 4,
      "point": 310,
      "avatar_url": "tentendomain/107.jpeg"
    },
    {
      "user_id": 11753531,
      "full_name": "Tien Mai tien mai mai mai",
      "rank": 5,
      "point": 247,
      "avatar_url": "tentendomain/271.jpg"
    },
    {
      "user_id": 902,
      "full_name": "Update Login",
      "rank": 6,
      "point": 240,
      "avatar_url": "tentendomain/8.jpg"
    },
    {
      "user_id": 929,
      "full_name": "Francis Smith",
      "rank": 7,
      "point": 144,
      "avatar_url": "tentendomain/10.jpg"
    },
    {
      "user_id": 11753553,
      "full_name": "Lia Trần hdjdidksknsnsjskks",
      "rank": 8,
      "point": 40,
      "avatar_url": "tentendomain/338.jpg"
    },
    {
      "user_id": 138670,
      "full_name": "John Le",
      "rank": 9,
      "point": 24,
      "avatar_url": "tentendomain/16.jpg"
    },
    {
      "user_id": 11753449,
      "full_name": "Mốc G",
      "rank": 10,
      "point": 24,
      "avatar_url": "tentendomain/279.png"
    }
  ]
};

final Map<String, dynamic> LeaderboardMyRankResponseMockup = {
  "status": 0,
  "data": {
    "rank": 100,
    "userID": 0,
    "point": 2000,
  }
};

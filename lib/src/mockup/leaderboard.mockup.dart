final List<Map<String, dynamic>> LeaderboardMockup = [
  {
    "user": {
      "full_name": "Deralle P.",
      "avatar_url":
          "",
      "extensions": {
        "coin": 1242302,
        "gift": "",
      }
    },
    "rank": 1,
  },
  {
    "user": {
      "full_name": "Nimo",
      "avatar_url":
          "https://fiverr-res.cloudinary.com/images/t_main1,q_auto,f_auto,q_auto,f_auto/gigs/105812126/original/6d098b1dcfd4296a30f4866ab23eea04447906a2/design-a-minimalist-and-flat-line-art-avatar.png",
      "extensions": {
        "coin": 813810,
        "gift": "",
      }
    },
    "rank": 2,
  },
  {
    "user": {
      "full_name": "Juju",
      "avatar_url":
          "https://fiverr-res.cloudinary.com/images/t_main1,q_auto,f_auto,q_auto,f_auto/gigs/105812126/original/6d098b1dcfd4296a30f4866ab23eea04447906a2/design-a-minimalist-and-flat-line-art-avatar.png",
      "extensions": {
        "coin": 621512,
        "gift": "",
      }
    },
    "rank": 3,
  },
  {
    "user": {
      "full_name": "Deralle P.",
      "avatar_url":
          "https://fiverr-res.cloudinary.com/images/t_main1,q_auto,f_auto,q_auto,f_auto/gigs/105812126/original/6d098b1dcfd4296a30f4866ab23eea04447906a2/design-a-minimalist-and-flat-line-art-avatar.png",
      "extensions": {
        "coin": 122321,
        "gift": "",
      }
    },
    "rank": 4,
  }
];

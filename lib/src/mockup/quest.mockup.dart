final List<Map<String, dynamic>> MockupDailyQuest = [
  {
    "name": "Collect 10 Air Baloon",
    "goal": 10,
    "earned": 5,
    "gemsBonus": 10,
    "icon":
        "https://pulse-api-v2.mocogateway.com/static/pulse/gift/Flirting/Dom%20Perignon%20Champagne.png",
  },
  {
    "name": "Collect 10 Roses",
    "goal": 10,
    "earned": 3,
    "gemsBonus": 10,
    "icon":
        "https://pulse-api-v2.mocogateway.com/static/pulse/gift/Luxury/Diamond.png",
  },
  {
    "name": "Collect 10 Kisses",
    "goal": 10,
    "earned": 2,
    "gemsBonus": 10,
    "icon":
        "https://pulse-api-v2.mocogateway.com/static/pulse/gift/Luxury/Diamond.png",
  },
  {
    "name": "Collect 10 Dancing Dress",
    "goal": 10,
    "earned": 6,
    "gemsBonus": 10,
    "icon":
        "https://pulse-api-v2.mocogateway.com/static/pulse/gift/Luxury/Diamond.png",
  },
  {
    "name": "Collect 10 Date Palm",
    "goal": 10,
    "earned": 5,
    "gemsBonus": 10,
    "icon":
        "https://pulse-api-v2.mocogateway.com/static/pulse/gift/Luxury/Diamond.png",
  },
];

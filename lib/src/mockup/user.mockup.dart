import 'dart:convert';

String MockupUser = json.encode({
	"avatar": "https://api.pulse-web.com/static/pulse/document/1313895.jpeg",
	"name": "Whiskey",
	"age": 26,
	"id": "281371",
	"country": "us",
	"coins": 1000000,
	"platinum": false,
	"level": 24,
	"photos": [
		"https://images.unsplash.com/photo-1555445091-5a8b655e8a4a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80",
	]
});

Map<String, dynamic> MockupUserStatsReponse = {
    "status": 1,
    "data": {
        "friend_count": 3,
        "people_matched_count": 10,
        "gift_sent_count": 100,
        "list_gift_received_count": [
	        {
	            "count": 28,
	            "gift_id": 1
	        },
	        {
	            "count": 144,
	            "gift_id": 2
	        }
	    ],
    }
};

Map<String, dynamic> MockupUserPhotos = {
	"status": 1,
    "data": [
        {
            "id": 777,
            "created_at": 1589862023,
            "updated_at": 1591963347,
            "user_id": 11753501,
            "document_id": 326,
            "display_order": 1,
            "type": 0,
            "document": {
                "id": 326,
                "name": "11753501_photo.jpg",
                "url": "https://www.pandasecurity.com/mediacenter/src/uploads/2013/11/pandasecurity-facebook-photo-privacy.jpg"
            }
        }
    ]
};

Map<String, dynamic> MockupUserInterest = {
    "status": 1,
    "data": [
        {
            "id": 1266,
            "created_at": 1589862039,
            "updated_at": 1589862039,
            "user_id": 11753501,
            "interest_id": 56,
            "interest": {
                "id": 56,
                "created_at": 1559554571,
                "updated_at": 1561963147,
                "name": "Movies",
                "icon_url": "https://dev-gapi.mocogateway.com/static/pulse/interest/45aa8bf3de56e8130c4d7bde7d29a5907c8c479ade40db4358b3639607f7d025.png",
                "selected_icon_url": "https://dev-gapi.mocogateway.com/static/pulse/interest/8c3b529933911563e727c15c3c82119562d7e1dc3ecb81d3b515341a764e79ea.png"
            }
        }
    ]
};
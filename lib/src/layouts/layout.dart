import 'package:flutter/material.dart';

class NuLayout extends StatelessWidget {
  final Widget child;
  final Color background;
  final EdgeInsetsGeometry padding;
  final Color statusBarColor;
  final bool safeLeft;
  final bool safeRight;

  NuLayout({
    this.child,
    this.background = Colors.white,
    this.padding,
    this.statusBarColor = Colors.white,
    this.safeLeft = true,
    this.safeRight = true,
  });

  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      padding: this.padding,
      color: this.background,
      child: SafeArea(
        left: this.safeLeft,
        right: this.safeRight,
        child: SingleChildScrollView(
          child: this.child,
        ),
      ),
    );
  }
}

class Level {
	int value;
	int percentage;
	int score;
	int nextLevelScore;

	Level(Map<String, int> data){
		this.value = data["value"] ?? 0;
		this.percentage = data["percentage"] ?? 0;
		this.score = data["score"] ?? 0;
		this.nextLevelScore = data["nextLevelScore"] ?? 0;
	}
}

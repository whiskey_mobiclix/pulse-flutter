class DailyQuest {
  String name;
  int goal;
  int earned;
  int bonusCoins;
  int bonusGems;
  String icon;

  DailyQuest({
    this.name = "",
    this.goal = 0,
    this.earned = 0,
    this.bonusCoins = 0,
    this.bonusGems = 0,
    this.icon = "",
  });
}

import 'package:nu/src/models/avatar.model.dart';
import 'package:nu/src/models/photos.model.dart';
import 'package:nu/src/models/properties.model.dart';
import 'package:nu/src/utils/constants.dart';
import 'package:nu/src/models/level.model.dart';
import 'package:nu/src/models/stat.model.dart';
import 'package:nu/src/models/gift.model.dart';

class User {
  String name;
  int id;
  String uuid;
  String country;
  Avatar avatar;
  int age;
  Level level;
  int gender;
  List<Stat> stats;
  int coins;
  bool platinum;
  List<String> hobbies;
  Photos photos;
  Properties properties;
  List<Gift> gifts;
  Map<String, dynamic> extra;
  bool isIdol;
  int friendStatus;

  User(Map<String, dynamic> data) {
    this.id = data["id"];
    this.uuid = data["uuid"] ?? "";
    this.avatar = data["avatar"] ?? new Avatar();
    this.coins = data["coins"] ?? 0;
    this.level = data["level"] ?? new Level({});
    this.platinum = data["platinum"] ?? false;
    this.name = data["name"] ?? "";
    this.age = data["age"] ?? 0;
    this.country = data["country"] ?? "us";
    this.gender = data["gender"] ?? Constants.gender.unknown;
    this.hobbies = data["hobbies"] ?? [];
    this.photos = data["photos"] ?? new Photos();
    this.stats = data["stats"] ?? [];
    this.gifts = data["gifts"] ?? [];
    this.extra = data["extra"] ?? {};
    this.properties = data["properties"] ?? new Properties();
    this.isIdol = data["isIdol"] ?? false;
    this.friendStatus =
        data["friend-status"] ?? Constants.friendStatus.stranger;
  }

  void setHobbies(List<String> hobbies) {
    this.hobbies = hobbies;
  }

  void setPhotos(Photos photos) {
    this.photos = photos;
  }

  void setStats(List<Stat> stats) {
    this.stats = stats;
  }

  void setExtra(Map<String, dynamic> extra) {
    this.extra = extra;
  }

  void setProperties(Properties properties) {
    this.properties = properties;
  }

  bool isFemale() {
    return this.gender == Constants.gender.female;
  }
}

class Quest {
  String name;
  int goal;
  int earned;
  int bonus;
  int giftId;
  int gems;
  int score;

  Quest({
    String name,
    int goal,
    int earned,
    int bonus,
    int giftId,
    int gems,
    int score,
  }) {
    this.name = name ?? "";
    this.goal = goal ?? 0;
    this.earned = earned ?? 0;
    this.bonus = bonus ?? 0;
    this.giftId = giftId ?? -99;
    this.gems = gems ?? 0;
    this.score = score ?? 0;
  }
}

class Message {
  String type;
  String i18n;
  String text;
  bool show;

  Message({
    this.type = "",
    this.i18n = "",
    this.text = "",
    this.show = false,
  });

  void toggleShow({bool status}) {
    this.show = status ?? !this.show;
  }
}

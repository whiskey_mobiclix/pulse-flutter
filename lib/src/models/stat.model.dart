class Stat {
  Map<String, String> label;
  double value;
  bool show;

  Stat({
    Map<String, String> label,
    double value,
    bool show,
  }) {
    this.label = label ?? {};
    this.value = value ?? 0;
    this.show = show ?? true;
  }
}

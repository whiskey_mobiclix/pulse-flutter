import 'package:nu/src/models/gift.model.dart';

class Gifts {
  bool loading;
  List<Gift> data;

  Gifts({
    bool loading,
    List<Gift> data,
  }) {
    this.loading = loading ?? true;
    this.data = data ?? [];
  }

  void setData(List<Gift> data) {
    this.data = data ?? [];
    this.loading = false;
  }
}

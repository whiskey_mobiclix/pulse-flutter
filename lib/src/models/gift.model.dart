class Gift{
	int id;
	String thumbnail;
	String name;
	int amount;
	double gems;
	double price;

	Gift({
		int id,
		String thumbnail,
		String name,
		int amount,
		double gems,
		double price,
	}){
		this.id = id ?? 0;
		this.thumbnail = thumbnail ?? "";
		this.name = name ?? "";
		this.amount = amount ?? 1;
		this.gems = gems ?? 0;
		this.price = price ?? 0;
	}
}
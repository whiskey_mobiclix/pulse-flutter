class Property{
	String name;
	int id;
	double price;
	String thumbnail;

	Property(Map<String, dynamic> data){
		this.id = data["id"];
		this.name = data["name"];
		this.price = data["price_in_coin"];
		this.thumbnail = data["thumbnail"];
	}
}
class Photos {
  bool loading;
  List<String> data;

  Photos({
    bool loading,
    List<String> data,
  }) {
    this.loading = loading ?? true;
    this.data = data ?? [];
  }

  void setData(List<String> data) {
    this.data = data ?? [];
    this.loading = false;
  }
}

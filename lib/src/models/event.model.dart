class Event {
  String name;
  int start;
  int end;
  bool opened;

  Event({
    this.name = "",
    this.start = 0,
    this.end = 0,
    this.opened = true,
  });
}

class QuestReward {
  int goal;
  int completed;
  int bonusGems;

  QuestReward({
    this.goal = 0,
    this.completed = 0,
    this.bonusGems = 0,
  });
}

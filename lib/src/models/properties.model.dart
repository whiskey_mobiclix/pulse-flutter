import 'package:nu/src/models/property.model.dart';

class Properties {
  bool loading;
  List<Property> data;

  Properties({
    bool loading,
    List<Property> data,
  }) {
    this.loading = loading ?? true;
    this.data = data ?? [];
  }

  void setData(List<Property> data) {
    this.data = data ?? [];
    this.loading = false;
  }
}

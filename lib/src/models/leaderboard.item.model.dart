import 'package:nu/src/models/user.model.dart';
import 'package:nu/src/utils/constants.dart';

class LeaderboardItem {
  User user;
  int point;
  int rank;
  String type;

  LeaderboardItem({
    User user,
    int point,
    int rank,
    String type,
  }) {
    this.user = user ?? new User({});
    this.point = point ?? 0;
    this.rank = rank ?? 0;
    this.type = type ?? Constants.leaderboardType.coin;
  }
}

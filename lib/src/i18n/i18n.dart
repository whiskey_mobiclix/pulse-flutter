import 'en.dart';
import 'vi.dart';

class I18n {
  static Map<String, Map> languages = const {'en': en, 'vi': vi};
}

import 'dart:convert';
import 'dart:math';

import 'package:nu/dependency-injection.dart';
import 'package:nu/src/providers/me.provider.dart';
import 'package:nu/src/utils/constants.dart';

class Utils {
  static String randomString(int length) {
    const _chars =
        'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
    Random _rnd = Random();

    return String.fromCharCodes(Iterable.generate(
        length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));
  }

  static bool isEmpty(dynamic object) {
    if (object == null) return true;

    if (object is String) return object == "";

    if (object is List) return object.isEmpty;

    return false;
  }

  static String clearZeroWidthSpace(String str) {
    try {
      List<int> bytes = str.toString().codeUnits;
      return utf8.decode(bytes).replaceAll("", "\u{200B}");
    } catch (e) {
      print("Error when handle utf8 with string: $str");
      print(e);
      return str.replaceAll("", "\u{200B}");
    }
  }

  static int getRelationship(int userId) {
    final data = getIt<MeProvider>().relationship.value;

    if (!Utils.isEmpty(data["blocking"]) && (data["blocking"]).contains(userId))
      return Constants.friendStatus.blocking;

    if (!Utils.isEmpty(data["blocked"]) && (data["blocked"]).contains(userId))
      return Constants.friendStatus.blocked;

    if (!Utils.isEmpty(data["friend_normal"]) &&
        (data["friend_normal"]).contains(userId))
      return Constants.friendStatus.friend;

    if (!Utils.isEmpty(data["friend_favourite"]) &&
        (data["friend_favourite"]).contains(userId))
      return Constants.friendStatus.friend;

    if (!Utils.isEmpty(data["requesting"]) &&
        (data["requesting"]).contains(userId))
      return Constants.friendStatus.sent;

    if (!Utils.isEmpty(data["requested"]) &&
        (data["requested"]).contains(userId))
      return Constants.friendStatus.received;

    return Constants.friendStatus.stranger;
  }
}

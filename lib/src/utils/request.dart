import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:nu/dependency-injection.dart';
import 'package:nu/src/configs/enviroment.config.dart';
import 'package:nu/src/services/auth.service.dart';
import 'package:nu/src/services/device.service.dart';

class Request {
  static Duration timeout = Duration(seconds: 7);

  static Future<Map<String, String>> get headers async {
    final authService = getIt.get<AuthService>();
    String deviceId = "";
    String userAgent =
        getIt<Enviroment>().mode != "production" ? "PostmanRuntime" : "";

    try {
      final deviceService = getIt.get<DeviceService>();
      deviceId = await deviceService.id;
    } catch (e) {}

    return {
      "Authorization": "Bearer ${authService.accessToken}",
      "User-Agent": userAgent,
      "Device-Id": deviceId,
      "Device-Info": "Test Device",
    };
  }

  // ignore: non_constant_identifier_names
  static Future<http.Response> GET(String url) async {
    final headers = await Request.headers;

    return await http.get(url, headers: headers).timeout(Request.timeout);
  }

  // ignore: non_constant_identifier_names
  static Future<http.Response> POST(
      String url, Map<String, dynamic> body) async {
    final headers = await Request.headers;

    return await http
        .post(url, headers: headers, body: json.encode(body))
        .timeout(Request.timeout);
  }

  // ignore: non_constant_identifier_names
  static Future<http.Response> PUT(
      String url, Map<String, dynamic> body) async {
    final headers = await Request.headers;

    return await http
        .put(url, headers: headers, body: json.encode(body))
        .timeout(Request.timeout);
  }

  // ignore: non_constant_identifier_names
  static Future<http.Response> DELETE(String url) async {
    final headers = await Request.headers;

    return await http.delete(url, headers: headers).timeout(Request.timeout);
  }
}

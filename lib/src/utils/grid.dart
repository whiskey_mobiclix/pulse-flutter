import 'package:flutter/material.dart';
import 'package:nu/src/configs/theme.config.dart';

class Grid {
	static double columns(BuildContext context, double value){
		return MediaQuery.of(context).size.width / AppTheme.grid.columns * value;
	}
}

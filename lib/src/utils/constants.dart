class Constants {
  static final gender = const _Gender();
  static final frames = const _Frames();
  static final friendStatus = const _FriendStatus();
  static final friends = const _Friends();
  static final response = const _Response();
  static final regex = _Regex();
  static final leaderboardType = const _LeaderboardType();
  static final friendRequestStatus = const _FriendRequestStatus();

  Constants();
}

class _Gender {
  final int unknown = 0;
  final int male = 1;
  final int female = 2;

  const _Gender();
}

class _FriendStatus {
  final int stranger = 0;
  final int sent = 1;
  final int received = 2;
  final int friend = 3;
  final int favorite = 4;
  final int blocking = 5;
  final int blocked = 6;

  const _FriendStatus();
}

class _FriendRequestStatus {
  final int open = 0;
  final int confirm = 1;
  final int reject = 2;
  final int cancel = 3;

  const _FriendRequestStatus();
}

class _Frames {
  final List<Map<String, dynamic>> list = const [
    {
      "thumbnail": "assets/images/frames/bronze.png",
      "i18n": "frame.bronze",
    },
    {
      "thumbnail": "assets/images/frames/silver.png",
      "i18n": "frame.silver",
    },
    {
      "thumbnail": "assets/images/frames/gold.png",
      "i18n": "frame.gold",
      "description": "Lv. 10 - 14",
    },
    {
      "thumbnail": "assets/images/frames/diamond.png",
      "i18n": "frame.diamond",
      "description": "Lv. 15 - 19",
    },
    {
      "thumbnail": "assets/images/frames/social.png",
      "i18n": "frame.celebrity",
      "description": "Lv. 20 - 29",
    },
    {
      "thumbnail": "assets/images/frames/world_class.png",
      "i18n": "frame.world-class",
      "description": "Lv. 30 - 50",
    },
  ];

  const _Frames();
}

class _Friends {
  final double minimumCoinsToAddFriend = 80;

  const _Friends();
}

class _Response {
  final int success = 1;

  const _Response();
}

class _Regex {
  final url = new RegExp(
      r"https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)");

  _Regex();
}

class _LeaderboardType {
  final String coin = "buyer";
  final String sendGift = "send_gift";
  final String receiveGift = "receive_gift";
  final String level = "level";
  final String receiveGem = "receive_gem";

  const _LeaderboardType();
}

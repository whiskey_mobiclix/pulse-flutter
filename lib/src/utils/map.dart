class CustomMap {
  Map<String, dynamic> data;

  CustomMap(this.data);

  dynamic get(String path) {
    dynamic res = data;

    List<String> keys = path.split(".");
    keys.forEach((o) {
      if (res != null && res is Map)
        res = res[o] ?? null;
      else
        res = null;
    });

    return res;
  }
}

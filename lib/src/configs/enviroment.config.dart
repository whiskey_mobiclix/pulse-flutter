class Enviroment {
  String mode = "staging";

  static final Map<String, String> production = {
    'api-url': 'https://pulse-api-v2.mocogateway.com',
  };

  static final Map<String, String> staging = {
    'api-url': 'https://stag-pulse-api-v2.mocogateway.com',
  };

  Map<String, String> get all {
    if (this.mode == 'production') {
      return Enviroment.production;
    }

    return Enviroment.staging;
  }

  String get(String key) {
    return this.all[key];
  }

  void setMode(String value) {
    this.mode = value;
  }
}

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AppTheme {
	static final background = const _Background();
	static final font = const _Font();
	static final status = const _StatusBar();
	static final grid = const _Grid();
	const AppTheme();
}

class _Background {
	final Color color = Colors.white;
	const _Background();
}

class _Font {
	final String name = 'FF-Mark-Pro';
	final double size = 14;
	final Color color = Colors.black;
	const _Font();
}

class _StatusBar {
	final style = SystemUiOverlayStyle.dark;
	const _StatusBar();
}

class _Grid{
	final columns = 24;
	const _Grid();
}
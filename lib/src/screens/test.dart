import 'package:flutter/material.dart';
import 'package:nu/src/layouts/layout.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/widgets/core/center-popup.widget.dart';
import 'package:nu/src/widgets/core/text.widget.dart';

class TestScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print(MediaQuery.of(context).size.height);

    return NuLayout(
      child: Column(
        children: [
          GestureDetector(
            onTap: () {
              _openCenterPopup(context);
            },
            child: NuText(
              text: "Change message",
            ),
          ),
        ],
      ),
    );
  }

  void _openCenterPopup(BuildContext context) {
    showCenterPopup(
      context: context,
      width: Grid.columns(context, 22),
      height: 400,
      isDismissible: true,
      content: Container(
        child: GestureDetector(
          onTap: () => Navigator.pop(context),
          child: Text("awwdfffwwwfewfefw"),
        ),
      ),
//      actions: [
//        {
//          "title": "OK",
//          "action": () => print("OK"),
//        },
//      ],
    );
  }
}

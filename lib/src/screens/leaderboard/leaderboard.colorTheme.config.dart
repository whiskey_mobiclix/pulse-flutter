import 'package:flutter/material.dart';
import 'package:nu/src/utils/constants.dart';

class LeaderboardColorThemeConfig {
  static Map<String, Map<String, dynamic>> coinColor = {
    Constants.leaderboardType.coin: {
      "color": Color(0xff6721E0),
      "gradient": LinearGradient(
        colors: [Color(0xffB184FF), Color(0xff6721E0)],
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
      ),
    },
    Constants.leaderboardType.receiveGem: {
      "color": Color(0xffDF0C8B),
      "gradient": LinearGradient(
        colors: [Color(0xfff486c8), Color(0xffDF0C8B)],
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
      ),
    },
    Constants.leaderboardType.sendGift: {
      "color": Color(0xff0B99E2),
      "gradient": LinearGradient(
        colors: [Color(0xff64E2DA), Color(0xff0B99E2)],
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
      ),
    },
    Constants.leaderboardType.receiveGift: {
      "color": Color(0xffE2590B),
      "gradient": LinearGradient(
        colors: [Color(0xffE2B064), Color(0xffE2590B)],
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
      ),
    }
  };

  LeaderboardColorThemeConfig();
}

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:intl/intl.dart';
import 'package:nu/dependency-injection.dart';
import 'package:nu/src/layouts/layout.dart';
import 'package:nu/src/modules/datetime.module.dart';
import 'package:nu/src/providers/leaderboard.provider.dart';
import 'package:nu/src/providers/me.provider.dart';
import 'package:nu/src/screens/leaderboard/appbar/leaderboard.appbar.dart';
import 'package:nu/src/utils/constants.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/widgets/core/text.widget.dart';
import 'package:nu/src/widgets/leaderboard/leaderboard.me.widget.dart';
import 'package:nu/src/widgets/leaderboard/leaderboard.widgets.dart';

import 'leaderboard.colorTheme.config.dart';

class LeaderboardScreen extends StatefulWidget {
  @override
  _LeaderboardScreenState createState() => _LeaderboardScreenState();
}

class _LeaderboardScreenState extends State<LeaderboardScreen> {
  @override
  void initState() {
    getIt<LeaderboardProvider>().getLeaderboardGlobalList(
      type: getIt<LeaderboardProvider>().currentActiveTab.value,
    );
    getIt<MeProvider>().getRelationShip();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: LeaderboardAppBar(),
      body: ValueListenableBuilder(
        valueListenable: getIt<LeaderboardProvider>().leaderboardGlobalList,
        builder: (ctx, list, _) {
          return NuLayout(
            safeLeft: false,
            safeRight: false,
            background: list.isEmpty ? Colors.white : Color(0xfff5f5f5),
            child: _leaderBoardListener(context),
          );
        },
      ),
    );
  }

  Widget _leaderboardList(BuildContext context) {
    final leaederboardProvider = getIt<LeaderboardProvider>();

    var paddingBottom = Grid.columns(context, 5.5);

    //Male user in tab 'Hot Idol'
    if (leaederboardProvider.currentActiveTab.value ==
            Constants.leaderboardType.receiveGem &&
        !getIt<MeProvider>().user.value.isFemale()) paddingBottom = Grid.columns(context, 1);

    //Female user in tab 'Top Buyer'
    if (leaederboardProvider.currentActiveTab.value ==
            Constants.leaderboardType.coin &&
        getIt<MeProvider>().user.value.isFemale()) paddingBottom = Grid.columns(context, 1);

    //Female user in tab 'Top Gifts Sender'
    if (leaederboardProvider.currentActiveTab.value ==
            Constants.leaderboardType.sendGift &&
        getIt<MeProvider>().user.value.isFemale()) paddingBottom = Grid.columns(context, 1);

    return AnimatedBuilder(
      animation: Listenable.merge([
        getIt<MeProvider>().relationship,
        getIt<MeProvider>().user,
        getIt<LeaderboardProvider>().myGlobalRank,
      ]),
      builder: (ctx, _) {
        return ListView(
          padding: EdgeInsets.only(
            left: Grid.columns(context, 1),
            right: Grid.columns(context, 1),
            bottom: paddingBottom,
          ),
          children: (leaederboardProvider.leaderboardGlobalList.value)
              .map(
                (o) => LeaderboardItemWidget(
                  context,
                  data: o,
                ),
              )
              .toList(),
        );
      },
    );
  }

  Widget _leaderboardLoader() {
    final leaederboardProvider = getIt<LeaderboardProvider>();
    return Expanded(
      child: Container(
        padding: EdgeInsets.zero,
        alignment: Alignment.center,
        child: Container(
          child: CircularProgressIndicator(
            valueColor: new AlwaysStoppedAnimation<Color>(
                LeaderboardColorThemeConfig
                        .coinColor[leaederboardProvider.currentActiveTab.value]
                    ["color"]),
          ),
        ),
      ),
    );
  }

  Widget _leaderboardTitle() {
    String i18n = "leaderboard.title";
    final tab = getIt<LeaderboardProvider>().currentActiveTab.value;

    if (tab == Constants.leaderboardType.coin)
      i18n = "leaderboard.tab.buyer";
    else if (tab == Constants.leaderboardType.receiveGem)
      i18n = "leaderboard.tab.idol";
    else if (tab == Constants.leaderboardType.receiveGift)
      i18n = "leaderboard.tab.gift_receive";
    else if (tab == Constants.leaderboardType.sendGift)
      i18n = "leaderboard.tab.gift_send";

    final _timeStyle = TextStyle(
      color: Color(0xff7f7f7f),
      fontSize: 11,
    );

    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: Grid.columns(context, 1),
        vertical: 15,
      ),
      child: Row(
        children: [
          NuText(
            capitalize: true,
            i18n: i18n,
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
              fontSize: 13,
            ),
          ),
          Spacer(),
          NuText(
            i18n: "leaderboard.update-at",
            style: _timeStyle,
          ),
          NuText(
            style: _timeStyle,
            text:
                " ${DateFormat.Hm().format(DateTimeModule.parseFromTimestamp(DateTimeModule.getCachedTimestampUTC()))}",
          ),
        ],
      ),
    );
  }

  Widget _leaderBoardListener(BuildContext context) {
    final leaederboardProvider = getIt<LeaderboardProvider>();

    Future<void> _getData() async {
      setState(() {
        getIt<LeaderboardProvider>().getLeaderboardGlobalList(
          type: getIt<LeaderboardProvider>().currentActiveTab.value,
        );
        getIt<MeProvider>().getRelationShip();
      });
    }

    return AnimatedBuilder(
      animation: Listenable.merge([
        leaederboardProvider.loadingGlobal,
        leaederboardProvider.leaderboardGlobalList,
        leaederboardProvider.currentActiveTab,
      ]),
      builder: (ctx, _) {
        if (leaederboardProvider.loadingGlobal.value)
          return _layoutWithBoxConstraints(context,
              child: _leaderboardLoader());

        if (leaederboardProvider.leaderboardGlobalList.value.isEmpty)
          return _layoutWithBoxConstraints(context,
              child: LeaderBoardEmptyWidget(
                type: leaederboardProvider.currentActiveTab.value,
              ));

        return _layoutWithBoxConstraints(
          context,
          showMyRank: true,
          child: Expanded(
            child: RefreshIndicator(
              onRefresh: _getData,
              color: LeaderboardColorThemeConfig
                      .coinColor[leaederboardProvider.currentActiveTab.value]
                  ["color"],
              child: _leaderboardList(
                context,
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _layoutWithBoxConstraints(
    BuildContext context, {
    Widget child,
    bool showMyRank = false,
  }) {
    return ConstrainedBox(
      constraints: BoxConstraints(
          maxHeight: MediaQuery.of(context).size.height -
              LeaderboardAppBar().preferredSize.height -
              MediaQuery.of(context).padding.top -
              MediaQuery.of(context).padding.bottom),
      child: Stack(
        children: [
          Column(
            children: [
              _leaderboardTitle(),
              child,
            ],
          ),
          if (showMyRank)
            Positioned(
              bottom: -1.0 * Grid.columns(context, .5),
              child: LeaderBoardMeWidget(
                type: getIt<LeaderboardProvider>().currentActiveTab.value,
              ),
            )
        ],
      ),
    );
  }
}

import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:nu/dependency-injection.dart';
import 'package:nu/src/providers/leaderboard.provider.dart';
import 'package:nu/src/providers/me.provider.dart';
import 'package:nu/src/screens/leaderboard/leaderboard.colorTheme.config.dart';
import 'package:nu/src/utils/constants.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/widgets/core/image.widget.dart';
import 'package:nu/src/widgets/core/text.widget.dart';

class LeaderboardAppBar extends StatefulWidget implements PreferredSizeWidget {
  @override
  // TODO: implement preferredSize
  Size get preferredSize => Size.fromHeight(180);

  @override
  _LeaderboardAppBarState createState() => _LeaderboardAppBarState();
}

class _LeaderboardAppBarState extends State<LeaderboardAppBar> {
  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
        valueListenable: getIt<LeaderboardProvider>().currentActiveTab,
        builder: (context, activeTab, _) {
          return Container(
            decoration: BoxDecoration(
              gradient: LeaderboardColorThemeConfig.coinColor[activeTab]
                  ["gradient"],
            ),
            child: Stack(
              children: [
                Positioned.fill(
                  top: 0,
                  right: 0,
                  bottom: 0,
                  child: Container(
                    clipBehavior: Clip.hardEdge,
                    decoration: BoxDecoration(),
                    alignment: Alignment.centerRight,
                    child: Transform.rotate(
                      angle: -pi / 4,
                      child: Transform.scale(
                        scale: 2,
                        child: NuImage(
                          path: "assets/images/common/pulse_ic1.png",
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  padding:
                      EdgeInsets.only(top: MediaQuery.of(context).padding.top),
                  child: Column(
                    children: [
                      _leaderboardAppBarTitle(context),
                      Spacer(),
                      _leaderboardTabBar(context),
                      SizedBox(height: 10),
                      _leaderboardTimeRange(context),
                    ],
                  ),
                ),
              ],
            ),
          );
        });
  }

  Widget _leaderboardAppBarTitle(BuildContext context) {
    return Container(
      height: kToolbarHeight,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Container(
              color: Colors.transparent,
              padding: EdgeInsets.symmetric(
                  horizontal: Grid.columns(context, 1), vertical: 10),
              child: NuImage(
                height: Grid.columns(context, 1.1),
                path: "assets/images/common/back-arrow.png",
                fit: BoxFit.fitHeight,
              ),
            ),
          ),
          NuText(
            capitalize: true,
            i18n: "leaderboard.title",
            style: TextStyle(
              color: Colors.white,
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
          GestureDetector(
            onTap: () {
              final leaderboardPvdr = getIt<LeaderboardProvider>();
              leaderboardPvdr.getLeaderboardGlobalList(
                  type: leaderboardPvdr.currentActiveTab.value);
              getIt<MeProvider>().getRelationShip();
            },
            child: Container(
              padding: EdgeInsets.symmetric(
                  horizontal: Grid.columns(context, 1), vertical: 10),
              child: NuImage(
                height: Grid.columns(context, 1.1),
                path: "assets/images/common/refresh.png",
                fit: BoxFit.fitHeight,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _leaderboardTabBar(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: getIt<LeaderboardProvider>().currentActiveTab,
      builder: (context, activeTab, _) {
        return SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(width: Grid.columns(context, 1)),
              _tabBarItem(
                context,
                active: activeTab == Constants.leaderboardType.coin,
                i18n: "leaderboard.tab.buyer",
                type: Constants.leaderboardType.coin,
              ),
              _tabBarItem(
                context,
                active: activeTab == Constants.leaderboardType.receiveGem,
                i18n: "leaderboard.tab.idol",
                type: Constants.leaderboardType.receiveGem,
              ),
              _tabBarItem(
                context,
                active: activeTab == Constants.leaderboardType.sendGift,
                i18n: "leaderboard.tab.gift_send",
                type: Constants.leaderboardType.sendGift,
              ),
              _tabBarItem(
                context,
                active: activeTab == Constants.leaderboardType.receiveGift,
                i18n: "leaderboard.tab.gift_receive",
                type: Constants.leaderboardType.receiveGift,
              ),
              SizedBox(width: Grid.columns(context, 1)),
            ],
          ),
        );
      },
    );
  }

  Widget _tabBarItem(
    BuildContext context, {
    bool active = false,
    String i18n,
    String type,
  }) {
    return GestureDetector(
      onTap: () {
        final leaderBoardPvdr = getIt<LeaderboardProvider>();
        if (!leaderBoardPvdr.loadingGlobal.value)
          leaderBoardPvdr.onActiveTabChange(type);
      },
      child: AnimatedContainer(
        duration: Duration(milliseconds: 200),
        padding: EdgeInsets.symmetric(
          vertical: 12,
          horizontal: active ? Grid.columns(context, 1.2) : 12,
        ),
        margin: EdgeInsets.symmetric(horizontal: 2),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: active ? Colors.white : Colors.white.withOpacity(.4),
          borderRadius: BorderRadius.circular(25.0),
        ),
        child: NuText(
          capitalize: true,
          i18n: i18n,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 12.5,
          ),
        ),
      ),
    );
  }

  Widget _leaderboardTimeRange(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: Grid.columns(context, 1),
        vertical: 5,
      ),
      child: Row(
        children: [
          _leaderboardTimeRangeItem(
            value: "all",
            i18n: "leaderboard.timeframe.allTime",
          ),
          _leaderboardTimeRangeItem(
            value: "weekly",
            i18n: "leaderboard.timeframe.weekly",
          ),
        ],
      ),
    );
  }

  Widget _leaderboardTimeRangeItem({
    String value,
    String i18n,
  }) {
    final leaderBoardPvdr = getIt<LeaderboardProvider>();

    return ValueListenableBuilder(
      valueListenable: leaderBoardPvdr.leaderboardGlobalTimeFilter,
      builder: (ctx, timeFilter, _) {
        bool active =
            (value == leaderBoardPvdr.leaderboardGlobalTimeFilter.value);

        return GestureDetector(
          onTap: () {
            getIt<LeaderboardProvider>().setLeaderBoardGlobalTimeFrom(value);
          },
          child: AnimatedPadding(
            duration: Duration(milliseconds: 200),
            padding: EdgeInsets.all(10.0),
            child: Row(
              children: [
                if (active)
                  Container(
                    width: 6,
                    height: 6,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(2),
                    ),
                  ),
                if (active) SizedBox(width: 5),
                NuText(
                  capitalize: true,
                  i18n: i18n,
                  style: TextStyle(
                    color: active ? Colors.white : Colors.white.withOpacity(.8),
                    fontWeight: FontWeight.bold,
                    fontSize: 13,
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}

import 'package:flutter/material.dart';
import 'package:nu/dependency-injection.dart';
import 'package:nu/src/configs/enviroment.config.dart';
import 'package:nu/src/layouts/layout.dart';
import 'package:nu/src/screens/me/me.screen.dart';
import 'package:nu/src/services/communicate.service.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  void initState() {
    super.initState();
    getIt<CommunicateService>().context = context;

    if (getIt<Enviroment>().mode == "dev") {
      print("Navigate to other screen");

      new Future.delayed(Duration(milliseconds: 500), () {
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) => MeScreen(),
            ));
      });
    }
  }

  Widget build(BuildContext context) {
    return NuLayout();
  }
}

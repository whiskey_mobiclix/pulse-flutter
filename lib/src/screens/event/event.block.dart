import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:nu/dependency-injection.dart';
import 'package:nu/src/modules/number.module.dart';
import 'package:nu/src/providers/event.provider.dart';
import 'package:nu/src/screens/event/Idol-event-screen-body/idol-event-content.dart';
import 'package:nu/src/screens/event/idol-event-screen-bar/idol-event-screen-bar.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/widgets/core/text.widget.dart';

class EventBlock extends StatelessWidget {
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black.withOpacity(.9),
      appBar: IdolEventScreenBar(
        handleBack: () => Navigator.pop(context),
      ),
      body: Container(
        alignment: Alignment.center,
        height: double.infinity,
        padding: EdgeInsets.symmetric(horizontal: Grid.columns(context, 1.5)),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              NuText(
                i18n: "event.comming-soon",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Color(0xffd5c926),
                  fontSize: 13,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 10),
              TimeLeft(),
              SizedBox(height: 20),
              IdolEventContent()
            ],
          ),
        ),
      ),
    );
  }
}

class TimeLeft extends StatefulWidget {
  Duration timeUtilDue;

  @override
  _TimeLeftState createState() => _TimeLeftState();
}

class _TimeLeftState extends State<TimeLeft> {
  Timer _timer;

  Duration _timeUntilDue = Duration(milliseconds: 0);

  @override
  void initState() {
    new Future.delayed(Duration(milliseconds: 0), () {
      final startTIme = new DateTime.fromMillisecondsSinceEpoch(
          getIt<EventProvider>().details.value.start * 1000);

      setState(() {
        _timeUntilDue =
            new DateTime.utc(startTIme.year, startTIme.month, startTIme.day)
                .difference(DateTime.now());
      });

      _timer = Timer.periodic(Duration(seconds: 1), (timer) {
        setState(() {
          _timeUntilDue =
              new DateTime.utc(startTIme.year, startTIme.month, startTIme.day)
                  .difference(DateTime.now());
        });
      });
    });

    super.initState();
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    TextStyle _textStyle = TextStyle(
      color: Colors.white,
      fontSize: 25,
      fontWeight: FontWeight.bold,
    );

    String _getTimeLeft() {
      int hours = _timeUntilDue.inHours;
      int minutes = (_timeUntilDue.inMinutes - _timeUntilDue.inHours * 60);
      int seconds = (_timeUntilDue.inSeconds - _timeUntilDue.inMinutes * 60);

      return "${NumberModule.display(hours)} : ${NumberModule.display(minutes)} : ${NumberModule.display(seconds)}";
    }

    return Padding(
      padding: EdgeInsets.only(
        top: 20,
        bottom: 5,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          NuText(
            text: "${_getTimeLeft()}",
            style: _textStyle,
          ),
        ],
      ),
    );
  }
}

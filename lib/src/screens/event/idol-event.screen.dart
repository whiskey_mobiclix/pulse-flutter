import 'package:flutter/material.dart';
import 'package:nu/dependency-injection.dart';
import 'package:nu/src/providers/event.provider.dart';
import 'package:nu/src/screens/event/event.block.dart';

import 'Idol-event-screen-body/Idol-event-screen-body.dart';
import 'idol-event-screen-bar/idol-event-screen-bar.dart';

class IdolEventScreen extends StatefulWidget {
  @override
  _IdolEventScreenState createState() => _IdolEventScreenState();
}

class _IdolEventScreenState extends State<IdolEventScreen> {
  @override
  void initState() {
    getIt<EventProvider>().fetch();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    precacheImage(
      AssetImage('assets/images/backgrounds/idol-event-bg.png'),
      context,
    );

    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              alignment: Alignment.topCenter,
              image: AssetImage('assets/images/backgrounds/idol-event-bg.png'),
              fit: BoxFit.fitWidth,
            ),
          ),
          child: Scaffold(
            backgroundColor: Colors.transparent,
            appBar: IdolEventScreenBar(
              handleBack: () => Navigator.pop(context),
            ),
            body: IdolEventScreenBody(),
          ),
        ),
        if (!getIt<EventProvider>().details.value.opened) EventBlock(),
      ],
    );
  }
}

import 'package:flutter/material.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/widgets/core/text.widget.dart';

class IdolEventScreenBar extends StatelessWidget
    implements PreferredSizeWidget {
  final Function handleBack;

  IdolEventScreenBar({
    this.handleBack,
  });

  @override
  Widget build(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: false,
      centerTitle: false,
      title: _buildBarComponent(context),
      backgroundColor: Colors.transparent,
      elevation: 0.0,
      titleSpacing: 0.0,
    );
  }

  Widget _buildBarComponent(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        height: kToolbarHeight,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              flex: 1,
              child: GestureDetector(
                behavior: HitTestBehavior.opaque,
                onTap: () => handleBack(),
                child: Container(
                  padding: EdgeInsets.only(left: Grid.columns(context, 1)),
                  width: double.infinity,
                  alignment: Alignment.centerLeft,
                  child: Image.asset(
                    'assets/images/common/back-arrow.png',
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 5,
              child: Align(
                alignment: Alignment.center,
                child: NuText(
                  i18n: "idolEvent.title",
                  style: TextStyle(
                    fontSize: 14,
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: SizedBox(),
            ),
          ],
        ),
      ),
    );
  }

  Size get preferredSize => Size.fromHeight(kBottomNavigationBarHeight);
}

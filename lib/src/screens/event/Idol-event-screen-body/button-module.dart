import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/widgets/core/image.widget.dart';
import 'package:nu/src/widgets/core/text.widget.dart';
import 'package:nu/src/widgets/popup/idol-event-leaderboard-popup/idol-event-leaderboard-popup.dart';

class ButtonModule extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _height = Grid.columns(context, 24) / 6.32967033;

    precacheImage(
      AssetImage(
        'assets/images/backgrounds/idol-event-leaderboard-popup-bg.png',
      ),
      context,
    );

    precacheImage(
      AssetImage(
        'assets/images/common/idol-event-leaderboard-stand-reward.png',
      ),
      context,
    );

    return Container(
      child: Row(
        children: [
          GestureDetector(
            onTap: () => _openLeaderboardPopUp(context),
            child: Container(
              height: _height,
              width: Grid.columns(context, 8),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(60)),
                border: Border.all(width: 2, color: Colors.white),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  NuImage(
                    path: 'assets/images/common/podium.png',
                    height: _height / 3,
                    fit: BoxFit.contain,
                  ),
                  SizedBox(height: _height / 30),
                  NuText(
                    i18n: "idolEvent.btn.leaderboard",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 11,
                    ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(width: Grid.columns(context, 1)),
          GestureDetector(
            onTap: () {
              Navigator.pushNamed(context, "/event-introduction");
            },
            child: Container(
                height: _height,
                width: Grid.columns(context, 13),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(60)),
                  color: Color(0xff26d57d),
                ),
                child: Center(
                  child: NuText(
                    i18n: "idolEvent.btn.tutorial",
                    style: TextStyle(
                      fontSize: 13,
                      fontWeight: FontWeight.w700,
                      color: Colors.white,
                    ),
                  ),
                )),
          )
        ],
      ),
    );
  }

  void _openLeaderboardPopUp(BuildContext context) {
    showModal(
      context: context,
      configuration: FadeScaleTransitionConfiguration(
        barrierDismissible: false,
        barrierColor: Colors.black.withOpacity(0.8),
      ),
      builder: (BuildContext context) => IdolEventLeaderboardPopup(),
    );
  }
}

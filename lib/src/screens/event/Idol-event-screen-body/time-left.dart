import 'dart:async';

import 'package:flutter/material.dart';
import 'package:nu/dependency-injection.dart';
import 'package:nu/src/providers/event.provider.dart';
import 'package:nu/src/widgets/core/text.widget.dart';

class TimeLeft extends StatefulWidget {
  Duration timeUtilDue;

  @override
  _TimeLeftState createState() => _TimeLeftState();
}

class _TimeLeftState extends State<TimeLeft> {
  Timer _timer;

  Duration _timeUntilDue = Duration(milliseconds: 0);

  @override
  void initState() {
    new Future.delayed(Duration(milliseconds: 100), () {
      final endTime = new DateTime.fromMillisecondsSinceEpoch(
          getIt<EventProvider>().details.value.end * 1000);

      setState(() {
        _timeUntilDue =
            new DateTime.utc(endTime.year, endTime.month, endTime.day)
                .difference(DateTime.now());
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    TextStyle _textStyle = TextStyle(
      color: Colors.white.withOpacity(0.6),
      fontSize: 10,
    );

    String _getTimeLeft() {
      String days = _timeUntilDue.inDays.toString();

      String hours =
          (_timeUntilDue.inHours - _timeUntilDue.inDays * 24).toString();
      String minutes =
          (_timeUntilDue.inMinutes - _timeUntilDue.inHours * 60).toString();
      String seconds =
          (_timeUntilDue.inSeconds - _timeUntilDue.inMinutes * 60).toString();

      return "${days}d ${hours}h ${minutes}m";
    }

    return Padding(
      padding: EdgeInsets.only(
        top: 20,
        bottom: 5,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          if (_timeUntilDue.inSeconds > 1)
            NuText(
              i18n: "idolEvent.timeOut",
              style: _textStyle,
            ),
          if (_timeUntilDue.inSeconds > 1)
            NuText(
              i18n: "idolEvent.timeLeft",
              style: _textStyle,
            ),
          if (_timeUntilDue.inSeconds > 1)
            NuText(
              text: ": ${_getTimeLeft()}",
              style: _textStyle,
            ),
        ],
      ),
    );
  }
}

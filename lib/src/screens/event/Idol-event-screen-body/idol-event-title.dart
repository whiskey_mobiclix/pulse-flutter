import 'package:flutter/material.dart';
import 'package:nu/dependency-injection.dart';
import 'package:nu/src/providers/general.provider.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/widgets/core/image.widget.dart';

class IdolEventTitle extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: getIt<GeneralProvider>().language,
      builder: (context, lang, _) {
        return Container(
          child: NuImage(
            path: 'assets/images/common/idol-event-title.$lang.png',
            width: Grid.columns(context, 15),
          ),
        );
      },
    );
  }
}

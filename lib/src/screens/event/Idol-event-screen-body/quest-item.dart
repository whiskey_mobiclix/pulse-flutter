import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:nu/dependency-injection.dart';
import 'package:nu/src/models/gift.model.dart';
import 'package:nu/src/models/quest.event.model.dart';
import 'package:nu/src/providers/gift.provider.dart';
import 'package:nu/src/screens/event/Idol-event-screen-body/progress-bar.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/widgets/core/image.widget.dart';
import 'package:nu/src/widgets/core/text.widget.dart';

class QuestItem extends StatelessWidget {
  final double width;
  final double sizeRatio;
  final int number;
  final double require;
  final double currentValue;
  final Quest data;

  QuestItem({
    this.data,
    this.width,
    this.sizeRatio = 3.6,
    this.number = 1,
    this.require,
    this.currentValue,
  }) : assert(width != null);

  @override
  Widget build(BuildContext context) {
    bool isCompleted = this.data.earned >= this.data.goal;
    final giftPvdr = getIt<GiftProvider>();
    Gift gift = new Gift();

    if (giftPvdr.list.value.isNotEmpty)
      gift = giftPvdr.list.value
              .where((o) => o.id == this.data.giftId)
              .toList()[0] ??
          new Gift();

    return Container(
      width: width,
      height: width / sizeRatio,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(width / 23.857142857)),
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Color(0xffb915d8).withOpacity(0.9),
            Color(0xff26078f).withOpacity(0.9),
          ],
        ),
      ),
      margin: EdgeInsets.symmetric(vertical: 5),
      padding: EdgeInsets.symmetric(
        horizontal: width / 25.52173913,
        vertical: width / 35.75,
      ),
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: width / 8.35,
                height: width / 8.35,
                padding: EdgeInsets.all(3),
                decoration: new BoxDecoration(
                  color: Colors.white,
                  shape: BoxShape.circle,
                ),
                child: NuImage(
                  path: gift.thumbnail,
                  fit: BoxFit.fitHeight,
                ),
              ),
              SizedBox(width: 7),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  NuText(
                    text: gift.name,
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w700,
                      fontSize: 11,
                    ),
                  ),
                  SizedBox(height: 4),
                  _QuestMissionDescription(gift: gift),
                ],
              ),
//              if (!isCompleted) SizedBox(width: 3),
              if (!isCompleted) _rewardBuild(context, gift: gift),
              if (isCompleted) Spacer(),
              if (isCompleted)
                Container(
                  width: width / 10,
                  height: width / 10,
                  decoration: new BoxDecoration(
                    color: Color(0xff26d57d),
                    shape: BoxShape.circle,
                    border: Border.all(width: 3, color: Colors.white),
                  ),
                  child: Center(
                    child: Icon(
                      Icons.check,
                      color: Colors.white,
                    ),
                  ),
                )
            ],
          ),
          Spacer(),
          ProgressBar(
            width: width - width / 25.52173913,
            threshold: this.data.goal.toDouble(),
            value: this.data.earned > this.data.goal
                ? this.data.goal.toDouble()
                : this.data.earned.toDouble(),
          ),
        ],
      ),
    );
  }

  Widget _rewardBuild(BuildContext context, {Gift gift}) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        if (this.data.bonus != null && this.data.bonus > 0)
          itemQuantity(
            NuImage(path: gift.thumbnail, width: Grid.columns(context, 2)),
            quantity: this.data.bonus,
          ),
        if (this.data.gems > 0) SizedBox(width: 3),
        if (this.data.gems > 0)
          itemQuantity(
            Container(
              height: this.width / 11.13333333,
              width: this.width / 12.8462,
              alignment: Alignment.bottomCenter,
              padding: EdgeInsets.only(
                left: this.width / 11.13333333 / 15,
                right: this.width / 11.13333333 / 7,
              ),
              child: NuImage(
                path: 'assets/images/common/gem.png',
                width: Grid.columns(context, 1),
                fit: BoxFit.fitWidth,
              ),
            ),
            quantity: this.data.gems.toInt(),
          ),
      ],
    );
  }

  Widget _addGiftEffect({
    String imagePath,
    double width,
    double height,
  }) {
    final finalWidth = width != null ? width : height * 0.866666667;
    final finalHeight = height != null ? height : width / 0.866666667;

    return Container(
      width: finalWidth,
      height: finalHeight,
      alignment: Alignment.bottomCenter,
      padding: EdgeInsets.only(
        left: finalHeight / 15,
        right: finalHeight / 7,
      ),
      decoration: BoxDecoration(
        image: DecorationImage(
          alignment: Alignment.topCenter,
          image: AssetImage(
              'assets/images/backgrounds/idol-event-gift-effect.png'),
          fit: BoxFit.fitHeight,
        ),
      ),
      child: Image.asset(imagePath),
    );
  }

  Widget itemQuantity(Widget gift, {int quantity}) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        gift,
        Padding(
          padding: EdgeInsets.only(
            bottom: this.width / 111.333333333,
          ),
          child: NuText(
            text: "x $quantity ",
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w700,
              fontSize: 7,
            ),
          ),
        ),
      ],
    );
  }

  Widget _QuestMissionDescription({Gift gift}) {
    final _style = TextStyle(
      color: Colors.white.withOpacity(0.9),
      fontWeight: FontWeight.w300,
      fontSize: 8,
    );

    if (this.data.earned >= this.data.goal)
      return NuText(
        i18n: "event.quest.finished",
        style: _style,
      );

    return Row(
      children: [
        NuText(
          i18n: "event.collect",
          style: _style,
        ),
        NuText(
          text: " ${this.data.goal} ${gift.name} ",
          style: _style,
        ),
        NuText(
          i18n: "event.collect.to-get",
          style: _style,
        ),
      ],
    );
  }
}

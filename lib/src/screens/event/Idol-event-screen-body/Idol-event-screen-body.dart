import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:nu/dependency-injection.dart';
import 'package:nu/src/models/quest.event.model.dart';
import 'package:nu/src/modules/number.module.dart';
import 'package:nu/src/providers/event.provider.dart';
import 'package:nu/src/providers/gift.provider.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/widgets/core/image.widget.dart';
import 'package:nu/src/widgets/core/text.widget.dart';

import 'button-module.dart';
import 'idol-event-content.dart';
import 'idol-event-title.dart';
import 'quest-item.dart';
import 'time-left.dart';
import 'top-reward-item.dart';

class IdolEventScreenBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      left: false,
      right: false,
      child: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Container(
          padding: EdgeInsets.only(
            left: Grid.columns(context, 1),
            right: Grid.columns(context, 1),
            bottom: Grid.columns(context, 1),
          ),
          child: Column(
            children: [
              IdolEventTitle(),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 15.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(top: 23),
                      child: TopRewardItem(
                        width: Grid.columns(context, 22) / 3 - 5,
                        reward: 250,
                        top: 2,
                      ),
                    ),
                    TopRewardItem(
                      width: Grid.columns(context, 22) / 3 - 5,
                      top: 1,
                      reward: 400,
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 23),
                      child: TopRewardItem(
                        width: Grid.columns(context, 22) / 3 - 5,
                        top: 3,
                        reward: 150,
                      ),
                    )
                  ],
                ),
              ),
              IdolEventContent(),
              TimeLeft(),
              _GiftsListListener(
                child: Column(
                  children: [
                    _EventQuestList(),
                    _EventTotalPoint(),
                  ],
                ),
              ),
              ButtonModule(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _EventQuestList() {
    return ValueListenableBuilder(
      valueListenable: getIt<EventProvider>().list,
      builder: (context, list, _) {
        return Column(
          children: (list as List<Quest>)
              .map((o) => new QuestItem(
                    data: o,
                    width: Grid.columns(context, 22),
                  ))
              .toList(),
        );
      },
    );
  }

  Widget _EventTotalPoint() {
    return Container(
      padding: EdgeInsets.only(top: 20, bottom: 30),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          NuText(
            i18n: "idolEvent.totalPoint",
            style: TextStyle(
              color: Colors.white,
              fontSize: 13,
            ),
          ),
          SizedBox(width: 10),
          ValueListenableBuilder(
            valueListenable: getIt<EventProvider>().list,
            builder: (context, list, _) {
              int totalPoint = 0;

              list.forEach((o) {
                totalPoint += o.score;
              });

              return NuText(
                text: NumberModule.format(totalPoint),
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),
              );
            },
          ),
          Container(
            child: Transform.translate(
              offset: Offset(0.0, -1.0),
              child: Transform.scale(
                scale: 0.8,
                child: NuImage(
                  path: 'assets/images/common/star.png',
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _GiftsListListener({Widget child}) {
    final giftPvdr = getIt<GiftProvider>();
    if (giftPvdr.list.value.isEmpty) giftPvdr.fetch();

    return ValueListenableBuilder(
      valueListenable: giftPvdr.loading,
      builder: (context, loading, _) {
        if (loading)
          return Padding(
            padding: EdgeInsets.symmetric(vertical: 40),
            child: CircularProgressIndicator(
              valueColor: new AlwaysStoppedAnimation<Color>(Color(0xff26d57d)),
            ),
          );

        return child;
      },
    );
  }
}

import 'package:flutter/material.dart';
import 'package:nu/dependency-injection.dart';
import 'package:nu/src/modules/number.module.dart';
import 'package:nu/src/providers/event.provider.dart';
import 'package:nu/src/widgets/core/text.widget.dart';

class IdolEventContent extends StatelessWidget {
  TextStyle contentTextStyle = TextStyle(
    fontSize: 10,
    color: Color(0xffdcdcdc),
    height: 1.5,
  );

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Align(
          alignment: Alignment.centerLeft,
          child: NuText(
            i18n: "idolEvent.content1",
            style: contentTextStyle,
            textAlign: TextAlign.left,
          ),
        ),
        SizedBox(height: 10),
        Align(
          alignment: Alignment.centerLeft,
          child: _EventTimeRange(),
        ),
        SizedBox(height: 10),
        Align(
          alignment: Alignment.centerLeft,
          child: NuText(
            i18n: "idolEvent.content3",
            style: contentTextStyle,
            textAlign: TextAlign.left,
          ),
        ),
      ],
    );
  }

  Widget _EventTimeRange() {
    final event = getIt<EventProvider>().details.value;
    final start =
            new DateTime.fromMillisecondsSinceEpoch(event.start * 1000);
        final end = new DateTime.fromMillisecondsSinceEpoch(event.end * 1000);

    return Row(
      children: [
        NuText(
          i18n: "idolEvent.content2",
          style: contentTextStyle,
          textAlign: TextAlign.left,
        ),
        NuText(
            i18n: "event.time.from",
            capitalize: true,
            style: contentTextStyle,
        ),
        NuText(
            text: " ${NumberModule.display(start.hour)}h${NumberModule.display(start.minute)} ${NumberModule.display(start.day)}/${NumberModule.display(start.month)} ",
            style: contentTextStyle,
        ),
        NuText(
            i18n: "event.time.to",
            style: contentTextStyle,
        ),
        NuText(
            text: " ${NumberModule.display(end.hour)}h${NumberModule.display(end.minute)} ${NumberModule.display(end.day)}/${NumberModule.display(end.month)}/${NumberModule.display(end.year)}",
            style: contentTextStyle,
        ),
      ],
    );
  }
}

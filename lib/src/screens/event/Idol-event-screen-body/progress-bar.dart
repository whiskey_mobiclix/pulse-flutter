import 'dart:math';

import 'package:flutter/material.dart';
import 'package:nu/src/utils/utils.dart';
import 'package:nu/src/widgets/core/text.widget.dart';

class ProgressBar extends StatelessWidget {
  final double threshold;
  final double value;
  final double width;
  final double ratio;
  Color color;
  LinearGradient gradient;
  Border border;
  TextStyle textStyle;
  String i18n;

  ProgressBar({
    this.threshold,
    this.value,
    this.width,
    this.ratio = 18,
    this.color,
    this.gradient,
    this.border,
    this.textStyle,
    this.i18n,
  }) : assert(
          ratio >= 10 && width != null && threshold != null && value != null,
        );

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: this.border,
        borderRadius: BorderRadius.all(Radius.circular(width / 2.9)),
      ),
      child: Stack(
        children: [
          if (this.color != null)
            Container(
              width: width * (value / threshold),
              height: width / ratio,
              decoration: new BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(width / 2.9)),
                boxShadow: [
                  BoxShadow(
                    color: this.color ?? Color(0xffea0979).withOpacity(0.5),
                    spreadRadius: 3,
                    blurRadius: 10,
                    offset: Offset(0, 0), // changes position of shadow
                  ),
                ],
              ),
            ),
          Container(
            width: width,
            height: width / ratio,
            decoration: new BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(width / 2.9)),
            ),
          ),
          Container(
            width: width * (value / threshold),
            height: width / ratio,
            decoration: new BoxDecoration(
              gradient: this.gradient ??
                  LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    colors: [
                      this.color ?? Color(0xffea0979),
                      this.color ?? Color(0xffea0979)
                    ],
                  ),
              borderRadius: BorderRadius.all(Radius.circular(width / 2.9)),
            ),
          ),
          if (ratio <= 18)
            Container(
              width: width,
              height: width / ratio,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  NuText(
                    text: "${value.toInt()}/${threshold.toInt()}",
                    style: TextStyle(
                      color: Color(0xff270c89),
                      fontWeight: FontWeight.bold,
                      fontSize: 10,
                    ).merge(this.textStyle),
                  ),
                  if (!Utils.isEmpty(this.i18n))
                    Container(
                      margin: EdgeInsets.only(
                        left: 3,
                      ),
                      child: NuText(
                        i18n: this.i18n,
                        style: TextStyle(
                          color: Color(0xff270c89),
                          fontWeight: FontWeight.bold,
                          fontSize: 10,
                        ).merge(this.textStyle),
                      ),
                    ),
                ],
              ),
            )
        ],
      ),
    );
  }
}

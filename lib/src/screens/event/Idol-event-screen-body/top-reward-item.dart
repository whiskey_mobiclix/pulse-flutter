import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:nu/src/widgets/core/image.widget.dart';
import 'package:nu/src/widgets/core/text.widget.dart';

class TopRewardItem extends StatelessWidget {
  final double width;
  final int top;
  final int reward;

  TopRewardItem({
    this.width,
    this.top,
    this.reward,
  }) : assert(width != null);

  @override
  Widget build(BuildContext context) {
    final _rewardImage = {
      '1': 'assets/images/common/first-gift-star-reward.png',
      '2': 'assets/images/common/second-gift-star-reward.png',
      '3': 'assets/images/common/third-gift-star-reward.png',
    };

    return Container(
      width: width,
      child: Stack(
        alignment: Alignment.topCenter,
        children: [
          Container(
            width: width - width / 15.4643,
            height: width - width / 15.4643 + 5,
            decoration: BoxDecoration(
              borderRadius:
                  BorderRadius.all(Radius.circular(width / 7.732142857142857)),
              gradient: RadialGradient(
                radius: 0.6,
                colors: [
                  Color(0xff7500E9),
                  Color(0xff110058),
                ],
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(height: width / 9.1857),
                Image.asset(
                  _rewardImage[top.toString()],
                  height: width / 2,
                  fit: BoxFit.fitHeight,
                ),
                SizedBox(height: width / 50.742857),
                NuText(
                  text: '$reward Gems',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.w700,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            child: Container(
              height: width / 5.6234,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(
                    Radius.circular(width / 30.928571428571427)),
                gradient: LinearGradient(
                  colors: [
                    Color(0xffaa00ff),
                    Color(0xff0097ff),
                  ],
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 4.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    NuImage(
                      path: 'assets/images/common/star.png',
                      height: width / 8.83673469387755,
                      fit: BoxFit.fitHeight,
                    ),
                    SizedBox(width: width / 14.4333),
                    NuText(
                      text: 'Top $top Gift Star',
                      style: TextStyle(
                        fontSize: 8,
                        fontWeight: FontWeight.w700,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

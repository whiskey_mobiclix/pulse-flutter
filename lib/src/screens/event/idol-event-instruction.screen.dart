import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/widgets/core/image.widget.dart';
import 'package:nu/src/widgets/core/text.widget.dart';
import 'idol-event-screen-bar/idol-event-screen-bar.dart';

class IdolEventInstruction extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          alignment: Alignment.topCenter,
          image: AssetImage('assets/images/backgrounds/idol-event-bg.png'),
          fit: BoxFit.fitWidth,
        ),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: IdolEventScreenBar(
          handleBack: () => Navigator.of(context).pop(context),
        ),
        body: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Container(
            width: Grid.columns(context, 24),
            padding: EdgeInsets.only(bottom: 30),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  padding: EdgeInsets.all(
                    Grid.columns(context, 22) / 13.857142857142854,
                  ),
                  alignment: Alignment.center,
                  width: Grid.columns(context, 22),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(14)),
                    color: Color(0xff06001c).withOpacity(0.5),
                  ),
                  child: Column(
                    children: [
                      NuText(
                        i18n: "idolEvent.instruction.how",
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                        ),
                      ),
                      SizedBox(
                        height: Grid.columns(context, 1) / 2,
                      ),
                      NuText(
                        i18n: "idolEvent.instruction.how.content",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.white.withOpacity(0.8),
                          fontWeight: FontWeight.w400,
                          fontSize: 12,
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                    vertical: Grid.columns(context, 1),
                  ),
                  child: Stack(
                    children: [
                      Positioned(
                        bottom: Grid.columns(context, 22) / 4.7125,
                        left: Grid.columns(context, 22) / 10.771428571,
                        child: Image.asset(
                          "assets/images/common/pulse-man-1.png",
                          width: Grid.columns(context, 22) / 3.77,
                          fit: BoxFit.fitWidth,
                        ),
                      ),
                      _buildStepItem(
                        context,
                        i18nTitle: "idolEvent.instruction.stepOne.title",
                        i18nContent: "idolEvent.instruction.stepOne.content",
                        imagePath:
                            "assets/images/common/idol-event-instruction-step-one.png",
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                    vertical: Grid.columns(context, 1),
                  ),
                  child: Stack(
                    children: [
                      _buildStepItem(
                        context,
                        i18nTitle: "idolEvent.instruction.stepTwo.title",
                        i18nContent: "idolEvent.instruction.stepTwo.content",
                        imagePath:
                            "assets/images/common/idol-event-instruction-step-two.png",
                      ),
                      Positioned(
                        bottom: Grid.columns(context, 22) / 2.7125,
                        right: Grid.columns(context, 22) / 11.771428571,
                        child: Image.asset(
                          "assets/images/common/pulse-man-2.png",
                          width: Grid.columns(context, 22) / 3.77,
                          fit: BoxFit.fitWidth,
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                    vertical: Grid.columns(context, 1),
                  ),
                  child: Stack(
                    children: [
                      _buildStepItem(
                        context,
                        i18nTitle: "idolEvent.instruction.stepThree.title",
                        i18nContent: "idolEvent.instruction.stepThree.content",
                        imagePath:
                            "assets/images/common/idol-event-instruction-step-three.png",
                      ),
                      Positioned(
                        bottom: Grid.columns(context, 22) / 3.3,
                        left: Grid.columns(context, 22) / 12,
                        child: NuImage(
                          path: "assets/images/common/pulse-man-3.png",
                          width: Grid.columns(context, 22) / 3.77,
                          fit: BoxFit.fitWidth,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildStepItem(
    BuildContext context, {
    String i18nTitle,
    String i18nContent,
    String imagePath,
  }) {
    return Column(
      children: [
        NuText(
          i18n: i18nTitle,
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontSize: 13,
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(
            vertical: Grid.columns(context, 1) / 2,
          ),
          child: Image.asset(
            imagePath,
            width: Grid.columns(context, 10),
            fit: BoxFit.fitWidth,
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(
            horizontal: Grid.columns(context, 2),
          ),
          child: NuText(
            textAlign: TextAlign.center,
            i18n: i18nContent,
            style: TextStyle(
              color: Colors.white.withOpacity(0.78),
              fontWeight: FontWeight.w400,
              fontSize: 12,
            ),
          ),
        ),
      ],
    );
  }
}

import 'package:flutter/material.dart';
import 'package:nu/src/layouts/layout.dart';
import 'package:nu/src/widgets/core/text.widget.dart';

class UserRouter extends StatelessWidget {
  Widget build(BuildContext context) {
    return NuLayout(
      child: NuText(
        text: "This is user profile page",
      ),
    );
  }
}

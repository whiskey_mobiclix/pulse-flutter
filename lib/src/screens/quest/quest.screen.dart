import 'package:flutter/material.dart';
import 'package:nu/dependency-injection.dart';
import 'package:nu/src/layouts/layout.dart';
import 'package:nu/src/models/quest.daily.model.dart';
import 'package:nu/src/models/quest.reward.model.dart';
import 'package:nu/src/providers/quest.provider.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/widgets/core/image.widget.dart';
import 'package:nu/src/widgets/core/text.widget.dart';
import 'package:nu/src/widgets/quest/quest.daily.widget.dart';
import 'package:nu/src/widgets/quest/quest.reward.widget.dart';

class DailyQuestScreen extends StatefulWidget {
  @override
  _DailyQuestScreenState createState() => _DailyQuestScreenState();
}

class _DailyQuestScreenState extends State<DailyQuestScreen> {
  void initState() {
    super.initState();
    getIt<QuestProvider>().getDailyQuests();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.light,
        backgroundColor: Color(0xffd5d5d5).withOpacity(.2),
        elevation: 0,
        title: _dailyQuestAppBar(context),
      ),
      body: NuLayout(
        padding: EdgeInsets.all(Grid.columns(context, 1)),
        child: Column(
          children: [
            _dailyQuestList(context),
            SizedBox(height: 20),
            DailyQuestRewardWidget(
                data: new QuestReward(
              completed: 2,
              goal: 5,
            )),
          ],
        ),
      ),
    );
  }

  Widget _dailyQuestList(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: getIt<QuestProvider>().dailyQuests,
      builder: (context, dailyQuests, _) {
        return Column(
          children: (dailyQuests as List<DailyQuest>)
              .map((o) => new DailyQuestWidget(data: o))
              .toList(),
        );
      },
    );
  }

  Widget _dailyQuestAppBar(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: Grid.columns(context, .5),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          GestureDetector(
            child: Container(
              width: Grid.columns(context, 2),
              color: Colors.transparent,
              padding: EdgeInsets.only(
                top: 10,
                bottom: 10,
                right: Grid.columns(context, 1),
              ),
              child: NuImage(
                path: "assets/images/arrow-left-black.png",
              ),
            ),
          ),
          NuText(
            i18n: "quest.title",
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(width: Grid.columns(context, 1)),
        ],
      ),
    );
  }
}

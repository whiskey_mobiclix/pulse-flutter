import 'package:flutter/material.dart';
import 'package:nu/src/models/user.model.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/widgets/core/text.widget.dart';
import 'package:nu/src/widgets/profile/details.widget.dart';

class ProfileDetailsSection extends StatelessWidget {
  User user;

  ProfileDetailsSection({User user}) {
    this.user = user ?? new User({});
  }

  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: Grid.columns(context, 1)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          NuText(
            i18n: "profile.title",
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 12,
            ),
          ),
          SizedBox(height: 15),
          ProfileDetailsWidget(user: this.user),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:nu/dependency-injection.dart';
import 'package:nu/src/models/user.model.dart';
import 'package:nu/src/providers/me.provider.dart';
import 'package:nu/src/services/communicate.service.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/utils/utils.dart';
import 'package:nu/src/widgets/core/country.widget.dart';
import 'package:nu/src/widgets/core/image.widget.dart';
import 'package:nu/src/widgets/core/text.widget.dart';
import 'package:nu/src/widgets/profile/avatar.widget.dart';
import 'package:nu/src/widgets/profile/coins.widget.dart';
import 'package:nu/src/widgets/profile/gender.widget.dart';
import 'package:nu/src/widgets/profile/platinum.buy.widget.dart';
import 'package:nu/src/widgets/profile/platinum.tag.widget.dart';

class ProfilePreviewSection extends StatelessWidget {
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: getIt<MeProvider>().user,
      builder: (context, user, _) {
        return Stack(
          overflow: Overflow.visible,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: Grid.columns(context, 1),
                vertical: 15,
              ),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Transform.translate(
                    offset: Offset(-1.0 * Grid.columns(context, .5), 0),
                    child: ProfileAvatarWidget(
                      avatar: user.avatar,
                      level: user.level,
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      _ProfilePreviewNameAndAge(context, user),
                      SizedBox(height: 8),
                      _ProfilePreviewIdAndCountry(context, user),
                      if (user.platinum)
                        Padding(
                            padding: EdgeInsets.only(top: 5),
                            child: PlatinumTagWidget()),
                      SizedBox(height: 10),
                      if (!user.isFemale()) _ProfilePreviewButtons(user),
                    ],
                  ),
                ],
              ),
            ),
            Positioned(
              top: 0,
              right: Grid.columns(context, .5),
              child: _ProfileEditButton(context),
            )
          ],
        );
      },
    );
  }

  Widget _ProfilePreviewButtons(User user) {
    return Row(
      children: [
        ProfileCoinsWidget(coins: user.coins.toDouble()),
        SizedBox(width: 10),
        if (!Utils.isEmpty(user.id) && !user.platinum)
          BuyPlatinumButtonWidget(),
      ],
    );
  }

  Widget _ProfilePreviewIdAndCountry(BuildContext context, User user) {
    const style = TextStyle(
      fontSize: 9,
      color: Color(0xff7f7f7f),
    );

    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        NuImage(
          path: "assets/images/id.png",
          width: Grid.columns(context, .9),
        ),
        SizedBox(width: 8),
        Container(
          constraints: BoxConstraints(maxWidth: Grid.columns(context, 6)),
          child: NuText(
            text: Utils.clearZeroWidthSpace(user.uuid),
            overflow: TextOverflow.ellipsis,
            style: style,
          ),
        ),
        SizedBox(width: 15),
        NuCountry(
          code: user.country,
          style: style,
        )
      ],
    );
  }

  Widget _ProfilePreviewNameAndAge(BuildContext context, User user) {
    final style = TextStyle(
      fontSize: 12,
      fontWeight: FontWeight.bold,
    );

    return Row(
      children: [
        ProfileGenderWidget(gender: user.gender),
        SizedBox(width: 8),
        Container(
          constraints: BoxConstraints(maxWidth: Grid.columns(context, 11)),
          child: NuText(
            text: Utils.clearZeroWidthSpace(user.name),
            overflow: TextOverflow.ellipsis,
            maxLines: 1,
            softWrap: false,
            style: style,
          ),
        ),
        NuText(
          text: ", ${user.age}",
          style: style,
        )
      ],
    );
  }

  Widget _ProfileEditButton(BuildContext context) {
    return Padding(
        padding: EdgeInsets.all(10),
        child: GestureDetector(
          onTap: () {
            getIt
                .get<CommunicateService>()
                .navigate(route: "/user-edit-profile", args: {});
          },
          child: NuImage(
            path: "assets/images/pencil-circle.png",
            width: Grid.columns(context, 2),
          ),
        ));
  }
}

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:nu/dependency-injection.dart';
import 'package:nu/src/layouts/layout.dart';
import 'package:nu/src/models/message.model.dart';
import 'package:nu/src/providers/event.provider.dart';
import 'package:nu/src/providers/me.provider.dart';
import 'package:nu/src/screens/me/details/me.details.dart';
import 'package:nu/src/screens/me/menu/me.menu.dart';
import 'package:nu/src/screens/me/photos/me.photos.dart';
import 'package:nu/src/screens/me/preview/me.preview.dart';
import 'package:nu/src/services/communicate.service.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/utils/utils.dart';
import 'package:nu/src/widgets/core/image.widget.dart';
import 'package:nu/src/widgets/core/loader.widget.dart';
import 'package:nu/src/widgets/core/text.widget.dart';
import 'package:nu/src/widgets/profile/introduction.widget.dart';
import 'package:nu/src/widgets/profile/level.widget.dart';
import 'package:nu/src/widgets/profile/photo.preview.dart';
import 'package:nu/src/widgets/profile/stats.list.widget.dart';

class MeScreen extends StatefulWidget {
  @override
  _MeScreenState createState() => _MeScreenState();
}

class _MeScreenState extends State<MeScreen> {
  String previewPhoto = "";

  void initState() {
    super.initState();
    getIt<MeProvider>().fetch();
    getIt<EventProvider>().getDetails();
    getIt<CommunicateService>().context = context;
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: !Utils.isEmpty(previewPhoto)
          ? null
          : PreferredSize(
              preferredSize: Size.fromHeight(0.0),
              child: AppBar(
                elevation: 0.0,
                brightness: Brightness.light,
                backgroundColor: Color(0xfff5f5f5),
              ),
            ),
      body: WillPopScope(
        onWillPop: () async => false,
        child: _UserDataListener(
          child: _IntruductionStatusListener(
            child: ValueListenableBuilder(
              valueListenable: getIt<MeProvider>().updated,
              builder: (context, message, _) {
                return Stack(
                  alignment: Alignment.center,
                  children: [
                    _ProfileMainContent(),
                    if (message.show)
                      Positioned(
                        top: 0,
                        child: _ProfileUpdatedMessage(message),
                      ),
                    if (!Utils.isEmpty(previewPhoto))
                      PhotoPreview(
                        url: previewPhoto,
                        onCancel: () {
                          setState(() {
                            this.previewPhoto = "";
                          });
                        },
                      )
                  ],
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget _ProfileMainContent() {
    final user = getIt<MeProvider>().user.value;

    return NuLayout(
      child: Column(
        children: [
          Container(
              padding: EdgeInsets.only(bottom: 15),
              color: Color(0xfff5f5f5),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ProfilePreviewSection(),
                  SizedBox(height: 20),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: Grid.columns(context, 1)),
                    child: ProfileLevelWidget(),
                  ),
                  SizedBox(height: 15),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: Grid.columns(context, 1)),
                    child: StatsListWidget(
                      stats: user.stats,
                    ),
                  ),
                ],
              )),
          SizedBox(height: 30),
          ProfileDetailsSection(user: user),
          SizedBox(height: 20),
          MyPhotosSection(
              user: user,
              onSelect: (url) {
                setState(() {
                  this.previewPhoto = url;
                });
              }),
          ProfileMenuSection(),
          SizedBox(height: 30),
        ],
      ),
    );
  }

  Widget _ProfileUpdatedMessage(Message message) {
    return SafeArea(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 25, vertical: 10),
        decoration: BoxDecoration(
          color: Color(0xff26d57d),
          borderRadius: BorderRadius.all(Radius.circular(25)),
        ),
        child: Row(
          children: [
            NuImage(
              path: "assets/images/tick.png",
              width: Grid.columns(context, 1),
            ),
            SizedBox(width: 5),
            NuText(
              i18n: message.i18n,
              style: TextStyle(
                color: Colors.white,
                fontSize: 11,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _IntruductionStatusListener({Widget child}) {
    return ValueListenableBuilder(
      valueListenable: getIt<MeProvider>().introduction,
      builder: (context, introdction, _) {
        if (introdction["checking"]) return NuLoader();

        if (!introdction["users"]
            .contains(getIt<MeProvider>().user.value.id.toString())) {
          return ProfileIntroductionWidget();
        }

        return child;
      },
    );
  }

  Widget _UserDataListener({Widget child}) {
    return ValueListenableBuilder(
      valueListenable: getIt<MeProvider>().user,
      builder: (context, user, _) {
        if (Utils.isEmpty(user.id)) return NuLoader();

        if (user.id < 0) return BuildRetryModule(parentContext: context);

        return child;
      },
    );
  }
}

class BuildRetryModule extends StatefulWidget {
  final BuildContext parentContext;

  const BuildRetryModule({Key key, this.parentContext}) : super(key: key);

  @override
  _BuildRetryModuleState createState() => _BuildRetryModuleState();
}

class _BuildRetryModuleState extends State<BuildRetryModule> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Stack(
        alignment: Alignment.bottomCenter,
        children: [
          Container(
//            decoration: BoxDecoration(
//                color:
//            ),
            alignment: Alignment.center,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                NuImage(
                  path: "assets/images/common/network-error-ic.png",
                  height: 70,
                ),
                SizedBox(height: 15),
                ShowUp(
                  child: NuText(
                    i18n: "retry.title",
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  delay: 250,
                ),
                SizedBox(height: 10),
                ShowUp(
                  child: NuText(
                    i18n: "retry.subtitle",
                    style: TextStyle(
                      fontSize: 13,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  delay: 300,
                ),
              ],
            ),
          ),
          Positioned(
            bottom: Grid.columns(context, 1),
            child: Ink(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(60),
                color: Color(0xffF5F5F5),
              ),
              child: InkWell(
                onTap: () {
                  getIt<MeProvider>().fetch();
                },
                borderRadius: BorderRadius.circular(60),
                child: Container(
                  alignment: Alignment.center,
                  width: Grid.columns(context, 22),
                  height: 60,
                  child: NuText(
                    i18n: "retry.btn.title",
                    uppercase: true,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class ShowUp extends StatefulWidget {
  final Widget child;
  final int delay;

  ShowUp({@required this.child, this.delay});

  @override
  _ShowUpState createState() => _ShowUpState();
}

class _ShowUpState extends State<ShowUp> with TickerProviderStateMixin {
  AnimationController _animController;
  Animation<Offset> _animOffset;

  @override
  void initState() {
    super.initState();

    _animController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 500));
    final curve =
        CurvedAnimation(curve: Curves.decelerate, parent: _animController);
    _animOffset =
        Tween<Offset>(begin: const Offset(0.0, 0.35), end: Offset.zero)
            .animate(curve);

    if (widget.delay == null) {
      _animController.forward();
    } else {
      Timer(Duration(milliseconds: widget.delay), () {
        _animController.forward();
      });
    }
  }

  @override
  void dispose() {
    super.dispose();
    _animController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FadeTransition(
      child: SlideTransition(
        position: _animOffset,
        child: widget.child,
      ),
      opacity: _animController,
    );
  }
}

import 'package:flutter/material.dart';
import 'package:nu/dependency-injection.dart';
import 'package:nu/src/providers/event.provider.dart';
import 'package:nu/src/providers/me.provider.dart';
import 'package:nu/src/services/communicate.service.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/utils/utils.dart';
import 'package:nu/src/widgets/core/image.widget.dart';
import 'package:nu/src/widgets/core/text.widget.dart';

class ProfileMenuSection extends StatelessWidget {
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: getIt<MeProvider>().user,
      builder: (context, user, _) {
        return Padding(
          padding: EdgeInsets.symmetric(horizontal: Grid.columns(context, 1)),
          child: Column(
            children: [
              if (!Utils.isEmpty(user.id) && user.isFemale() & user.isIdol)
                Column(
                  children: [
                    _ProfileMenuSectionDivider(),
                    _ProfileMenuSectionItem(context,
                        icon: "assets/images/gem-alt.png",
                        nameI18nkey: "profile.data", onPressed: () {
                      getIt
                          .get<CommunicateService>()
                          .navigate(route: "/idol-data", args: {});
                    }),
                    _ProfileMenuSectionDivider(),
                    _ProfileMenuSectionItem(
                      context,
                      icon: "assets/images/event.png",
                      nameI18nkey: "profile.event",
                      padding: EdgeInsets.only(
                        top: 15,
                        left: 0,
                        right: 0,
                        bottom: 5,
                      ),
                      onPressed: () {
                        Navigator.pushNamed(context, "/event");
                      },
                    ),
                    _ProfileEventDetails(context),
                    SizedBox(height: 15),
                  ],
                ),
              if (!Utils.isEmpty(user.id) && !user.isFemale())
                Column(
                  children: [
                    _ProfileMenuSectionDivider(),
                    _ProfileMenuSectionItem(context,
                        icon: "assets/images/wallet.png",
                        nameI18nkey: "profile.coins", onPressed: () {
                      getIt<CommunicateService>().navigate(
                          route: "/store", args: {"selected": "coins"});
                    }),
                    _ProfileMenuSectionDivider(),
                    _ProfileMenuSectionItem(context,
                        icon: "assets/images/store.png",
                        nameI18nkey: "profile.store", onPressed: () {
                      getIt<CommunicateService>()
                          .navigate(route: "/store", args: {"selected": ""});
                    }),
                    _ProfileMenuSectionDivider(),
                    _ProfileMenuSectionItem(context,
                        icon: "assets/images/diamond-icon.png",
                        nameI18nkey: "profile.upgrade", onPressed: () {
                      getIt<CommunicateService>().navigate(
                          route: "/store",
                          args: {"selected": "", "type": "platinum"});
                    }),
                  ],
                ),
              _ProfileMenuSectionDivider(),
              _ProfileMenuSectionItem(context,
                  icon: "assets/images/leaderboard.png",
                  nameI18nkey: "profile.leaderboard", onPressed: () {
                Navigator.pushNamed(context, "/leader-board");
              }),
              _ProfileMenuSectionDivider(),
              _ProfileMenuSectionItem(context,
                  icon: "assets/images/level.png",
                  nameI18nkey: "profile.level", onPressed: () {
                Navigator.pushNamed(context, "/level");
              }),
              if (!Utils.isEmpty(user.id) && !user.isFemale())
                Column(children: [
                  _ProfileMenuSectionDivider(),
                  _ProfileMenuSectionItem(context,
                      icon: "assets/images/bag.png",
                      nameI18nkey: "profile.bag", onPressed: () {
                    getIt
                        .get<CommunicateService>()
                        .navigate(route: "/user-bag", args: {});
                  }),
                ]),
              _ProfileMenuSectionDivider(),
              _ProfileMenuSectionItem(context,
                  icon: "assets/images/settings.png",
                  nameI18nkey: "profile.settings", onPressed: () {
                getIt
                    .get<CommunicateService>()
                    .navigate(route: "/settings", args: {});
              }),
            ],
          ),
        );
      },
    );
  }

  Widget _ProfileMenuSectionItem(BuildContext context,
      {String icon,
      String nameI18nkey,
      VoidCallback onPressed,
      final EdgeInsetsGeometry padding}) {
    VoidCallback handlePressed() {
      if (onPressed != null) onPressed();
    }

    return GestureDetector(
      onTap: handlePressed,
      child: Container(
        color: Colors.transparent,
        padding: padding ?? EdgeInsets.symmetric(vertical: 15),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            NuImage(
              path: icon,
              width: Grid.columns(context, 2),
            ),
            SizedBox(width: 10),
            NuText(
              i18n: nameI18nkey,
              style: TextStyle(
                fontSize: 12,
              ),
            ),
            Expanded(child: Container()),
            NuImage(
              path: "assets/images/chevron.png",
              width: Grid.columns(context, .6),
            ),
          ],
        ),
      ),
    );
  }

  Widget _ProfileMenuSectionDivider() {
    return Container(
      color: Color(0xff707070).withOpacity(.2),
      height: 1,
    );
  }

  Widget _ProfileEventDetails(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: getIt<EventProvider>().details,
      builder: (context, event, _) {
        final start =
            new DateTime.fromMillisecondsSinceEpoch(event.start * 1000);
        final end = new DateTime.fromMillisecondsSinceEpoch(event.end * 1000);

        return Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(10)),
            image: DecorationImage(
              alignment: Alignment.topCenter,
              image:
                  AssetImage('assets/images/backgrounds/event-gift-star.png'),
              fit: BoxFit.cover,
            ),
          ),
          width: double.infinity,
          child: Row(
            children: [
              Container(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    NuText(
                      text: event.name,
                      style: TextStyle(
                        color: Color(0xffffe600),
                        fontSize: 14,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(height: 5),
                    NuText(
                      text:
                          "${start.day}/${start.month} - ${end.day}/${end.month}",
                      style: TextStyle(
                        color: Color(0xffffffff),
                        fontSize: 10,
                        fontStyle: FontStyle.italic,
                      ),
                    ),
                  ],
                ),
              ),
              Spacer(),
              NuImage(
                path: "assets/images/backgrounds/gift-star-decor.png",
                width: 100,
              ),
            ],
          ),
        );
      },
    );
  }
}

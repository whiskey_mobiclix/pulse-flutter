import 'package:flutter/material.dart';
import 'package:nu/dependency-injection.dart';
import 'package:nu/src/models/user.model.dart';
import 'package:nu/src/services/communicate.service.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/widgets/core/text.widget.dart';
import 'package:nu/src/widgets/profile/photos.slide.widget.dart';

class MyPhotosSection extends StatelessWidget {
  final User user;
  final Function(String) onSelect;

  MyPhotosSection({this.user, this.onSelect});

  Widget build(BuildContext context) {
    return Column(
      children: [
        _MyPhotosSectionDivider(context),
        SizedBox(height: 15),
        _MyPhotoSectionHeader(context),
        SizedBox(height: 25),
        PhotosSlideWidget(
            photos: this.user.photos.data,
            onSelect: (url) {
              if (this.onSelect != null) this.onSelect(url);
            }),
        if (this.user.photos.data.isNotEmpty) SizedBox(height: 25),
      ],
    );
  }

  Widget _MyPhotoSectionHeader(BuildContext context) {
    final noteStyle = new TextStyle(
      fontSize: 8,
      color: Color(0xff666666),
    );

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: Grid.columns(context, 1)),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          NuText(
            i18n: "profile.photos.title",
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(width: 5),
          NuText(
            text: "(",
            style: noteStyle,
          ),
          NuText(
            i18n: "profile.max",
            style: noteStyle,
          ),
          NuText(
            text: " 7 ",
            style: noteStyle,
          ),
          NuText(
            i18n: "profile.photos",
            style: noteStyle,
          ),
          NuText(
            text: ")",
            style: noteStyle,
          ),
          Expanded(child: Container()),
          _MyPhotoSectionUploadButton(),
        ],
      ),
    );
  }

  Widget _MyPhotosSectionDivider(BuildContext context) {
    return Container(
      height: 1,
      width: Grid.columns(context, 22),
      color: Color(0xff707070).withOpacity(.2),
    );
  }

  Widget _MyPhotoSectionUploadButton() {
    return GestureDetector(
      onTap: () {
        getIt
            .get<CommunicateService>()
            .navigate(route: "/user-edit-photos", args: {});
      },
      child: Container(
        padding: EdgeInsets.only(top: 10, left: 10),
        child: NuText(
          i18n: "profile.photos.upload",
          style: TextStyle(
            fontSize: 10,
            decoration: TextDecoration.underline,
            decorationStyle: TextDecorationStyle.solid,
            decorationColor: Color(0xff666666),
            color: Color(0xff666666),
          ),
        ),
      ),
    );
  }
}

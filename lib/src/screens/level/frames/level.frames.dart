import 'package:flutter/material.dart';
import 'package:nu/src/utils/constants.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/widgets/core/list.widget.dart';
import 'package:nu/src/widgets/core/text.widget.dart';
import 'package:nu/src/widgets/profile/frame.item.dart';

class LevelFramesSection extends StatelessWidget {
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(
        vertical: 20,
        horizontal: Grid.columns(context, 1),
      ),
      child: Column(
        children: [
          NuText(
              i18n: "level.frameReward.title",
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.bold,
              )),
          SizedBox(height: 5),
          NuText(
            i18n: "level.frameReward.subtitle",
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 11),
          ),
          SizedBox(height: 5),
          NuList(
            columns: 3,
            children: Constants.frames.list
                .map((o) => FrameItemWidget(
                      nameI18nKey: o["i18n"],
                      thumbnail: o["thumbnail"],
                      description: o["description"],
                      width: Grid.columns(context, 4),
                    ))
                .toList(),
          )
        ],
      ),
    );
  }
}

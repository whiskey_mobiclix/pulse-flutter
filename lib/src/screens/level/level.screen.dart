import 'package:flutter/material.dart';
import 'package:nu/dependency-injection.dart';
import 'package:nu/src/layouts/layout.dart';
import 'package:nu/src/providers/me.provider.dart';
import 'package:nu/src/screens/level/frames/level.frames.dart';
import 'package:nu/src/screens/level/introduction/level.introduction.dart';
import 'package:nu/src/screens/level/properties/level.properties.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/widgets/core/image.widget.dart';
import 'package:nu/src/widgets/core/text.widget.dart';
import 'package:nu/src/widgets/profile/avatar.widget.dart';
import 'package:nu/src/widgets/profile/level.widget.dart';

class LevelScreen extends StatelessWidget {
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        centerTitle: false,
        backgroundColor: Colors.black,
        elevation: 0.0,
        titleSpacing: 0.0,
        brightness: Brightness.dark,
        title: _LevelAppBar(context),
      ),
      body: NuLayout(
        child: Column(
          children: [
            SizedBox(height: 20),
            _UserInfoPreview(context),
            SizedBox(height: 20),
            LevelIntroductionSection(),
            LevelFramesSection(),
            if (!getIt<MeProvider>().user.value.isFemale())
              Container(
                margin: EdgeInsets.only(
                  top: 10,
                  bottom: 20,
                ),
                width: Grid.columns(context, 3),
                height: 1,
                color: Color(0xff1ebcbf),
              ),
            if (!getIt<MeProvider>().user.value.isFemale())
              LevelPropertiesSection(),
          ],
        ),
      ),
    );
  }

  Widget _UserInfoPreview(BuildContext context) {
    final user = getIt<MeProvider>().user.value;

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: Grid.columns(context, 1)),
      child: Column(
        children: [
          ProfileAvatarWidget(
            avatar: user.avatar,
            level: user.level,
          ),
          SizedBox(height: 10),
          ProfileLevelWidget(),
        ],
      ),
    );
  }

  Widget _LevelAppBar(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: Container(
            color: Colors.transparent,
            width: Grid.columns(context, 3),
            padding: EdgeInsets.all(Grid.columns(context, 1)),
            child: NuImage(
              path: "assets/images/arrow-left.png",
              fit: BoxFit.fitWidth,
            ),
          ),
        ),
        NuText(
          i18n: "profile.level",
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
        SizedBox(width: Grid.columns(context, 3)),
      ],
    );
  }
}

import 'package:flutter/material.dart';
import 'package:nu/dependency-injection.dart';
import 'package:nu/src/models/property.model.dart';
import 'package:nu/src/providers/property.provider.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/widgets/core/list.widget.dart';
import 'package:nu/src/widgets/core/text.widget.dart';
import 'package:nu/src/widgets/profile/property.item.dart';

class LevelPropertiesSection extends StatelessWidget {
  Widget build(BuildContext context) {
    final pvdr = getIt<PropertyProvider>();
    if (pvdr.list.value.isEmpty) pvdr.fetch();

    return Padding(
      padding: EdgeInsets.symmetric(
        vertical: 20,
        horizontal: Grid.columns(context, 1),
      ),
      child: Column(
        children: [
          NuText(
              i18n: "level.properties.title",
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.bold,
              )),
          SizedBox(height: 5),
          NuText(
            i18n: "level.properties.subtitle",
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 11),
          ),
          SizedBox(height: 15),
          ValueListenableBuilder(
            valueListenable: pvdr.list,
            builder: (context, list, _) {
              final items = (list as List<Property>)
                  .map((o) => new PropertyItemWidget(
                      data: o, width: Grid.columns(context, 6)))
                  .toList();

              return NuList(
                columns: 3,
                children: items,
              );
            },
          )
        ],
      ),
    );
  }
}

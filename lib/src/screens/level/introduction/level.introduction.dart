import 'package:flutter/material.dart';
import 'package:nu/src/utils/grid.dart';
import 'package:nu/src/widgets/core/image.widget.dart';
import 'package:nu/src/widgets/core/text.widget.dart';

class LevelIntroductionSection extends StatelessWidget {
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      color: Color(0xfff5f5f5),
      padding: EdgeInsets.symmetric(
        horizontal: Grid.columns(context, 1),
        vertical: 20,
      ),
      child: Column(
        children: [
          NuText(
            i18n: "level.how",
            style: TextStyle(
              fontSize: 14,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(height: 10),
          Row(children: [
            NuImage(
              path: "assets/images/swipe.png",
              width: Grid.columns(context, 3),
            ),
            SizedBox(width: 15),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                NuText(
                  i18n: "level.swipe",
                  style: TextStyle(
                    fontSize: 13,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: 5),
                SizedBox(
                  width: Grid.columns(context, 15),
                  child: NuText(
                    i18n: "level.swipe.info",
                    style: TextStyle(
                      fontSize: 10,
                    ),
                  ),
                )
              ],
            )
          ]),
        ],
      ),
    );
  }
}

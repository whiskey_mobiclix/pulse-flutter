import 'package:get_it/get_it.dart';
import 'package:nu/src/configs/enviroment.config.dart';
import 'package:nu/src/providers/event.provider.dart';
import 'package:nu/src/providers/general.provider.dart';
import 'package:nu/src/providers/gift.provider.dart';
import 'package:nu/src/providers/leaderboard.provider.dart';
import 'package:nu/src/providers/me.provider.dart';
import 'package:nu/src/providers/property.provider.dart';
import 'package:nu/src/providers/quest.provider.dart';
import 'package:nu/src/providers/test.provider.dart';

//Services
import 'package:nu/src/services/auth.service.dart';
import 'package:nu/src/services/communicate.service.dart';
import 'package:nu/src/services/device.service.dart';

//Create getIt instance
final getIt = GetIt.instance;

configureInjection() {
  //Services
  getIt.registerLazySingleton<DeviceService>(() => DeviceService());
  getIt.registerLazySingleton<AuthService>(() => AuthService());
  getIt.registerSingleton<CommunicateService>(CommunicateService());

  //Config
  getIt.registerSingleton<Enviroment>(Enviroment());

  //Providers
  getIt.registerSingleton<GeneralProvider>(GeneralProvider());
  getIt.registerSingleton<MeProvider>(MeProvider());
  getIt.registerLazySingleton<PropertyProvider>(() => PropertyProvider());
  getIt.registerLazySingleton<EventProvider>(() => EventProvider());
  getIt.registerLazySingleton<GiftProvider>(() => GiftProvider());
  getIt.registerLazySingleton<LeaderboardProvider>(() => LeaderboardProvider());
  getIt.registerLazySingleton<QuestProvider>(() => QuestProvider());
  getIt.registerLazySingleton<TestProvider>(() => TestProvider());
}

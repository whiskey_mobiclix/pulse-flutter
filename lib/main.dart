import 'package:flutter/material.dart';
import 'package:nu/dependency-injection.dart';
import 'package:nu/routes.dart';
import 'package:nu/src/configs/enviroment.config.dart';
import 'package:nu/src/configs/theme.config.dart';
import 'package:nu/src/providers/providers.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  // Configs
  configureInjection();
  getIt<Enviroment>().setMode("production");
  runApp(App());
}

@pragma('vm:entry-point')
void mainStaging() {
  WidgetsFlutterBinding.ensureInitialized();

  // Configs
  configureInjection();
  runApp(App());
}

class App extends StatelessWidget {
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: providers,
      child: MaterialApp(
        title: 'Nu core project',
        theme: ThemeData(
          fontFamily: AppTheme.font.name,
        ),
        routes: routes,
      ),
    );
  }
}